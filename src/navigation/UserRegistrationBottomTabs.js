// import 'react-native-gesture-handler';
import React, {useState, useEffect } from 'react';
import { Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import CampaignScreen from '../component/RegistrationFlow/dashboard/CampaignScreen/CampaignScreen/index'
import ProductsScreen from '../component/RegistrationFlow/dashboard/ProductsScreen/index'
import OrdersScreen from '../component/RegistrationFlow/dashboard/OrdersScreen/OrdersScreen/index'
import Images from '../utils/Images';
import Color from '../utils/Colors';
import { useIsFocused } from "@react-navigation/native";
import Fonts from '../assets/fonts';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Tab = createBottomTabNavigator();





const UserRegistrationBottomTabs = () => {

  const[storeId, setStoreId] = useState('')
  const isFocused = useIsFocused();


  useEffect(async()=> {
    await AsyncStorage.getItem("store_id").then((res) => setStoreId(res))
  })

  console.log('s--t---o---r--e--i---d', storeId)

  
    return (
        <Tab.Navigator 
        initialRouteName="CampaignScreen"
        // tabBarOptions= {{
        //   activeTintColor: Color.skyBlue
          
        // }}

        tabBarOptions = {{
          tabStyle: {
        
        },
          labelStyle: {
            fontSize: 13,
            fontFamily: Fonts.Samsung,
            marginVertical: 5,
            fontWeight: '500'
          },
        }}

        
        >
            <Tab.Screen
                name="CampaignScreen"
                component={CampaignScreen}
                initialParams={{ storeId }}
                
                // children={()=> <CampaignScreen data={storeId} />}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color , focused}) => (
                      <Image source={focused ? Images.ic_outline_campaign: Images.ic_compaign} style={{width: 16.67, height: 13.33, marginTop: 5}} />

                    ),
                    tabBarLabel: 'Campaign',
                  }} 
                  listeners={({ navigation }) => ({
                    focus: () => isFocused
                  })}  
                  //  navigationOptions: ({ navigation }) => ({
                  //   tabBarOnPress: (scene, jumpToIndex) => {
                  //     console.log('onPress:', scene.route);
                  //     jumpToIndex(scene.index);
                  //   },
                  // }),
                  
            />

            <Tab.Screen
                name="ProductsScreen"
                component={ProductsScreen}
                initialParams={{ storeId }}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused, color }) => (
                      <Image source={focused ? Images.ic_active_product : Images.ic_product} style={{width: 17.28, height: 18, marginTop: 5}} />
                    ),
                    tabBarLabel: 'Products',
                  }}
                  listeners={({ navigation }) => ({
                    focus: () => isFocused
                  })}  
            />

            <Tab.Screen
                name="OrdersScreen"
                component={OrdersScreen}
                initialParams={{ storeId }}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused, color }) => (
                      <Image source={focused ? Images.ic_active_order : Images.ic_orders} style={{width: 15.56, height: 16.88, marginTop: 5}} />
                    ),
                    tabBarLabel: 'Orders',
                  }}
                  listeners={({ navigation }) => ({
                    focus: () => isFocused
                  })}  
            />
        </Tab.Navigator>
    );
}
export default UserRegistrationBottomTabs