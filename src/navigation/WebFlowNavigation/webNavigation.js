import React from 'react'
import 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
// import User_Product_Detail from '../../component/userFlow/ProductDetail/index'
import Product_CheckOut from '../../component/userFlow/checkOut/index'
// import Shop_Product_Detail from '../../component/userFlow/ShopProductDetail/index'
import Order_Placed_Confirm from '../../component/userFlow/orderPlaced/index'
import Spin_Screen from '../../component/userFlow/SpinScreen/index'
import mauzoPointsScreen from '../../component/userFlow/mauzoPointsScreen/index'
import LoginScreen from '../../component/userFlow/LoginScreen/index'
import webTabNavigation from './webTabNavigation'
import Otp_Screen from '../../component/userFlow/otpScreen/index'
import mPaisaPaymentConfirmation from '../../component/userFlow/m_paisa_paymentConfirmation/index'
import mCashPaymentConfirmation from '../../component/userFlow/m_cash_paymentConfirmation/index'
import valoraPaymentConfirmation from '../../component/userFlow/valora_PaymentConfirmation/index'
import Payment_Processing from '../../component/userFlow/paymentProcessing/index'
import Country_ModelBoxs from '../../component/userFlow/countryCode/Country_ModelBox'
import ResetPassword from '../../component/userFlow/resetpassword/index'
import User_Product_Detail from '../../component/userFlow/ProductDetail/index'


const Stack = createNativeStackNavigator();

// const linking = {
//     config: {
//       screens: {
//         webTabNavigation: '/',
//         Product_CheckOut: ':id/product',
//         mPaisaPaymentConfirmation: ':id/m_Paisa',
//         valoraPaymentConfirmation: ':id/valora',
//         Payment_Processing: ':id/processing',
//         orderPlacedConfirm: ':id/orderSuccess',
//         spin_screen: ':id/spin',
//         mauzoPointsScreen: ':id/points',
//         LoginScreen: ':id/Login',
//         Otp_Screen: ':id/otp'
//       }
//     },
//   };


const linking = {
    config: {
        screens: {
            webTabNavigation: '/',
            //Product_CheckOut: '/product',
            Shop_Product_Detail: '/ShopProductDetail',
            User_Product_Detail: '/ProductDetail',
            Product_CheckOut: '/productCheckOut',
            mPaisaPaymentConfirmation: '/m_Paisa',
            mCashPaymentConfirmation: '/m_Cash',
            valoraPaymentConfirmation: '/valora',
            Payment_Processing: '/processing',
            orderPlacedConfirm: '/orderSuccess',
            spin_screen: '/spin',
            mauzoPointsScreen: '/points',
            LoginScreen: '/Login',
            Otp_Screen: '/otp',
            ResetPassword: '/resetPassword'
        }
    },
};



export default function WebNavigation() {
    return (
        <NavigationContainer linking={linking}  >
            <Stack.Navigator
                initialRouteName="webTabNavigation"
                screenOptions={{
                    headerShown: false
                }}>
                <Stack.Screen name="webTabNavigation"
                    component={webTabNavigation}
                />

                <Stack.Screen name="productCheckOut"
                    component={Product_CheckOut} />



                <Stack.Screen name="mPaisaPaymentConfirmation"
                    component={mPaisaPaymentConfirmation} />

                <Stack.Screen name="mCashPaymentConfirmation"
                    component={mCashPaymentConfirmation} />

                <Stack.Screen name="valoraPaymentConfirmation"
                    component={valoraPaymentConfirmation} />

                <Stack.Screen name="Payment_Processing"
                    component={Payment_Processing} />

                <Stack.Screen name="orderPlacedConfirm"
                    component={Order_Placed_Confirm} />

                <Stack.Screen name="spin_screen"
                    component={Spin_Screen} />


                <Stack.Screen name="mauzoPointsScreen"
                    component={mauzoPointsScreen} />

                <Stack.Screen name="User_Product_Detail"
                    component={User_Product_Detail} />


                <Stack.Screen name="LoginScreen"
                    component={LoginScreen} />

                <Stack.Screen name="Country_ModelBoxs"
                    component={Country_ModelBoxs}
                />


                <Stack.Screen name="Otp_Screen"
                    component={Otp_Screen} />

                <Stack.Screen name="ResetPassword"
                    component={ResetPassword} />


                {/*  <Stack.Screen name="spin_screen"
                    component={Spin_Screen} /> */}

            </Stack.Navigator>
        </NavigationContainer>
    )
}

