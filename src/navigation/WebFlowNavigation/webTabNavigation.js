import 'react-native-gesture-handler';
import React,{useState} from 'react';
import {Linking, Image, useRef} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Nuzo_Shop from '../../component/userFlow/shop/index'
import Nuzo_Points from '../../component/userFlow/nuzoPoint/index'
import shop from '../../assets/shop.png'
import nuzoPoint from '../../assets/Loyalty_Point.png'
import inactive_shop from '../../assets/inactive_shop.png'
import active_NuzoShop from '../../assets/active_Loyalty Point.png'
import Fonts from '../../assets/fonts';


const Tab = createBottomTabNavigator();


const webTabNavigation = ({route}) => {
  const dataVal =route.state;

  //const [showname, setShowName] = useState("Nuzo_Shop")
 // const [showurl, setShowURL] = useState("Nuzo_Shop")
  var showurl="Nuzo_Shop";
  var showname="Nuzo_Shop";
  //console.log('=======dataVal', dataVal)
  const queryParams = window.location.href;
  const lastSegment = queryParams.split("/").pop();

  if(dataVal==undefined)
  {
    
    if(lastSegment=="Nuzo_Points"){
      showurl="Nuzo_Points";
    }else{
      showname=lastSegment;
      showurl=showname;
    }
    
  }else{
    showname=dataVal.routeNames[0];
    if(dataVal.index==0){
      showurl=dataVal.routeNames[0];
    }else{
      showurl=dataVal.routeNames[1];

    }

  }
  
 // const id = queryParams.get('id');
//  console.log("test lastSegment --",lastSegment);
//  Linking.getInitialURL().then((url) => {
//   let data = url
//    lastSegment = data.split("/").pop();
//   //setUserToken(lastSegment)
   
console.log('----------------redirect nuzo-----lastSegment=', lastSegment)

// });
//console.log('----------------redirect nuzo-----', showurl, 'lastSegment', lastSegment)

    return (
      
        <Tab.Navigator 
        initialRouteName={showurl}
        tabBarOptions = {{
          tabStyle: {
        },
          labelStyle: {
            fontSize: 15,
            marginVertical: 5,
            fontFamily: Fonts.Samsung,
            fontWeight: '500'
          },
        }}
        >
          <Tab.Screen
                name={showname}//"Nuzo_Shop"
                component={Nuzo_Shop}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color , focused}) => (
                      <Image source={focused ? shop: inactive_shop} style={{width: 20, height: 20, marginTop: 5}} />

                    ),
                    tabBarLabel: 'Shop',
                  }}
            />

            <Tab.Screen
                name="Nuzo_Points"
                component={Nuzo_Points}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused, color }) => (
                      <Image source={focused ? active_NuzoShop: nuzoPoint} style={{width: 20, height: 20, marginTop: 5}} />
                    ),
                    tabBarLabel: 'Nuzo Points',
                  }}
            />
        </Tab.Navigator>
    );
}
export default webTabNavigation