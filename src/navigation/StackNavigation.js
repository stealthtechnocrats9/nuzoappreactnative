import React, { useState, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import EmailScreen from '../component/RegistrationFlow/signUpScreens/emailScreen/index'
import PasswordScrreen from '../component/RegistrationFlow/signUpScreens/passwordScrreen/index'
import NameRegister from '../component/RegistrationFlow/signUpScreens/nameRegister/index'
import PhoneVerificationScreen from '../component/RegistrationFlow/signUpScreens/phoneVerify/index'
import Country_ModelBox from '../component/RegistrationFlow/signUpScreens/phoneVerify/Country_ModelBox'
import Otp_Varification from '../component/RegistrationFlow/signUpScreens/phoneVerify/optVarification/index'
import Verify_account from '../component/RegistrationFlow/verifyAccount/index'
import loginScreen from '../component/RegistrationFlow/loginScreen/index'
import ResetPassword from '../component/RegistrationFlow/resetPassword/resetPasword/index'
import Opt_ForResetPassword from '../component/RegistrationFlow/resetPassword/optForResetPassword/index'
import Set_NewPassword from '../component/RegistrationFlow/resetPassword/setNewPassword/index'
import Store_Create from '../component/RegistrationFlow/createStore/index'
import Store_Verify from '../component/RegistrationFlow/storeVerify/index'
import Product_Create from '../component/RegistrationFlow/createProduct/index'
import Product_Verify from '../component/RegistrationFlow/productVerify/index'
import Product_offer from '../component/RegistrationFlow/createOffer/index'
import contactList from '../component/RegistrationFlow/createOffer/contactList'
import Select_Product from '../component/RegistrationFlow/SelectProduct/index'
import Campaign_Created from '../component/RegistrationFlow/CampaignCreated/index'
import UserRegistrationBottomTabs from './UserRegistrationBottomTabs'
import Campaign_Detail from '../component/RegistrationFlow/dashboard/CampaignScreen/CampaignDetail'
import Product_Update from '../component/RegistrationFlow/UpdateProduct/index'
import orderDetailScreen from '../component/RegistrationFlow/dashboard/OrdersScreen/orderDetailScreen/orderDetailScreen'
import orderDetailStatusChange from '../component/RegistrationFlow/dashboard/OrdersScreen/orderDetailStatusChange/index'
// import Browser from '../component/RegistrationFlow/dashboard/CampaignDetail/Browser'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../utils/loader';


const Stack = createStackNavigator();
export default function StackNavigation() {
    const [storeId, setStoreId] = useState('')
    const [userId, setUserId] = useState('')
    const [isShow, setIsShow] = useState(true)
    const [status, setStatus] = useState('')
    const [token, setToken] = useState('')



    useEffect(async () => {
        await AsyncStorage.getItem("status").then((res) => setStatus(res))
        await AsyncStorage.getItem("user_id").then((res) => setUserId(res))
        await AsyncStorage.getItem("token").then((res) => setToken(res))
        await AsyncStorage.getItem("store_id").then((res) => setStoreId(res))

        await setIsShow(false)
    })

    console.log('-----s--t--a--t--u--s--', status, userId, 'storeId-----', storeId, token)

    return (
        <NavigationContainer
        // linking={linking} 
        >
            {isShow ?
                <Loader />
                :
                <Stack.Navigator
                    initialRouteName={status == null ? "emailScreen" :
                        status == 1 ? "passwordScrreen" :
                            status == 2 ? "nameRegister" :
                                status == 3 ? "phoneVerificationScreen" :
                                    status == 4 ? "phoneVerificationScreen" :
                                        // status == 5 ? "stphoneVerificationScreenore_create" :
                                        storeId == null ? 'store_create' :
                                        token ? "userRegistration_bottomTabs" :
                                                "login_Screen"}

                    screenOptions={{
                        headerShown: false
                    }}>
                    <Stack.Screen name="emailScreen"
                        component={EmailScreen}
                    />

                    <Stack.Screen name="passwordScrreen"
                        component={PasswordScrreen}
                        initialParams={{ userId: "userId" }}
                    />

                    <Stack.Screen name="nameRegister"
                        component={NameRegister}
                        initialParams={{ userId }}

                    />
                    <Stack.Screen name="phoneVerificationScreen"
                        component={PhoneVerificationScreen}
                        initialParams={{ userId }}

                    />

                    <Stack.Screen name="Country_ModelBox"
                        component={Country_ModelBox}
                        initialParams={{ userId }}
                    />


                    <Stack.Screen name="otp_varification"
                        component={Otp_Varification}
                        initialParams={{ userId }}

                    />

                    <Stack.Screen name="verify_account"
                        component={Verify_account}
                        initialParams={{ userId }}
                    />

                    <Stack.Screen name="login_Screen"
                        component={loginScreen} />

                    <Stack.Screen name="reset_password"
                        component={ResetPassword} />

                    <Stack.Screen name="opt_forResetPassword"
                        component={Opt_ForResetPassword} />

                    <Stack.Screen name="set_newPassword"
                        component={Set_NewPassword} />

                    <Stack.Screen name="store_create"
                        component={Store_Create}
                    />

                    <Stack.Screen name="store_verify"
                        component={Store_Verify}
                    />

                    <Stack.Screen name="product_create"
                        component={Product_Create} />

                    <Stack.Screen name="product_verify"
                        component={Product_Verify} />

                    <Stack.Screen name="product_offer"
                        component={Product_offer} />

                    <Stack.Screen name="contactList"
                        component={contactList} />


                    <Stack.Screen name="select_product"
                        component={Select_Product} />

                    <Stack.Screen name="campaign_created"
                        component={Campaign_Created} />


                    <Stack.Screen name="userRegistration_bottomTabs"
                        component={UserRegistrationBottomTabs} />

                    <Stack.Screen name="campaign_detail"
                        component={Campaign_Detail} />

                    <Stack.Screen name="orderDetailScreen"
                        component={orderDetailScreen} />

                    <Stack.Screen name="orderDetailStatusChange"
                        component={orderDetailStatusChange} />

                    <Stack.Screen name="Product_Update"
                        component={Product_Update} />

                </Stack.Navigator>
            }
        </NavigationContainer>
    )
}
