import { gql } from '@apollo/client';

export const CUSTOMER_LOGIN = gql`
mutation customerLogin($phone: String! $country_code: String! $country_shortCode: String!){
    customerLogin (customer: { phone: $phone country_code: $country_code country_shortCode: $country_shortCode })
    {
      _id
      name
      phone
      token
      otp_status
      otp
    }
  }`


