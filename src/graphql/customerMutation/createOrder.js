import { gql } from '@apollo/client';

export const CREATE_ORDER = gql`
mutation createOrder(
    $subtotal: String!,
    $discount: String!,
    $total: String!,
    $pickup_address: String!,
    $country_code: String!,
    $country_shortCode: String!,
    $phone_no: String!,
    $name: String!,
    $product_id: String!,
    $device_id: String!,
    $device_type: String!,
    $device_token: String!
    $offer_id: String!
){
    createOrder(
      order: {
        subtotal: $subtotal,
        discount: $discount,
        total: $total,
        pickup_address: $pickup_address,
        country_code: $country_code,
        country_shortCode: $country_shortCode,
        phone_no: $phone_no,
        name: $name,
        product_id: $product_id,
        device_id: $device_id,
        device_type: $device_type,
        device_token: $device_token,
        offer_id: $offer_id
      }
    ) {
      _id
      customer_id
    }
  }`

  export const CREATE_NPORDER = gql`
  mutation createOrder(
      $subtotal: String!,
      $discount: String!,
      $total: String!,
      $pickup_address: String!,
      $country_code: String!,
      $country_shortCode: String!,
      $phone_no: String!,
      $name: String!,
      $product_id: String!,
      $device_id: String!,
      $device_type: String!,
      $device_token: String!
  ){
      createOrder(
        order: {
          subtotal: $subtotal,
          discount: $discount,
          total: $total,
          pickup_address: $pickup_address,
          country_code: $country_code,
          country_shortCode: $country_shortCode,
          phone_no: $phone_no,
          name: $name,
          product_id: $product_id,
          device_id: $device_id,
          device_type: $device_type,
          device_token: $device_token,
        }
      ) {
        _id
        customer_id
      }
    }`
  
  


