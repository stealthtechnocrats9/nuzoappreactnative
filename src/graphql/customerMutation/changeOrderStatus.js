import { gql } from '@apollo/client';

export const CHANGE_ORDER_STATUS = gql`
mutation{
    changeOrderStatus( order: { order_id: "614097a703947420d17a66bf", status: "2"  }){
      _id
    }
  }`