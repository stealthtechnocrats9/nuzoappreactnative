import { gql } from '@apollo/client';


export const RESET_PASSWORD = gql`
mutation resetPassword($token: String! $password: String! $confirmPassword: String!) {
       resetPassword(user: {
             token: $token
             password:$password
             confirmPassword: $confirmPassword
 })
 {
     _id
    }
  }`