import { gql } from '@apollo/client';

export const CREATE_PAYMENT = gql`
        mutation createPayment (
        $order_id: String!,
        $customer_id: String!,
        $amount: String!,
        $type: String!
    ){
    createPayment(
       payment: {
        order_id: $order_id,
        customer_id: $customer_id,
        amount: $amount,
        type: $type
      }
    ) {
      _id
    }
  }`