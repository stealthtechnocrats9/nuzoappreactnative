import { gql } from '@apollo/client';


export const CUSTOMER_OTP = gql`
  mutation verifyOTPtoCustomer($customer_id: String!, $otp: String!){
    verifyOTPtoCustomer (customer: {
      customer_id: $customer_id,
      otp: $otp
      })
    {
      _id
      name
      phone
      otp_status
      otp
    }
  }`

  export const CUSTOMER_RESEND_OTP = gql`
  mutation resendOTPtoCustomer( $customer_id: String!){
    resendOTPtoCustomer(customer: { 
      customer_id: $customer_id 
    }) {
      _id
      name
      phone
      otp_status
    }
  }`
  