import { gql } from '@apollo/client';

export const GET_ALL_PRODUCT = gql`
  query products($store_id: String!){
    products(store_id: $store_id) {
      _id
      product_name
      product_price
      product_description
      currency
      user_id 
      product_image{ _id product_image }
    }
  }`

  // query {
  //   products {
  //     _id
  //     product_name
  //     product_price
  //     product_description
  //     currency
  //     user_id  
  //     product_image{ _id product_image }
  //   }
  // }`


export const GET_SEARCH_PRODUCT = gql`
  query searchProducts ( $product_name: String!) {
      searchProducts (product_name: $product_name){
        _id
      product_name
      product_price
      currency
      user_id  
      product_image{ product_image }
      }
    }`


export const GET_PRODUCT_DETAIL_BY_ID = gql`
    query getProductDetailById ($offer_id : String!){
      getProductDetailById(offer_id: $offer_id)
      {  
        store_id 
        product_image{ product_image 
        }
        
      }
    }`