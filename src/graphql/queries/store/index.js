import { gql } from '@apollo/client';

export const GET_STORE = gql`
query {
    getStores {
      _id
      store_name
      store_image
      shop_address
      created_at
      
    }
  }`