import { gql } from '@apollo/client';

export const GET_ALL_ORDER = gql`
query {
  orders {
    _id
    subtotal
    discount
    total
    product_id
    pickup_address
    created_at
    order_no
    order_status
    product_detail {
      _id
      product_name
      product_price
      product_description
      currency
    }
  }
}`

