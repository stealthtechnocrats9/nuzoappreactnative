// import { gql } from '@apollo/client';

// export const GET_ORDERLIST_BY_ORDER_ID = gql`
// query getOrderDetailsByOrderId (
//     $order_id: String!
// ){
//     getOrderDetailsByOrderId( order_id: $order_id){
//       product_id
//       created_at
//       order_status
//       order_no
//       product_detail { product_name }
//       customer_detail { name }
  
//     }
//   }`




import { gql } from '@apollo/client';

export const GET_ORDERLIST_BY_ORDER_ID = gql`
query getOrderDetailsByOrderId (
    $order_id: String!
){
    getOrderDetailsByOrderId( order_id: $order_id){
      product_id
      order_status
      order_no
      created_at
      product_detail { product_name }
      customer_detail { name }
      offer_detail { _id expire_date }
  
    }
  }`

