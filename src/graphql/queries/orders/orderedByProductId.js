import { gql } from '@apollo/client';

export const GET_ORDER_BY_PRODUCTID = gql`
query ordersByProductId (
$product_id: String!
){
    ordersByProductId(
        product_id: $product_id
    ){
      _id
      product_id
      name
      phone_no
      created_at
      product_detail{
         product_name,
          product_price,
          
         }
    }
  }`