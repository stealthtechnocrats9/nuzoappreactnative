import { gql } from '@apollo/client';

export const GET_CAMPAIGN_OFFERS = gql`
query getOffers($store_id: String!) {
    getOffers (store_id: $store_id) {
      _id
      store_id
      store_name
      offer_id
      sent
      ctr
      total_clicks
      total_orders
      product_name
      product_price
      product_description
      product_image
      status
    }
  }`



export const GET_CAMPAIGN_DETAIL = gql`
  query getOfferDetailsByID ($offer_id: String!) {
    getOfferDetailsByID(offer_id: $offer_id) {
      _id     
       sent
       ctr
       total_clicks
       product_name
       product_price
       product_image
      product_id
      status
      message
      discount
      total_contacts
      discount
      offer_price
      order_detail {_id subtotal created_at name discount order_status total product_id pickup_address phone_no }
      }
  }`


  export const GET_GRAPH_DAILY_DETAIL = gql`
  query clicksGraph($offer_id: String!, $type: String!) {
    clicksGraph(offer_id: $offer_id, type: $type) {
       sent
       ctr
       total_clicks
      time 
    }
  }`

  export const GET_GRAPH_WEEKLY_DETAIL = gql`
  query clicksGraph($offer_id: String!, $type: String!){
    clicksGraph(offer_id: $offer_id, type: $type) {   
       sent
       ctr
       total_clicks
      day
   
    }
  }`


  export const GET_GRAPH_MONTHLY_DETAIL = gql`
  query clicksGraph($offer_id: String!, $type: String!){
    clicksGraph(offer_id: $offer_id, type: $type)  {
       sent
       ctr
       total_clicks
      month
   
    }
  }`



  export const GET_GRAPH_YEARLY_DETAIL = gql`
  query clicksGraph($offer_id: String!, $type: String!){
    clicksGraph(offer_id: $offer_id, type: $type)  {
       sent
       ctr
       total_clicks
      month
   
    }
  }`


