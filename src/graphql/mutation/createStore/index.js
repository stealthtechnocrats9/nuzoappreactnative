import { gql } from '@apollo/client';

export const CREATE_STORE = gql`
mutation createStoreFun(
   $store_name: String!, 
   $shop_address: String!,
   $store_image: File!,
   $payment_option: String!
   $marchant_name: String!
   ){ 
  createStoreFun(
    store: {
    store_name: $store_name ,
    shop_address: $shop_address,
    store_image: $store_image,
    payment_option: $payment_option,
    marchant_name: $marchant_name
  })
  {
    _id 
    store_name
    store_image
  }
}`




// export const CREATE_STORE =  gql`
// mutation($store_image:Upload!)
// { createStoreFun( store: {store_name:""dfd"" shop_address:""panchkula"" store_image:$store_image})
// {_id store_image}
// }`