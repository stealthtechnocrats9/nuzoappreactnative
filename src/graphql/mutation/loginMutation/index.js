import { gql } from '@apollo/client';

export const LOGIN = gql`
  mutation login($email: String!, $password: String!){
    login (user: {
       email: $email, 
       password: $password
      }) 
      {
      token
      signup_status
      _id
      store_details{_id store_name }
  }
}`


export const FORGOT_PASSWORD = gql`
mutation forgotPassword( $email: String!) 
{
  forgotPassword(user: { email: $email }){
   _id
  }
}`