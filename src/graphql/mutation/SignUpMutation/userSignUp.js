import { gql } from '@apollo/client';

export const CREATE_EMAIL = gql`
  mutation stepOneSignUp($email: String!){
    stepOneSignUp (user: {email: $email}) {
      _id
      email
      signup_status
      token
    }
  }
`

export const CREATE_PASSWORD = gql`
  mutation stepTwoSignUp($user_id: String!, $password: String!){
    stepTwoSignUp (user: { user_id: $user_id, password: $password}) {
      _id
      signup_status,
    }
  }
`

export const CREATE_NAME = gql`
  mutation stepThreeSignUp($user_id: String!, $firstname: String!, $lastname: String!){
    stepThreeSignUp (user: { user_id: $user_id, firstname: $firstname, lastname: $lastname}) {
      _id
      signup_status
    }
  }
`

export const ADD_PHONE = gql`
  mutation stepFourSignUp(
    $user_id: String!
    $phone: String!
     $country_code: String!
     $country_shortCode: String!
     ){
    stepFourSignUp (user: { 
      user_id: $user_id,
       phone: $phone,
        country_code: $country_code,
        country_shortCode: $country_shortCode
       }) {
      _id
      signup_status
      otp
      otp_status
      country_code
    }
  }
`


// export const ADD_PHONE = gql`
// mutation {
//   stepFourSignUp(user: {user_id: "6194cadbbc6e76142ce3c0a5",
//    , phone: "8295290111", country_code:"91"}) {
//    _id   
//    signup_status otp otp_status country_code
//  }
// }`

// export const OTP_VERIFY = gql`
// mutation resendOTP ($user_id: String!){
// resendOTP(user: {
//    user_id: $user_id
//    })
// { 
//   _id 
//   signup_status 
//   otp_status 
//   otp 
// }
// }`


export const OTP_VERIFY = gql`
  mutation verifyOTP($user_id: String!, $otp: String!){
    verifyOTP (user: { user_id: $user_id, otp: $otp }) {
      _id
      signup_status
      otp
    }
  }
`

export const RESEND_OTP = gql`
mutation resendOTP ($user_id : String!){
  resendOTP(user: {
     user_id: $user_id })
   { 
     _id 
     signup_status 
     otp_status 
     otp }}`