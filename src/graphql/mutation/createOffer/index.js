import { gql } from '@apollo/client';

export const CREATE_OFFER = gql`
  mutation createOffer(
      $product_id: String!, 
      $store_id: String!,
      $product_price: String!, 
      $discount: String!,
      $offer_price: String!, 
      $message: String!,
      $product_url: String!,
      $total_contacts: String!, 
      $contacts: String!,
      $expire_date: String!
      ){    
    createOffer (offer: {
        product_id: $product_id,
        store_id: $store_id,
        product_price: $product_price,
        discount: $discount,
        offer_price: $offer_price,
        message: $message,
        product_url: $product_url,
        total_contacts: $total_contacts,
        contacts: $contacts,
        expire_date: $expire_date
      }) 
      {
        _id
        expire_date
    }
}
`


export const PRODUCT_IMAGE = gql`
query getProductImages($product_id: String!){
  getProductImages(product_id: $product_id){
    _id
    product_image
  }
}`

export const PAUSE_OFFER = gql`
mutation pauseOffer( $offer_id : String!){
  pauseOffer(offer: { offer_id: $offer_id }){
    _id
    status
  }
}`


export const CONTINUE_OFFER = gql`
mutation continueOffer( $offer_id : String!){
    continueOffer(offer: { offer_id: $offer_id}){
      _id
      status
    }
  }`

  

  export const DELETE_CAMPAIGN = gql`
  mutation deleteOffer (
    $offer_id: String!
  ) {
      deleteOffer(offer: { offer_id: $offer_id }) {
        n
        ok
        deletedCount
      }
    }`