import { gql } from '@apollo/client';


export const CREATE_PRODUCT = gql`
mutation( 
  $product_name: String!,
  $currency: String!,
  $product_description: String!,
  $product_price: String!,
  $store_id: String!,
  $product_image: [Upload!]!,

  ) {
  createProduct(
    product: {
      product_name: $product_name,
      currency: $currency,
      product_description: $product_description,
      product_price: $product_price,
      store_id: $store_id
      product_image: $product_image
    })
  {
    _id
    store_id
  }}

`
export const UPDATE_PRODUCT = gql`
mutation(
  $product_name: String!,
  $product_description: String!,
  $product_price: String!,
  $product_id: String!,
  $product_image: [Upload!]!
  ) {
  updateProduct(
    product: {
      product_name: $product_name,
      product_description: $product_description,
      product_price: $product_price,
      product_id: $product_id
      product_image: $product_image
    }
  ) {
    _id
    created_at
    product_name
  }
}`




export const DELETE_PRODUCT = gql`
    mutation deleteProduct($product_id: String!){
    deleteProduct(
        product: { 
            product_id: $product_id 
        }) 
        {
      n
      ok
      deletedCount
    }
  }`


export const REMOVE_PRODUCT_IMAGE = gql`
 mutation removeProductImage(
   $image_id: String!, 
   $product_id: String!
    )
 {
  removeProductImage(
    product: {
       image_id: $image_id, 
       product_id: $product_id 
      })
      {
          ok
          n
        }
    }`


export const DUPLICATE_PRODUCT = gql`

    mutation duplicateProduct(
        $product_id: String!
    ){
      duplicateProduct(product : {product_id: $product_id }){
        _id
        product_name
        product_price
      }
    }`
  
  