import { gql } from '@apollo/client';

export const CHANGE_ORDER_STATUS = gql`
mutation changeOrderStatus(
    $order_id: String!,
     $status: String!
     ){
    changeOrderStatus( order: {
         order_id: $order_id,
          status: $status  
        })
        {
      _id
    }
  }`