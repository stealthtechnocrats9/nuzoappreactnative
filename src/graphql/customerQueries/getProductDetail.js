
import { gql } from '@apollo/client';

export const GET_ALLPRODUCT = gql`
query stores($storeName : String!){
    stores(storeName: $storeName){
      _id
      store_name
      store_image
      shop_address
      marchant_name 
    	payment_option
      created_at
      products {
        _id
        product_name
        product_price
        status
        currency
        product_description
        product_image {
          product_image
          product_id
        }
      }
      offer_products {
        _id
        product_name
        product_price
        status
        currency
        product_description
        product_image {
          product_image
          product_id
        }
        offer_price
        discount
        expire_date
      }
    }
  }`