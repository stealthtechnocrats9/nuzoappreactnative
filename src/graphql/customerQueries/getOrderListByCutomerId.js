import { gql } from '@apollo/client';

export const GET_ORDERLIST_BY_CUSTOMER_ID = gql`
query getOrdersListByCustomerId (
    $customer_id: String!
){
    getOrdersListByCustomerId(
        customer_id: $customer_id
        )
        {
          product_id
          total
          order_status
          created_at
          product_detail { product_name currency product_price }
          payment_detail { customer_id  } 
          store_detail { 
            _id 
            store_name 
            marchant_name 
          	payment_option
     
           } 
      
    }
  }`


