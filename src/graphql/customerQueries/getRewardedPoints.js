import { gql } from '@apollo/client'

export const GET_REWARDS_POINTS = gql`
query getRewardPoints($customer_id: String!){
  getRewardPoints(customer_id: $customer_id) {
    rewards
  }
}`