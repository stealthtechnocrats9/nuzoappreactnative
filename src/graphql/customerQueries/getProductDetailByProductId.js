import { gql } from '@apollo/client';

export const GET_ALLPRODUCT_BY_ID =  gql`
query getProductDetailByProductId ($token: String!){
  getProductDetailByProductId(product_id: $token) {
    _id
    product_name
    status
    product_price
    product_description
    currency
    store_detail { store_name shop_address _id }
    marchant_name
    payment_option
    product_image {
      product_image
    }
    }
  }
`
