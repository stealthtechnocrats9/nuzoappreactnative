import { gql } from '@apollo/client';

export const GET_ALLPRODUCT_BY_TOKEN =  gql`
query getProductDetailByToken ($token: String!){
    getProductDetailByToken(token: $token) {
      _id
      store_id
      product_id
      product_name
      status
      product_price
      product_description
      currency
      discount
      offer_price
      message
      total_contacts
      expire_date
      store_detail { store_name shop_address _id }
      marchant_name
      payment_option
      product_image {
        product_image
      }
    }
  }
`
