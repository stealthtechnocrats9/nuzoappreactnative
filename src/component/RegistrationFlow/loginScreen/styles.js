import { StyleSheet } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,

    },
    logo: {
        height: 246,
        width: 245,
        alignSelf: 'center'

    },
    main: {
        width: '100%',
        paddingVertical: 15,
    },
    headingText: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: '600',
        marginTop: 50,
        marginBottom: 8
    },
    nextLineHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        marginBottom: 4
    },
    inputBoxHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: 'Samsung Sharp Sans',
        marginTop: 10.5,
        marginBottom: 6.5,
    },

    input: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginHorizontal: 20,
        paddingHorizontal: 10,
        color: Color.black,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },
    InputViewStyle: {
        flexDirection: 'row',
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '90%',
        alignSelf: 'center'

    },
    passwordInput: {
        height: 50,
        borderRadius: 16,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '80%',
        paddingHorizontal: 10,
        color: Color.black,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },

    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        marginTop: 8,
        color: Color.red,
        marginLeft: 25,
        
    },
    showHidePassword: {
        height: 50,
        borderLeftWidth: 1,
        alignContent: 'center',
        borderLeftColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '20%',
        borderTopRightRadius: 16,
        borderBottomRightRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    showHideText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,

    },

    button: {
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'center',
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,
        marginHorizontal: 20
    },

    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontWeight:"600"

    },
    bottomView: {
        marginTop: 16,
        marginHorizontal: 20,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bottomText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        // fontFamily: "Samsung Sharp Sans"
    },
    bottomSecText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        // fontFamily: "Samsung Sharp Sans"
    },
    signupView: {
        marginHorizontal: 20,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative',
        bottom: 0,
        justifyContent: 'center'
    }
})