import React, { useState, useEffect } from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native'
import Images from '../../../utils/Images'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useMutation } from '@apollo/client'
import { LOGIN } from '../../../graphql/mutation/loginMutation/index'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../../../utils/loader/index'

export default function loginScreen({ navigation }) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isShow, setIsShow] = useState(true)
    const [errorName, setErrorName] = useState(null)
    const [passwordError, setPasswordError] = useState(null)

    const [userLogin, { data, loading, error }] = useMutation(LOGIN)




    const onChangeEmail = (text) => {
        setErrorName(null)
        setEmail(text)
    }




    const onLogin = () => {
        console.log('==================test log')

        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(email) === false) {
            setErrorName("Please enter valid email address")
        }
        else if (!email) {
            setErrorName("Plaese enter email address")
        }
        if (!password) {
            setPasswordError("Please enter your password")
        }
        else {
            userLogin({
                variables: {
                    email: email,
                    password: password
                },
            }).then(async (res) => {
                 console.log('==================res', res)
                await AsyncStorage.setItem("status", res.data.login.signup_status)
                await AsyncStorage.setItem("token", res.data.login.token)
                await AsyncStorage.setItem("user_id", res.data.login._id)

                // if (res.data.login.signup_status != "5") {
                //     alert(' Incorrect email and password')
                // }
                if (res.data.login.signup_status == "1") {
                    navigation.navigate("passwordScrreen")
                }

                else if (res.data.login.signup_status == "2") {
                    navigation.navigate("nameRegister")
                }
                else if (res.data.login.signup_status == "3") {
                    navigation.navigate("phoneVerificationScreen")
                }
                else if (res.data.login.signup_status == "4") {
                    navigation.navigate("phoneVerificationScreen")
                }
                else if (res.data.login.signup_status == "5") {
                    if(res.data.login.store_details!=undefined){
                        if(res.data.login.store_details._id==""){
                            navigation.navigate("store_create")
                        }else{
                        await AsyncStorage.setItem("store_id", res.data.login.store_details._id)

                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'userRegistration_bottomTabs' }],
                          });
                        }
                       // navigation.navigate("userRegistration_bottomTabs")
                    }
                    else {
                        navigation.navigate("store_create")
                    }                
                }else{
                       alert('Incorrect email and password')

                }
                
            }).catch((err)=>{alert('Incorrect email and password')})
        }
    }

    const onPasswordReset = () => {
        navigation.navigate("reset_password")
    }


    const onTogglePassword = () => {
        setIsShow(!isShow)
    }

    const onSignUpScreen = () => {
        navigation.navigate('emailScreen')
    }

    console.log('check', data, error)


    return (
        <SafeAreaView style={styles.container}>
            <Header
                leftText="Login" />

            {loading ?
                <Loader />
                :
                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={styles.main}>
                        <View>
                            <Image source={Images.LoginLogo} style={styles.logo} />
                        </View>

                        <KeyboardAwareScrollView>

                            <Text style={styles.headingText}>Welcome back!,</Text>
                            <Text style={styles.nextLineHeading} >Please login to your account to continue</Text>

                            <Text style={styles.inputBoxHeading}>Email Address</Text>
                            <TextInput
                                style={styles.input}
                                value={email}
                                onChangeText={onChangeEmail}

                            />
                            <Text style={styles.errorText}>{errorName}</Text>


                            <Text style={styles.inputBoxHeading}>Password</Text>
                            <View style={styles.InputViewStyle}>
                                <TextInput
                                    style={styles.passwordInput}
                                    value={password}
                                    secureTextEntry={isShow}
                                    onChangeText={(text) => {setPassword(text) ; setPasswordError(null) }}
                                />

                                <TouchableOpacity style={styles.showHidePassword}
                                    onPress={onTogglePassword}>
                                    {isShow ?
                                        <Text style={styles.showHideText}>Show</Text>
                                        :
                                        <Text style={styles.showHideText}>Hide</Text>
                                    }
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.errorText}>{passwordError}</Text>



                            <TouchableOpacity onPress={onLogin} style={styles.button}>
                                <Text style={styles.buttonText}>Login</Text>
                            </TouchableOpacity>

                            <View style={styles.bottomView}>
                                <Text style={styles.bottomText}>
                                    Forgot your password? </Text>
                                <TouchableOpacity onPress={onPasswordReset}>
                                    <Text style={styles.bottomSecText}> Reset password</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.signupView}>
                                <Text style={styles.bottomText}>
                                    Don't have an account? </Text>
                                <TouchableOpacity onPress={onSignUpScreen}>
                                    <Text style={styles.bottomSecText}>signup</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAwareScrollView >

                    </View>
                </ScrollView>
            }


        </SafeAreaView>
    )
}












// import React, { useState, useEffect } from 'react'
// import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native'
// import Images from '../../../utils/Images'
// import styles from './styles'
// import Header from '../../../utils/Header/Header'
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
// import { useMutation, useQuery } from '@apollo/client'
// import { LOGIN } from '../../../graphql/mutation/loginMutation/index'
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import Loader from '../../../utils/loader/index'
// import { GET_STORE } from '../../../graphql/queries/store/index'

// export default function loginScreen({ navigation }) {
//     const [email, setEmail] = useState('')
//     const [password, setPassword] = useState('')
//     const [isShow, setIsShow] = useState(true)
//     const [errorName, setErrorName] = useState(null)
//     const [passwordError, setPasswordError] = useState(null)
//     const [storeId, setStoreId] = useState('')
//     const [response, setResponse] = useState(null)

//     const [userLogin, { data, loading, error }] = useMutation(LOGIN)
//     const { data: storeData, loading: StoreLoading, error: storeError } = useQuery(GET_STORE);


//     useEffect(async () => {
//         await AsyncStorage.getItem("store_id").then((res) => setStoreId(res)).catch((err) => { console.log(err) })
//     })

//     const onChangeEmail = (text) => {


//         setErrorName(null)
//         setEmail(text)
//     }


//     console.log('-------response', response, 'data', storeData)
//     useEffect(() => {
//         const timer = setTimeout(() => {
//             if (response == 5) {
//                 if (storeData != undefined) {
//                     // AsyncStorage.setItem("store_id", storeData.getStores[0]._id)
//                     // navigation.navigate("userRegistration_bottomTabs")
//                     console.log('userRegistration_bottomTabs')
//                 }
//                 else {
//                     // navigation.navigate("store_create")
//                     console.log('store_create')

//                 }
//             }
//         }, 1000);
//         return () => clearTimeout(timer);

//     })


//     const onLogin = () => {
//         let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
//         if (reg.test(email) === false) {
//             setErrorName("Please enter valid email address")
//         }
//         else if (!email) {
//             setErrorName("Plaese enter email address")
//         }
//         if (!password) {
//             setPasswordError("Please enter your password")
//         }
//         else {
//             userLogin({
//                 variables: {
//                     email: email,
//                     password: password
//                 },
//             }).then(async (res) => {
//                 await AsyncStorage.setItem("status", res.data.login.signup_status)
//                 await AsyncStorage.setItem("token", res.data.login.token)
//                 await AsyncStorage.setItem("user_id", res.data.login._id)
//                 setResponse(res.data.login.signup_status)

//                 if (res.data.login.signup_status != "5") {
//                     alert(' Incorrect email and password')
//                 }


//                 //    else if (res.data.login.signup_status == "1") {
//                 //         navigation.navigate("passwordScrreen")
//                 //     }

//                 //    else if (res.data.login.signup_status == "2") {
//                 //         navigation.navigate("nameRegister")
//                 //     }
//                 //    else if (res.data.login.signup_status == "3") {
//                 //         navigation.navigate("phoneVerificationScreen")
//                 //     }
//                 //    else if (res.data.login.signup_status == "4") {
//                 //         navigation.navigate("phoneVerificationScreen")
//                 //     }

//                 //    else if (res.data.login.signup_status == "5") {
//                 //          navigation.navigate("store_create")
//                 //     }

//             })
//         }
//     }

//     const onPasswordReset = () => {
//         navigation.navigate("reset_password")
//     }


//     const onTogglePassword = () => {
//         setIsShow(!isShow)
//     }

//     const onSignUpScreen = () => {
//         navigation.navigate('emailScreen')
//         AsyncStorage.clear()
//     }

//     console.log('check', data, error)


//     return (
//         <SafeAreaView style={styles.container}>
//             <Header
//                 leftText="Login" />

//             {loading ?
//                 <Loader />
//                 :
//                 <ScrollView showsVerticalScrollIndicator={false}>

//                     <View style={styles.main}>
//                         <View>
//                             <Image source={Images.LoginLogo} style={styles.logo} />
//                         </View>

//                         <KeyboardAwareScrollView>

//                             <Text style={styles.headingText}>Welcome back!,</Text>
//                             <Text style={styles.nextLineHeading} >Please login to your account to continue</Text>

//                             <Text style={styles.inputBoxHeading}>Email Address</Text>
//                             <TextInput
//                                 style={styles.input}
//                                 value={email}
//                                 onChangeText={onChangeEmail}

//                             />
//                             <Text style={styles.errorText}>{errorName}</Text>


//                             <Text style={styles.inputBoxHeading}>Password</Text>
//                             <View style={styles.InputViewStyle}>
//                                 <TextInput
//                                     style={styles.passwordInput}
//                                     value={password}
//                                     secureTextEntry={isShow}
//                                     onChangeText={(text) => setPassword(text)}

//                                 />

//                                 <TouchableOpacity style={styles.showHidePassword}
//                                     onPress={onTogglePassword}>
//                                     {isShow ?
//                                         <Text style={styles.showHideText}>Show</Text>
//                                         :
//                                         <Text style={styles.showHideText}>Hide</Text>
//                                     }
//                                 </TouchableOpacity>
//                             </View>
//                             <Text style={styles.errorText}>{passwordError}</Text>



//                             <TouchableOpacity onPress={onLogin} style={styles.button}>
//                                 <Text style={styles.buttonText}>Login</Text>
//                             </TouchableOpacity>

//                             <View style={styles.bottomView}>
//                                 <Text style={styles.bottomText}>
//                                     Forgot your password? </Text>
//                                 <TouchableOpacity onPress={onPasswordReset}>
//                                     <Text style={styles.bottomSecText}> Reset password</Text>
//                                 </TouchableOpacity>
//                             </View>

//                             <View style={styles.signupView}>
//                                 <Text style={styles.bottomText}>
//                                     Don't have an account? </Text>
//                                 <TouchableOpacity onPress={onSignUpScreen}>
//                                     <Text style={styles.bottomSecText}>signup</Text>
//                                 </TouchableOpacity>
//                             </View>
//                         </KeyboardAwareScrollView >

//                     </View>
//                 </ScrollView>
//             }


//         </SafeAreaView>
//     )
// }

