import React from 'react'
import { SafeAreaView, Text, View, Image, TouchableOpacity } from 'react-native'
import styles from './styles'
import Images from '../../../utils/Images'

export default function Product_Verify({ navigation }) {

    const onNextScreen = () => {
        // navigation.navigate('product_offer',{productId, storeId} )
        navigation.navigate('userRegistration_bottomTabs')

    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.imageStyle}>
                <Image source={Images.productIcon} style={styles.image} />
                <Text style={styles.heading}>Product created successfully!</Text>
                <Text style={styles.title}>Create an offer to get more sales for your product</Text>
            </View>
            <View>
            <TouchableOpacity style={styles.button}
                onPress={onNextScreen}>
                <Text style={styles.buttonText}>Create an offer</Text>
                
            </TouchableOpacity>
            <Text style={styles.backButton} 
            onPress={()=> navigation.goBack()}>Go back</Text>
            </View>


        </SafeAreaView>
    )
}

