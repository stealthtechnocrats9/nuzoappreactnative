import { StyleSheet } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        justifyContent: 'space-evenly'
    }, 
    imageStyle: {
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center'
    },
    image: {
        height: 192, 
        width: 189,
        marginBottom: 52
    },
    heading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        fontWeight: '600',
        color: Color.black,
        // fontFamily: "samsung Sharp Sans"
    },
    title: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: '700',
        color: Color.black_80,
        // fontFamily: "samsung Sharp Sans"
    },
    button: {
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: "600"
    },
    backButton: {
        alignSelf: 'center', 
        marginTop: 29,
        color: Color.skyBlue,
        // fontFamily: "Samsung Sharp Sans",
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        fontWeight: "700"
    }
})