import React, { useEffect, useState } from 'react'
import { SafeAreaView, Text, Image, TouchableOpacity, View, TextInput, ScrollView, FlatList, Dimensions, Platform, PermissionsAndroid } from 'react-native'
import Images from '../../../utils/Images'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import { CREATE_PRODUCT } from '../../../graphql/mutation/products/index'
import Loader from '../../../utils/loader/index'
import { useMutation } from '@apollo/client'
import { ReactNativeFile } from 'apollo-upload-client'
import MultipleImagePicker from '@baronha/react-native-multiple-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage'


function generateRNFile(uri, type, name) {
    // console.log("i=uriÂ is", uri, type, name)
    return uri
        ? new ReactNativeFile({
            uri,
            type,
            name,
        })
        : null;
}



export default function Product_Create({ navigation }) {
    const [Store_id, setStoreId] = useState('')
    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [item, setItem] = useState('KSh')
    const [price, setPrice] = useState('')
    const [nameError, setNameError] = useState(null)
    const [priceError, setPriceError] = useState(null)
    const [descriptionError, setDescriptionError] = useState(null)
    const [images, setImages] = useState([]);
    const [createProduct, { data: createProductData, loading: createLoading, error: createError }] = useMutation(CREATE_PRODUCT)



    useEffect(async () => {
        await AsyncStorage.getItem("store_id").then((res) => setStoreId(res)).catch((err) => { console.log(err) })
    })

    const { width } = Dimensions.get('window');

    const IMAGE_WIDTH = (width - 24) / 3;

    // console.log("=====store id", Store_id)
    // const storeId = route.params.storeId




    const openPicker = async () => {
        try {
            const grantedstorage = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                  title: "App Storage Permission",
                  message:"App needs access to your storage to pic images ",
                  buttonNeutral: "Ask Me Later",
                  buttonNegative: "Cancel",
                  buttonPositive: "OK"
                }
              );
              if (grantedstorage ===  PermissionsAndroid.RESULTS.GRANTED) {
          

            const response = await MultipleImagePicker.openPicker({
                selectedAssets: images,
                isExportThumbnail: true,
                maxVideo: 1,
                usedCameraButton: false,
                isCrop: true,
                isCropCircle: true,
                // selectedColor: '#f9813a',
            });

            // console.log("before", response)
            response.map(image => {
                image.path =
                    Platform.OS === "android" ? "file://" + image.realPath : image.realPath;
                return image
            });
            // console.log(' Platform.OS', Platform.OS);
            // console.log('done: ', response);
            setImages(response);
        }else{
            alert("Please grant storage permission from settings to proceed.")
        }
        } catch (e) {
            // console.log(e);
        }
    
    };



    const onNextScreen = async () => {

        if (images.length < 1) {
            alert("Please add atleast one product image")
        }
        else if (images.length > 8) {
            alert("You can add maximum 7 image")
        } else if (!name) {
            setNameError("Please enter product name")
        }
        else if (address.length < 500) {
            setDescriptionError("Add minimum 500 characters")
        }
        else if (!price) {
            setPriceError("Please enter product price")
        } else {
            // console.log("onNextScreen productImage", images)
            let fileData = images.map(i => {
                let rimage = generateRNFile(i.path, i.mime, i.filename)
                // console.log("rimage", rimage)
                return rimage
            });
            // console.log("onNextScreen fileData", fileData)
            let vardata = {
                product_name: name,
                currency: item,
                product_description: address,
                product_price: price,
                store_id: Store_id,
                product_image: fileData
            }
            // console.log("onNextScreen vardata", vardata)
            // 
            await createProduct({
                variables: vardata
            }).then((res) => {
                if (res.data) {
                    navigation.navigate("product_verify")
                    // navigation.navigate('userRegistration_bottomTabs', { screen: 'ProductsScreen' })
                }
            }).catch((err) => { alert(err) })
        }
    }



    const onDelete = (value) => {
        const data = images.filter(
            (item) =>
                item?.localIdentifier &&
                item?.localIdentifier !== value?.localIdentifier
        );
        setImages(data);
    };


    const renderItemFunc = ({ item, index }) => {
        // console.log('item', item)
        // console.log('item', item.uri)

        return (
            <View>
                <Image
                    width={IMAGE_WIDTH}
                    source={{
                        uri: item?.type === 'video' ? item?.thumbnail ?? '' : item?.path,
                    }}
                    style={styles.productIcon} />

                <TouchableOpacity style={styles.deleteProductView}
                    onPress={() => onDelete(item)}>
                    <Image source={Images.deletePeoduct}
                        style={styles.deleteProduct} />
                </TouchableOpacity>
            </View>
        )
    };

    return (
        <ScrollView>
            <SafeAreaView style={styles.container}>
                <Header
                    leftIcon={Images.backArrow}
                    leftNextText="Create Product"
                    leftIconStyle={styles.leftIconStyle}
                    onLeftPress={() => navigation.goBack()}
                />



                {createLoading ?
                    <Loader />
                    :
                    <>
                        <View style={styles.mainView}>
                            <Text style={styles.textHeading}>Product image</Text>

                            <View style={styles.imageView}>
                                <TouchableOpacity onPress={openPicker} style={styles.cameraView}>
                                    <Image source={Images.cameraIcon} style={styles.cameraIcon} />
                                </TouchableOpacity>


                                <FlatList
                                    data={images}
                                    horizontal={true}
                                    renderItem={renderItemFunc}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsHorizontalScrollIndicator={false}
                                />

                            </View>
                        </View>

                        <View style={styles.mainView}>
                            <Text style={styles.textHeading}>Product name</Text>
                            <TextInput
                                style={styles.input}
                                value={name}
                                onChangeText={(text) => { setName(text); setNameError(null) }}
                            />
                        </View>
                        <Text style={styles.errorText}>{nameError}</Text>


                        <View style={styles.mainView}>
                            <Text style={styles.textHeading}>Product description</Text>
                            <TextInput
                                style={styles.descriptioninput}
                                value={address}
                                onChangeText={(text) => { setAddress(text); setDescriptionError(null) }}
                                multiline={true}
                            />
                        </View>
                        <Text style={styles.errorText}>{descriptionError}</Text>


                        <View style={styles.mainView}>
                            <View style={styles.priceTextView}>
                                <Text style={styles.textHeading}>Price</Text>
                                <Image source={Images.questionIcon} style={styles.questionIcon} />
                            </View>
                            <View style={styles.priceBoxStyle}>

                                <TextInput
                                    style={styles.inputItem}
                                    value={item}
                                    editable={false}
                                //                                    onChangeText={(text) => setItem(text)}
                                />

                                <TextInput
                                    style={styles.inputPrice}
                                    value={price}
                                    onChangeText={(text) => { setPrice(text); setPriceError(null) }}
                                    keyboardType='phone-pad'
                                />

                            </View>
                        </View>
                        <Text style={styles.errorText}>{priceError}</Text>

                        <TouchableOpacity style={styles.button}
                            onPress={onNextScreen}>
                            <Text style={styles.buttonText}>Create</Text>
                        </TouchableOpacity>
                    </>}
            </SafeAreaView>
        </ScrollView>
    )
}



// import React, { useEffect, useState } from 'react'
// import { SafeAreaView, Text, Image, TouchableOpacity, View, TextInput, ScrollView, FlatList, Pressable, Platform } from 'react-native'
// import Images from '../../../utils/Images'
// import styles from './styles'
// import Header from '../../../utils/Header/Header'
// import Color from '../../../utils/Colors'
// import { CREATE_PRODUCT } from '../../../graphql/mutation/products/index'
// import Loader from '../../../utils/loader/index'
// import { useMutation } from '@apollo/client'
// import { ReactNativeFile } from 'apollo-upload-client'
// import MultipleImagePicker from '@baronha/react-native-multiple-image-picker';
// import AsyncStorage from '@react-native-async-storage/async-storage'

// function generateRNFile(uri, type, name) {
    // console.log("i=uri is", uri, type, name)
//     return uri
//         ? new ReactNativeFile({
//             uri,
//             type,
//             name,
//         })
//         : null;
// }



// export default function Product_Create({ navigation }) {
//     const [name, setName] = useState('')
//     const [address, setAddress] = useState('')
//     const [item, setItem] = useState('Ksh')
//     const [price, setPrice] = useState('')
//     const [filePath, setFilePath] = useState(null);
//     const [productImage, setProductImage] = useState([]);
//     const [isShow, setIsShow] = useState(false)
//     const [modalVisible, setModalVisible] = useState(false);
//     const [nameError, setNameError] = useState(null)
//     const [priceError, setPriceError] = useState(null)
//     const [images, setImages] = useState([]);
//     const [Store_id, setStoreId] = useState('')
//     const [createProduct, { data: createProductData, loading: createLoading, error: createError }] = useMutation(CREATE_PRODUCT)


//     useEffect(async () => {
//         await AsyncStorage.getItem("store_id").then((res) => setStoreId(res))
//     })


    // console.log("=====", Store_id)
    // console.log("=====", images)

//     // const storeId = route.params.storeId

//     const openPicker = async () => {
//         try {
//             const response = await MultipleImagePicker.openPicker({
//                 selectedAssets: images,
//                 isExportThumbnail: true,
//                 maxVideo: 1,
//                 usedCameraButton: false,
//                 // selectedColor: '#f9813a',
//             });


//             response.map(image => {
//             image.path =
//           Platform.OS === "android" ? "file://" + image.path : image.path;
//             return image
//           });
        //   console.log(' Platform.OS',  Platform.OS );
            // console.log('done: ', response);
//             setImages(response);
//         } catch (e) {
            // console.log(e);
//         }
//     };



    // console.log('----productImage----', productImage.fileName)
//     const showMoreOption = () => {
//         setIsShow(!isShow)
//     }


//     const onNextScreen = () => {
//         if (!name) {
//             setNameError("Please enter product name")
//         }

//         if (!price) {
//             setPriceError("Please enter product name")
//         }
        // console.log("onNextScreen productImage",images)
//         let fileData = images.map(i => {
//           let rimage = generateRNFile(i.path, i.mime, i.filename)
        //   console.log("rimage",rimage)
//             return rimage
//         });
        // console.log("onNextScreen fileData",fileData)
//         let vardata =  {
//           product_name: name,
//           currency: item,
//           product_description: address,
//           product_price: price,
//           store_id: Store_id,
//           product_image: fileData
//       }
    //   console.log("onNextScreen vardata",vardata)
//         // navigation.navigate("product_verify")
//         createProduct({
//             variables: vardata
//         })
//     }

//     useEffect(() => {
//         if (createProductData != undefined) {
            // console.log('-----------success')
//             // navigation.navigate("product_verify", { productId: createProductData.createProduct._id, storeId: createProductData.createProduct.store_id })
//         }
//     })


    // console.log("-----createProductData", createProductData, "createError", createError)





//     const renderItemFunc = ({ item, index }) => {
        // console.log('item', item)
        // console.log('item', item.uri)

//         return (
//             <View style={{backgroundColor: 'white'}}>
//                 <Image
//                     source={{
//                         uri: item?.type === 'video' ? item?.thumbnail ?? '' : item?.path,

//                     }}
//                     style={styles.productIcon} />

//                 <TouchableOpacity style={styles.deleteProductView}
//                     // onPress={() => onDelete(item)}
//                     >
//                     <Image source={Images.deletePeoduct}
//                         style={styles.deleteProduct} />
//                 </TouchableOpacity>
//             </View>
//         )
//     };

//     return (
//         <ScrollView>
//             <SafeAreaView style={styles.container}>
//                 <Header
//                     leftText="Create Product"
//                 />
//                 {createLoading ?
//                     <Loader />
//                     :
//                     <>

//                         <View style={styles.mainView}>
//                             <Text style={styles.textHeading}>Product image</Text>

//                             <View style={styles.imageView}>
//                                 {/* <TouchableOpacity onPress={chooseFile} style={styles.cameraView}>
//                                     {filePath ?
//                                         <Image source={{ uri: filePath.uri }} style={styles.cameraIcon} />
//                                         :
//                                         <Image source={Images.cameraIcon} style={styles.cameraIcon} />}
//                                 </TouchableOpacity> */}

//                                 <TouchableOpacity onPress={openPicker} style={styles.cameraView}>
//                                     <Image source={Images.cameraIcon} style={styles.cameraIcon} />
//                                 </TouchableOpacity>


//                                 <FlatList
//                                     data={images}
//                                     horizontal={true}
//                                     renderItem={renderItemFunc}
//                                     keyExtractor={(item, index) => index.toString()}
//                                     showsHorizontalScrollIndicator={false}
//                                 />

//                             </View>
//                         </View>

//                         <View style={styles.mainView}>
//                             <Text style={styles.textHeading}>Product name</Text>
//                             <TextInput
//                                 style={styles.input}
//                                 value={name}
//                                 onChangeText={(text) => setName(text)}
//                             />
//                         </View>
//                         <Text style={styles.errorText}>{nameError}</Text>


//                         <View style={styles.mainView}>
//                             <Text style={styles.textHeading}>Product description</Text>
//                             <TextInput
//                                 style={styles.descriptioninput}
//                                 value={address}
//                                 onChangeText={(text) => setAddress(text)}
//                                 multiline={true}
//                             />
//                         </View>


//                         <View style={styles.mainView}>
//                             <View style={styles.priceTextView}>
//                                 <Text style={styles.textHeading}>Price</Text>
//                                 <Image source={Images.questionIcon} style={styles.questionIcon} />
//                             </View>
//                             <View style={styles.priceBoxStyle}>

//                                 <TextInput
//                                     style={styles.inputItem}
//                                     value={item}
//                                     onChangeText={() => setItem(text)}
//                                 />

//                                 <TextInput
//                                     style={styles.inputPrice}
//                                     value={price}
//                                     onChangeText={(text) => setPrice(text)}
//                                     keyboardType='phone-pad'
//                                 />

//                             </View>
//                         </View>
//                         <Text style={styles.errorText}>{priceError}</Text>

//                         <TouchableOpacity style={styles.button}
//                             onPress={onNextScreen}>
//                             <Text style={styles.buttonText}>Create</Text>
//                         </TouchableOpacity>
//                     </>}
//             </SafeAreaView>
//         </ScrollView>
//     )
// }



