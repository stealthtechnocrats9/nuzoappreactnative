import { StyleSheet } from 'react-native';
import { color } from 'react-native-reanimated';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
    },
    leftIconStyle: {
        height: 20, width: 20
    },
    rightNextIconStyle: {
        height: 20, width: 20
    },
    mainView: {
        marginHorizontal: 20,
        marginTop: 18.5
    },
    deleteBox: {
        paddingVertical: 7,
        width: 100,
        backgroundColor: Color.white,
        borderWidth: 1,
        borderRadius: 16,
        borderColor: Color.borderColor,
        position: 'absolute',
        right: 20,
        top: 40,
        alignItems: 'center',
        zIndex: 9
    },
    deleteButton: {
        width: '100%',
        paddingVertical: 10,
        alignItems: 'center',
    },
    borderView: {
        height: 1,
        width: 85,
        alignSelf: 'center',
        backgroundColor: Color.borderColor,
        marginVertical: 6
    },

    deleteButtonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight:"600"
    },
    textHeading: {
        color: Color.black_60,
        marginBottom: 6.5,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        // fontFamily: 'Samsung Sharp Sans',
    },
    imageView: {
        flexDirection: 'row',
        marginHorizontal: 10
    },
    cameraView: {
        height: 65,
        width: 65,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cameraIcon: {
        height: 64,
        width: 64
    },
    productIcon: {
        height: 64,
        width: 64,
        marginHorizontal: 10
    },
    deleteProductView: {
        height: 20,
        width: 20,
        position: 'absolute',
        right: 0,
        top: 0,
        zIndex: 1
    },
    deleteProduct: {
        height: 18.33,
        width: 18.33,
    },
    input: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        paddingHorizontal: 15,
        color: Color.skyBlue,
        fontSize: 15,
fontFamily: Fonts.Samsung,    },

    descriptioninput: {
        minHeight: 122,
        textAlignVertical: 'top',
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        paddingHorizontal: 15,
        color: Color.skyBlue,
        fontSize: 15,
fontFamily: Fonts.Samsung,    },
    priceTextView: {
        flexDirection: 'row',
    },
    questionIcon: {
        height: 13.5,
        width: 13.5,
        marginLeft: 6.3
    },

    priceBoxStyle: {
        flexDirection: 'row',
        width: 160,
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        justifyContent: 'center',
        alignContent: 'center',
    },
    inputItem: {
        width: 50,
        borderRightWidth: 1,
        borderColor: Color.skyBlue,
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        // fontFamily: 'Samsung Sharp Sans',
        textAlign: 'center'

    },
    inputPrice: {
        width: 100,
        paddingHorizontal: 10,
        alignItems: 'center',
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,

    },
    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        marginTop: 8,
        color: Color.red,
        marginLeft: 25,
        
    },

    button: {
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: "600"
    },
    modelView: {
        width: '100%',
        backgroundColor: Color.white,
        borderRadius: 10,
        paddingVertical: 25
    },
    modalHeading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        alignSelf: 'center',
        fontWeight: '600'
    },
    modalText: {
        alignSelf: 'center',
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 16,
        marginHorizontal: 50,
        textAlign: 'center',
        marginBottom: 41
    },
    modelButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 16

    },
    modelEditButton: {
        height: 52,
        borderColor: Color.red,
        width: 152,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelSendButton: {
        height: 52,
        width: 152,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight:"600"
    },
    modelActiveButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.white,
        fontWeight:"600"

    }

})