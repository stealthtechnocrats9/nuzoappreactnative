import React, { useEffect, useState } from 'react'
import { Text, View, TouchableOpacity, Image, FlatList, PermissionsAndroid, TextInput } from 'react-native'
import Images from '../../../utils/Images'
import styles from './contactStyle'
import Contacts from 'react-native-contacts';


export default function contactList({ navigation }) {
    const [allContacts, setAllContacts] = useState([])
    const [load, setLoading] = useState(true)
    const [isShow, setIsShow] = useState(false)
    const [showSearch, setShowSearch] = useState(false)
    const [searchItem, setSearchItem] = useState('')
    const [searchcontact, setSearchContact] = useState()



    useEffect(async () => {
        if (Platform.OS === "android") {
            const grantedcontact= await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
                title: "Contacts",
                message: "This app would like to view your contacts."
            })
            if(grantedcontact===PermissionsAndroid.RESULTS.GRANTED){
                loadContacts();
            }else{
                alert("Please grant permission to access contact list from settings");
            }
            
        } else {
            loadContacts();
        }
    }, [])


    // const myData = [].concat(allContacts)
    // .sort((a, b) => a.displayName > b.displayName ? 1 : -1)
    // .map((item, i) => 
    //     <div key={i}> {item.matchID} {item.timeM}{item.description}</div>
    // );


    // console.log('===========', myData)
    const loadContacts = () => {
        Contacts.getAll()
            .then(contacts => {
                let data = contacts.map((item) => {
                    item.isSelect = false
                    return { ...item }
                })
                let myData = [].concat(data)
                .sort((a, b) => a.displayName > b.displayName ? 1 : -1)

                setAllContacts(myData)
                // setSearchContact(data)
                setLoading(false)
            })
            .catch(e => {
                setLoading(false)

            });

        Contacts.checkPermission();
    }

    const ToggleSelect = () => {
        setIsShow(!isShow)
        let data = allContacts.map((item) => {
            item.isSelect = !item.isSelect
            return { ...item }
        })
        setAllContacts(data)
    }


    const selectOnItem = (ind) => {
        // console.log('ind val', ind)
        let select = allContacts.map((item, index) => {
            if (ind == index) {
                item.isSelect = !item.isSelect
            }
            return { ...item }
        })
        // let searchSelect = searchcontact.map((item, index) => {
        //     if (ind == index) {
        //         item.isSelect = !item.isSelect
        //     }
        //     // return { ...item }
        // })
        setAllContacts(select)
        // setSearchContact(searchSelect)
    }





    const renderItem = ({ item, index }) => {
        let firstString = item.displayName.charAt(0)

        console.log('item', item.isSelect)
        return (
            <TouchableOpacity style={styles.contactStyle}
                onPress={() => selectOnItem(index)}

            >
                {item.isSelect ?
                    <Image source={Images.contact_selected} style={styles.contactSelectIcon} />
                    :
                    <View style={styles.unSelectContactIcon}>
                        <Text style={styles.unSelectContactTextIcon}>{firstString}</Text>
                    </View>
                }
                <Text style={styles.contactName}>{item.displayName}</Text>

            </TouchableOpacity>
        )
    }

    const showSearchBox = () => {
        setShowSearch(!showSearch)
    }


    const setTextInputValue = (text) => {
        const filtercont = allContacts.filter(contact => {
            let contactLowerCase = (contact.displayName + { contact }).toLowerCase()
            console.log(contactLowerCase)
            let searchToLowerCase = text.toLowerCase()
            return contactLowerCase.indexOf(searchToLowerCase) > -1

        })

        console.log('=============', filtercont)

        setAllContacts(filtercont)
        setSearchItem(text)
    }


    console.log('searchitem is', searchcontact)

    const onConfirm = () => {
        // console.log('-----select contact', selectContact)
        let data = allContacts.filter((item) => item.isSelect == true)
        // console.log('filter data val', data)
        let selectContactData = data.map((item) => ({ contact_name: item.displayName, phone_no: item.phoneNumbers[0].number }))
        navigation.navigate("product_offer", { mobile: selectContactData })
    }



    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.left}>
                    <TouchableOpacity style={{ paddingVertical: 10, paddingHorizontal: 5 }}
                        onPress={() => navigation.goBack()}>
                        <Image source={Images.backArrow} style={styles.leftIconStyle} />
                    </TouchableOpacity>
                    <Text style={styles.headerText}>{"Select Contacts"}</Text>
                </View>
                <View style={styles.right}>
                    <TouchableOpacity onPress={showSearchBox} style={{ paddingVertical: 10, paddingLeft: 15, paddingRight: 5, }}>
                        <Image source={Images.searchIcon} style={styles.rightIconStyle} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onConfirm} style={{ padding: 4 }}>
                        <Text style={styles.headerRightText}>Done</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.logoStyle}>
                    <Image source={Images.mainlogo} style={styles.logo} />
                </View>
            </View>
            {
                showSearch &&
                <>
                    <TextInput
                        placeholder="search product"
                        value={searchItem}
                        onChangeText={(text) => setTextInputValue(text)}
                        style={styles.searchBox}
                    />
                    {isShow ?
                        <Text style={styles.heading} onPress={ToggleSelect}>Unselect all</Text>
                        :
                        <Text style={styles.heading} onPress={ToggleSelect}>Select all</Text>
                    }
                    <FlatList
                        data={allContacts}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </>
            }


            {
                !showSearch &&

                <View style={styles.main}>
                    {isShow ?
                        <Text style={styles.heading} onPress={ToggleSelect}>Unselect all</Text>
                        :
                        <Text style={styles.heading} onPress={ToggleSelect}>Select all</Text>
                    }

                    <FlatList
                        data={allContacts}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            }

        </View>
    )
}

