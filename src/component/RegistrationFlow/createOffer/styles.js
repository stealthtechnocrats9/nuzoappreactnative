import { StyleSheet } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        justifyContent: 'space-evenly'
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85,
    },
    main: {
        marginHorizontal: 20,
        marginTop: 18.5
    },
    aliasBox: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginTop: 6.5,
        marginBottom: 10,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight: "500",
        paddingHorizontal: 15
    },
    searchBoxView: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginTop: 6.5,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    searchBoxinsideText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight: "500"

    },
    search: {
        width: '15%',
        alignItems: 'center',
    },
    searchIcon: {
        height: 15,
        width: 15,
        tintColor: Color.skyBlue,
    },
    searchBox: {
        width: '85%',
        paddingRight: 15,
    },
    searchTextBox: {
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },
    boxView: {
        marginTop: 8.5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    inpurPriceView: {
        width: '50%',
    },
    priceHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        alignItems: 'center',
    },
    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        marginTop: 8,
        color: Color.red,

    },
    OfferView: {
        width: '24%',
        marginHorizontal: 5,
    },
    priceInputView: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginTop: 6.5,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginRight: 10
    },
    borderline: {
        height: '100%',
        width: 1,
        backgroundColor: Color.borderColor
    },
    searchPriceCurrencyBox: {
        width: '30%',
        // margin: 5,
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue
    },
    searchPriceBox: {
        width: '70%',
        paddingHorizontal: 15,
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },
    discountPrice: {
        fontSize: 15,
        marginTop: 6.5,
        justifyContent: 'center',
        textAlignVertical: 'center',
        fontFamily: Fonts.Samsung,
        color: Color.black,
        fontWeight: "bold",
        height: 52
    },
    calenderStyle: {
        marginTop: 18.5
    },
    calenderView: {
        flexDirection: 'row',
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginVertical: 6.5,
        width: '100%',
        alignSelf: 'center',
    },
    dateinput: {
        width: '85%',
        paddingHorizontal: 15,
        color: Color.skyBlue,
        justifyContent: 'center',
        textAlignVertical: 'center'

    },
    dateIconView: {
        width: '15%',
        justifyContent: 'center',
        alignItems: 'center',
        borderLeftWidth: 1,
        borderLeftColor: Color.borderColor
    },
    calenderIcon: {
        width: 16,
        height: 17.5
    },
    border: {
        height: 1,
        backgroundColor: Color.borderColor,
        marginTop: 32,
        marginBottom: 34
    },
    contactSearchView: {
        height: 52,
        borderRadius: 16,
        paddingLeft: 15,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginTop: 6.5,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },

    bottomView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 18.5
    },
    inActiveText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        fontWeight: "600"
    },
    activeText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight: "600"

    },
    selectedImage: {
        height: 239,
        width: 254,
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 21,
        borderWidth: 1,
        borderColor: Color.borderColor,
        borderRadius: 16

    },
    frameLink: {
        backgroundColor: Color.skyBlue,
        height: 53,
        width: '100%',
        // borderTopLeftRadius: 10,
        // borderTopRightRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    frame: {
        height: 166,
        width: 252,
        borderBottomLeftRadius: 16,
        borderBottomRightRadius: 16,
    },
    frameDetail: {
        height: 71,
        width: '96%',
        borderBottomLeftRadius: 16,
        borderBottomRightRadius: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    frameDetailText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black
    },
    frameLinkText: {
        color: Color.white,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },
    editTemplate: {
        minHeight: 130,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginVertical: 6.5,
        paddingHorizontal: 15
    },
    button: {
        marginVertical: 40,
        backgroundColor: Color.skyBlue,
        height: 52,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        color: Color.white,
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: '600'
    },
    modelView: {
        width: '100%',
        backgroundColor: Color.white,
        borderRadius: 16,
        paddingVertical: 25
    },
    modalHeading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        alignSelf: 'center',
        fontWeight: "600"

    },
    modalText: {
        alignSelf: 'center',
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 16,
        marginBottom: 41
    },
    modelButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 16

    },
    modelEditButton: {
        height: 52,
        borderWidth: 1,
        borderColor: Color.red,
        width: 152,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelSendButton: {
        height: 52,
        backgroundColor: Color.skyBlue,
        width: 152,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelEditButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.red,
        fontWeight: "600"

    },
    modelSendButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.white,
        fontWeight: "600"
    }

})