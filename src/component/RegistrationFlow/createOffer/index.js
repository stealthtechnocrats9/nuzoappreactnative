import React, { useState, useEffect } from 'react'
import {
    Text, TextInput, View, Image, TouchableOpacity, Platform, ScrollView, Linking
} from 'react-native'
import Header from '../../../utils/Header/Header'
import Images from '../../../utils/Images'
import styles from './styles'
import Modal from 'react-native-modal';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment'
import Color from '../../../utils/Colors'
import { useMutation, useQuery } from '@apollo/client'
import { CREATE_OFFER, PRODUCT_IMAGE } from '../../../graphql/mutation/createOffer/index'
// import { GET_SEARCH_PRODUCT } from '../../../graphql/queries/getProduct/index'
import Loader from '../../../utils/loader/index'
import Fonts from '../../../assets/fonts'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { isTypeAliasDeclaration } from 'typescript'

export default function Product_offer({ navigation, route }) {
    //console.log('----------route value is', route.params)

    const productId = route.params.productId
    const editData = route.params.editData

    const [storeId, setStoreId] = useState('')
    const selectedContect = route.params.mobile
    const product = route.params.product
    const [alias, setAlias] = useState('')
    const [productData, setProductData] = useState('')
    const [product_image, setproduct_image] = useState(null)
    const [product_id, setProduct_id] = useState('')
    const [price, setPrice] = useState('')
    const [contact, setContact] = useState(null)
    const [offerDetail, setOfferDetail] = useState(product != undefined ? 'Get Ksh' + ' ' + price + ' ' + 'off your order when you buy' + ' ' + product.product_name : 'Get Ksh 0 off your order when you buy')
    const [isShow, setIsShow] = useState(false)
    const [modalVisible, setModalVisible] = useState(false);
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [offerCreate, { data, loading, error }] = useMutation(CREATE_OFFER)
    const { data: imageData, error: imageError } = useQuery(PRODUCT_IMAGE, {
        variables: {
            product_id: product_id
        }
    });

   // console.log('price', price)
    useEffect(async () => {
        await AsyncStorage.getItem("store_id").then((res) => setStoreId(res)).catch((err) => { console.log(err) })
    })



    // const { data: searchProducts, error: searchError } = useQuery(GET_SEARCH_PRODUCT, {
    //     variables: {
    //         product_name: search
    //     }
    // })
    const [searchProduct, setSearchProduct] = useState(null)
    const [discounts, setDiscounts] = useState('')
    const [select, setSelect] = useState([])
    const [contactLength, setContactLength] = useState(0)
    const [errorMessage, setErrorMessage] = useState(null)



    useEffect(() => {

        if (select == undefined) {
            setSelect(0)
        }
        if (selectedContect != undefined) {
            let contactData = JSON.stringify(selectedContect)
            let contactData1 = JSON.stringify(contactData)
            setContact(contactData1)
        }
        if (selectedContect != undefined) {
            setContactLength(selectedContect.length)
        }

        if (productData != undefined) {
            let id = productData._id
            setProduct_id(id)
        }

    })

    useEffect(() => {
        if (product != undefined) {
            setProductData(product)
            setOfferDetail('Get Ksh' + ' ' + price + ' ' + 'off your order when you buy' + ' ' + product.product_name)
            setproduct_image('https://nuzo.co/product_images/' + product.product_image[0].product_image)
        }

    }, [product])



    const contactList = () => {
        // setShowContact(true)
        navigation.navigate("contactList")
    }



    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };
  //  console.log('-----------storeId-', productData.product_price)


    const onChangePrice = (text) => {
        if (product == undefined) {
            alert('Please select product first')
        }
        else {
            setPrice(text)
            let discountvalue = productData.product_price - text
            setOfferDetail('Get Ksh' + ' ' + discountvalue + ' ' + 'off your order when you buy' + ' ' + product.product_name)
            if(text==''){
                setDiscounts(0);

            }else{
                setDiscounts(discountvalue);
            }
            // if (productData.product_price > price) {
            //     setPrice(text)
            // }
            // else {
            //     setPrice(null)
            // }

        }

    }

   // console.log('-----search', productData)

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };


    const editTemplate = () => {
        setIsShow(!isShow)
    }

    const actualDate = moment(date).format("MM-DD-YYYY")

    const formatDate = (date) => {
        return `${date.getDate()}/${date.getMonth() +
            1}/${date.getFullYear()}`;
    };

    // let discount = product.product_price - price
    // let productId = product._id
    // let product_price = product.product_price


    const onSearchProduct = () => {
        navigation.navigate("select_product")
    }

   // console.log('camp screen id', storeId)

    const send_Campaign = () => {
        // navigation.navigate("select_product")
        if (discounts < 0) {
            alert("Offer price must be less then or equal to regular price")
        }
        else if (!price) {
            alert("Please add offer price")
        }
        else if (!contact) {
            setErrorMessage("Please select contacts")
        }
        else if (contact=='"[]"') {
            setErrorMessage("Please select contacts")
        }
        else {

            offerCreate({
                variables: {
                    product_id: productData._id,
                    store_id: storeId,
                    //compaign_alias: "alias",
                    product_price: productData.product_price,
                    discount: discounts.toString(),
                    offer_price: price,
                    message: offerDetail,
                    product_url: "https://app.nuzo.co/",
                    total_contacts: contactLength.toString(),
                    contacts: contact,
                    expire_date: actualDate

                },
            }).then((res) => {
                navigation.navigate("campaign_created", { storeId })
            }).catch((err) => { alert(err) })
        }
        setModalVisible(false)
    }

    const updateOffer = () => {
        alert('update')
    }




    return (
        <>
            {loading ?
                <Loader />
                :
                <ScrollView>
                    <Header
                        leftIcon={Images.backArrow}
                        leftNextText="Create An Offer"
                        onLeftPress={() => navigation.goBack()}
                        leftIconStyle={styles.menuLeftIcon}
                    />


                    <View style={styles.main}>


                        {/* <Text style={styles.priceHeading}>Alias name</Text>
                        <TextInput 
                         value={alias}
                         style={styles.aliasBox}
                         onChangeText={(text) => setAlias(text)}/> */}

                        <Text style={styles.priceHeading}>Select product</Text>
                        <TouchableOpacity style={styles.searchBoxView} onPress={onSearchProduct}>
                            <View style={styles.search} >
                                <Image source={Images.searchIcon} style={styles.searchIcon} />
                            </View>
                            <View style={styles.searchBox}>
                                <Text style={styles.searchBoxinsideText}>{product == undefined ? "Please select product" : product.product_name}</Text>
                            </View>

                        </TouchableOpacity>

                        <View style={styles.boxView}>
                            <View style={styles.inpurPriceView}>
                                <Text style={styles.priceHeading}>Promotional Price</Text>

                                <View style={styles.priceInputView}>
                                    <Text style={styles.searchPriceCurrencyBox}>KSh</Text>
                                    <View style={styles.borderline} />
                                    <TextInput
                                        style={styles.searchPriceBox}
                                        value={price}
                                        onChangeText={(text) => onChangePrice(text)}
                                        keyboardType="phone-pad"
                                    />
                                </View>
                            </View>

                            <View style={styles.OfferView}>
                                <Text style={styles.priceHeading}>Regular</Text>
                                <Text style={styles.discountPrice}>KSh {product == undefined ? 0 : product.product_price}</Text>

                            </View>

                            <View style={styles.OfferView}>
                                <Text style={styles.priceHeading}>Discount</Text>
                                <Text style={styles.discountPrice}>KSh {product == undefined ? 0 : discounts}</Text>
                            </View>
                        </View>


                        <View style={styles.calenderStyle}>
                            <Text style={styles.priceHeading}>Promotion expiry date</Text>
                            <View style={styles.calenderView}>
                                <Text style={[styles.dateinput, { fontSize: 15, fontFamily: Fonts.Samsung }]}>
                                    {formatDate(date)}
                                </Text>
                                <TouchableOpacity style={styles.dateIconView}
                                    onPress={showDatepicker}>
                                    <Image source={Images.calenderIcon} />
                                </TouchableOpacity>
                                {show && (
                                    <DateTimePicker
                                        testID="dateTimePicker"
                                        value={date}
                                        mode={mode}
                                        minimumDate={new Date()}
                                        is24Hour={true}
                                        display="default"
                                        onChange={onChange}
                                    />
                                )}

                            </View>
                        </View>

                        <View style={styles.border} />


                        <Text style={styles.priceHeading}>Select contacts</Text>
                        <TouchableOpacity style={styles.contactSearchView} onPress={contactList}>
                            <View style={styles.searchBox}>
                                <Text style={styles.searchTextBox}>{contactLength} contacts selected</Text>
                            </View>
                            <View style={styles.search}>
                                <Image source={Images.searchIcon} style={styles.searchIcon} />
                            </View>
                        </TouchableOpacity>
                        <Text style={styles.errorText}>{errorMessage}</Text>



                        <View style={styles.bottomView}>
                            <Text onPress={() => alert('back')}
                                style={styles.inActiveText}>Preview</Text>
                            <Text onPress={editTemplate}
                                style={styles.activeText}>Edit Message</Text>

                        </View>

                        <View style={styles.selectedImage}>
                            <View style={styles.frameDetail}>
                                <Text style={styles.frameDetailText}> {offerDetail}</Text>
                            </View>
                            <Image source={product_image == null ? null : { uri: product_image }} style={styles.frame} />
                            {/* <View style={styles.frameLink}>
                                <Text style={styles.frameLinkText}>https://abestore.com/75-off-dettol-eventone-antinacterial-soap-90g</Text>
                            </View> */}
                        </View>

                        {isShow &&
                            <View style={{ marginTop: 22 }}>
                                <Text style={styles.priceHeading}>Message</Text>
                                <TextInput
                                    style={[styles.editTemplate, , { fontSize: 15, color: Color.black, fontFamily: Fonts.Samsung }]}
                                    value={offerDetail}
                                    multiline={true}
                                    textAlignVertical="top"
                                    onChangeText={(text) => setOfferDetail(text)}
                                />
                            </View>
                        }

                        <View>
                            {editData ?
                                <TouchableOpacity style={styles.button}
                                    onPress={updateOffer} >
                                    <Text style={styles.buttonText}>Update</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.button}
                                    onPress={() => {
                                        console.log('----------contact----', contact.length)

                                        if (product == undefined) {
                                            alert('Please select product first')
                                        } else if (discounts < 0) {
                                            alert("Promotion price must be less then or equal to regular price")
                                        }
                                        else if (!price) {
                                            alert("Please add promotion price")
                                        }
                                        else if (!contact) {
                                            alert("Please select contacts ")
                                        }
                                        else if (contact=='"[]"') {
                                            alert("Please select contacts")
                                        }else {
                                            setModalVisible(true)
                                        }

                                    }} >
                                    <Text style={styles.buttonText}>Send to {contactLength} contacts</Text>
                                </TouchableOpacity>
                            }
                            <Modal
                                testID={'modal'}
                                isVisible={modalVisible}
                                swipeDirection={['left']}
                                transparent={true}
                                propagateSwipe={true}
                                onBackdropPress={() => setModalVisible(false)}
                            >
                                <View style={styles.modelView}>
                                    <Text style={styles.modalHeading}>Send campaign?</Text>
                                    <Text style={styles.modalText}> Are you ready to send your campaign out?</Text>

                                    <View style={styles.modelButton}>
                                        <TouchableOpacity
                                            onPress={() => setModalVisible(false)}
                                            style={styles.modelEditButton}>
                                            <Text style={styles.modelEditButtonTextStyle}>Keep editing</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={send_Campaign}
                                            style={styles.modelSendButton}>
                                            <Text style={styles.modelSendButtonTextStyle}>Send campaign</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </Modal>
                        </View>
                    </View>
                </ScrollView>
            }
        </>
    )
}



