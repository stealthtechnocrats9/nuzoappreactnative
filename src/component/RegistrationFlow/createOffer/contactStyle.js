import Color from "../../../utils/Colors";
import { StyleSheet } from "react-native";
import { Colors } from "react-native/Libraries/NewAppScreen";
import Fonts from "../../../assets/fonts";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    header: {
        backgroundColor: Color.skyBlue_5,
        height: 60,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        // paddingHorizontal: 21,
        paddingLeft: 21,
        paddingRight: 21,
        marginBottom: 8

    },
    searchBox: {
        height: 52,
        borderWidth: 1,
        borderRadius: 16,
        marginHorizontal: 15,
        paddingLeft: 15,
        borderColor: Color.borderColor,
        color: Color.black_60
    },
    left: {
        width: '48%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    right: {
        width: '48%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    logoStyle: {
        position: 'absolute',
        right: 80,
        bottom: 0
    },
    logo: {
        height: 49,
        width: 71
    },
    headerText: {
         fontSize: 20,
         fontFamily: Fonts.Samsung,
         fontWeight: "600",
         color: Color.black,
         marginLeft: 8
    }, 
    headerRightText: {
        fontSize: 16,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight: "600",

    },
    leftIconStyle: {
        height: 15,
        width: 15,
    },
    rightIconStyle: {
        height: 16,
        width: 15.85,
        marginRight: 10
    },
    main: {
        paddingHorizontal: 20
    },
    heading: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        marginTop: 16,
        fontWeight: "600",

        marginBottom: 10
    },
    contactStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10
    },
    contactName: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        color: Colors.black
    },
    contactSelectIcon: {
        height:30,
        width: 30,
        marginRight: 20
    },
    unSelectContactIcon: {
        backgroundColor: Color.black_60,
        marginRight: 20,
        height: 30,
        width: 30,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    unSelectContactTextIcon: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        textTransform: 'capitalize'
       
    }
})