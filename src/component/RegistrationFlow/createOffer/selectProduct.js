import React, { useState, useEffect } from 'react'
import { Text, View, FlatList, Image, TouchableOpacity, ScrollView, TextInput } from 'react-native'
import Header from '../../../utils/Header/Header'
import Images from '../../../utils/Images'
import styles from './styles'
import { useQuery } from '@apollo/client'
import { GET_ALL_PRODUCT, GET_SEARCH_PRODUCT } from '../../../graphql/queries/getProduct/index'
import Loader from '../../../utils/loader/index'
import AsyncStorage from '@react-native-async-storage/async-storage'


export default function selectProduct({ navigation }) {
    const [storeId, setStoreId] = useState('')
    const [showSearch, setShowSearch] = useState(false)
    const [searchItem, setSearchItem] = useState('')
    const { data, loading } = useQuery(GET_ALL_PRODUCT,{
        variables: {
            store_id: storeId
        }
        });
    const { data: searchProducts, error: searchError } = useQuery(GET_SEARCH_PRODUCT, {
        variables: {
            product_name: searchItem
        }
    })


    const [allProduct, setAllProduct] = useState([])
    const [searchProduct, setSearchProduct] = useState([])

  

    // useEffect(async() => {
    //     await AsyncStorage.getItem("store_id").then((res) => setStoreId(res))
    //     if (data) {
    //         setAllProduct(data.products)
    //     }
    // })


    console.log('======store id is', storeId)

    const setTextInputValue = (text) => {
        setSearchItem(text)
        if (searchProducts) {
            setSearchProduct(searchProducts.searchProducts)
        }

    }

    const showSearchBox = () => {
        setShowSearch(!showSearch)
    }

    const offerCreate = () => {

    }


    console.log('product data is============', searchProducts)
    console.log('product data is===searchProducts=========', searchItem)
    const numberOfItem = searchProduct.length


    // console.log('product data is============', allProduct)
    const onEditProduct = (item) => {
        navigation.navigate('Product_Update', { item })
    }


    const renderItem = ({ item }) => {
        let img = item.product_image
        console.log('img', img)
        return (
            <TouchableOpacity style={styles.flatlistView}
                onPress={() => onEditProduct(item)}>
                <View style={styles.titleView}>
                    {img[0] ?
                       <Image source={{ uri: 'https://nuzo.co/product_images/' + img[0].product_image}}
                        style={{ height: 35, width: 35, marginRight: 10, borderRadius: 4 }} /> 
                        : <View style={{ height: 35, width: 35, marginRight: 10, borderRadius: 4 }} /> }

                    <Text style={styles.titleText}>{item.product_name}</Text>
                </View>


                <View style={styles.priceView}>
                    <Text style={styles.priceText}>{item.currency}  {item.product_price}</Text>
                </View>

            </TouchableOpacity>
        )
    };



    return (
        <View style={styles.container}>
            {loading ?
                <Loader />
                :
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Header
                        leftIcon={Images.backArrow}
                        leftNextText="Select Product"
                        onLeftPress={() => navigation.goBack()}
                        leftIconStyle={styles.menuLeftIcon}
                        rightNextSecIcon={Images.searchIcon}
                        onRightNextPress={showSearchBox}
                        rightNextIconStyle={styles.rightNextIconStyle} />



                    {
                        showSearch &&

                        <TextInput
                            placeholder="search products"
                            value={searchItem}
                            onChangeText={(text) => setTextInputValue(text)}
                            style={styles.searchBox}
                        />
                    }

                    {numberOfItem > 1 ?
                        <View style={styles.main}>
                            <FlatList
                                data={searchProduct}
                                showsVerticalScrollIndicator={false}
                                renderItem={renderItem}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>

                        :
                        <View style={styles.main}>
                            <FlatList
                                data={allProduct}
                                showsVerticalScrollIndicator={false}
                                renderItem={renderItem}
                                style={{ marginBottom: 50 }}
                                keyExtractor={(item, index) => index.toString()}
                            />

                        </View>
                    }


                </ScrollView>
            }
        </View>
    )
}


// const styles = StyleSheet.create({
    
// })
