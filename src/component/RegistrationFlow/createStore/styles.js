import { StyleSheet, Dimensions } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        
    },


    mainView: {
        marginHorizontal: 20,
        marginTop: 18.5
    },
    camraView: {
        fontSize: 15,
        color: Color.red,
        fontFamily: Fonts.Samsung,
    },
    imageoptionmodel: {
        paddingTop: 20,
        backgroundColor: Color.white,
        borderWidth: 1,
        borderColor: Color.black_20,
        paddingLeft:  20,
        paddingRight:  20,
        shadowColor: "#000000",
        // borderRadius: 13,
        shadowOpacity: 0.5,
        shadowRadius: 12,
        shadowOffset: {
            height: 2,
            width: 2
        },
        zIndex:1,
        elevation: 10
    },
    takePhoto: {
        marginVertical: 10,
    },
    headingModalButton: {
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: Fonts.Samsung,
        marginBottom: 10
    }, 
    ModalOptionButton: {
        fontSize: 18,
        fontWeight: '600',
        fontFamily: Fonts.Samsung,
    },
    cancelButton: {
        fontSize: 15,
        fontWeight: 'bold',
        fontFamily: Fonts.Samsung,
        textAlign: 'right'
    }, 
    textHeading: {
        color: Color.black_60,
        marginBottom: 6.5,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        // fontFamily: 'Samsung Sharp Sans',
    },
    cameraView: {
        height: 65,
        width: 65,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cameraIcon: {
        height: 64,
        width: 64
    },
    input: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        paddingHorizontal: 15,
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },

    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        marginTop: 8,
        color: Color.red,
        marginLeft: 25,

    },

    instruction: {
        marginTop: 20,
        marginHorizontal: 20,
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: 'normal',
        color: Color.black,
        // fontFamily: 'Samsung Sharp Sans'
    },
    checkInstruction: {
        color: Color.skyBlue
    },
    imageView: {
        flexDirection: 'row',
        marginHorizontal: 10,
        alignItems: 'center',
    },
    productIcon: {
        height: 64,
        width: 64,
        borderRadius: 5,
        marginHorizontal: 10,

    },
    deleteProduct: {
        height: 18.33,
        width: 18.33,
        position: 'absolute',
        right: 3,
        top: 0,
    },
    button: {
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: "600"
    }

})