import React, { useEffect, useState } from 'react'
import { SafeAreaView, Text, Image, TouchableOpacity, View, TextInput, KeyboardAvoidingView, Platform, PermissionsAndroid, Pressable, Alert } from 'react-native'
import Images from '../../../utils/Images'
import styles from './styles'
import {
  launchCamera,
  launchImageLibrary
} from 'react-native-image-picker';
import Modal from 'react-native-modal'
import Header from '../../../utils/Header/Header'
import { useMutation } from '@apollo/client'
import { CREATE_STORE } from '../../../graphql/mutation/createStore/index'
import { ReactNativeFile } from 'apollo-upload-client'
import Loader from '../../../utils/loader/index'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-async-storage/async-storage'
import Color from '../../../utils/Colors'

function generateRNFile(uri, type, name) {
  console.log("i=uri is", uri, 'type', type, 'name', name)
  return uri
    ? new ReactNativeFile({
      uri,
      type,
      name,
    })
    : null;
}


const Store_Create = ({ navigation }) => {
  const [imageOption, setImageOption] = useState(false)
  const [name, setName] = useState('')
  const [address, setAddress] = useState('')
  const [payment, setPayment] = useState('')
  const [merchantName, setMerchantName] = useState('')
  const [addressError, setAddressError] = useState(null)
  const [imageError, setimageError] = useState('')
  const [nameError, setnameError] = useState(null)
  const [paymentError, setPaymentError] = useState(null)
  const [merchantNameError, setmerchantNameError] = useState(null)
  const [opacity, setOpacity] = useState('0.7')

  const [createStoreFun, { data, loading, error }] = useMutation(CREATE_STORE)

  const [image, setImage] = useState(null);

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'Store_Create needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'Store_Create needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };


  const openImagePicker = async () => {
    setimageError(null);
    setImageOption(!imageOption)

  }


  const captureImage = async (type) => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      videoQuality: 'low',
      durationLimit: 30, //Video max duration in seconds
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      launchCamera(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        else {
          let source = response
          // let source = {
          //   uri: 'data:image/jpeg;base64,' + response.uri
          // };
          console.log('source...', source)

          source.path =
            Platform.OS === "android" ? "file://" + source.path : source.path;
          setImage(source.assets[0]);
          setImageOption(false)
          console.log('source...adfgggg', image)
        }
      });
    }
  };

  const chooseFile = (type) => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      else {
        let source = response
        // let source = {
        //   uri: 'data:image/jpeg;base64,' + response.uri
        // };
        console.log('source...', source)

        source.path =
          Platform.OS === "android" ? "file://" + source.path : source.path;
        setImage(source.assets[0]);
        setImageOption(false)
        console.log('source...adfgggg', image)
      }



    });
  };


  console.log('image===============', image)
  const onNextScreen = () => {
    if (image == null) {
      setimageError("Please add store image")
    }

    else if (!name) {
      setnameError("Please enter store name")
    }
    else if (!address) {
      setAddressError("Please enter shop address")
    }
    else if (!merchantName) {
      setmerchantNameError("Please enter marchant name")
    }
    else if (!payment) {
      setPaymentError("Please enter payment number")
    }

    else {
      let fileData = generateRNFile(image.uri, image.type, image.fileName);
      createStoreFun({
        variables: {
          store_name: name,
          shop_address: address,
          store_image: fileData,
          payment_option: payment,
          marchant_name: merchantName
        }
      }).then((res) => {
        AsyncStorage.setItem("store_id", res.data.createStoreFun._id)
        // navigation.navigate("store_verify", { id: res.data.createStoreFun._id })
        navigation.reset({
          index: 0,
          routes: [{ name: 'store_verify' }],
        });

      }).catch((err) => {
        alert(err);
      })

    }
  }

  console.log('data', data)
  console.log('error', error)

  return (
    <>
      <SafeAreaView style={styles.container}>
        <Header leftText="Create Store" />
        {loading ?
          <Loader />
          :
          <KeyboardAwareScrollView>
            <View style={styles.mainView}>
              <Text style={styles.textHeading}>Store Photo</Text>
              <View style={styles.camraView}>
                <TouchableOpacity onPress={openImagePicker} style={styles.cameraView}>
                  {image ?
                    <Image source={{ uri: image.uri }} style={styles.cameraIcon} />
                    :
                    <Image source={Images.cameraIcon} style={styles.cameraIcon} />
                  }
                </TouchableOpacity>
              </View>
            </View>


            <Text style={styles.errorText}>{imageError}</Text>


            <View style={styles.mainView}>
              <Text style={styles.textHeading}>Store name</Text>
              <TextInput
                style={styles.input}
                value={name}
                onChangeText={(text) => { setName(text); setnameError(null) }}
              />
            </View>
            <Text style={styles.errorText}>{nameError}</Text>


            <View style={styles.mainView}>
              <Text style={styles.textHeading}>Shop Address</Text>
              <TextInput
                style={styles.input}
                value={address}
                onChangeText={(text) => { setAddress(text); setAddressError(null) }}
              />
            </View>
            <Text style={styles.errorText}>{addressError}</Text>


            <View style={styles.mainView}>
              <Text style={styles.textHeading}>Merchant name for payment</Text>
              <TextInput
                style={styles.input}
                value={merchantName}
                onChangeText={(text) => { setMerchantName(text); setmerchantNameError(null) }}
              />
            </View>
            <Text style={styles.errorText}>{merchantNameError}</Text>


            <View style={styles.mainView}>
              <Text style={styles.textHeading}>Payment mobile number</Text>
              <TextInput
                style={styles.input}
                value={payment}
                onChangeText={(text) => { setPayment(text); setPaymentError(null) }}
                keyboardType="numeric"
              />
            </View>
            <Text style={styles.errorText}>{paymentError}</Text>



            <Text style={styles.instruction}>By proceeding, you agree to the
              <Text style={styles.checkInstruction} onPress={() => alert('terms and condition')}>Terms and Conditions</Text> and
              <Text style={styles.checkInstruction} onPress={() => alert('terms and condition')}> Privacy Policy</Text>.</Text>


            <TouchableOpacity style={styles.button}
              onPress={onNextScreen}>
              <Text style={styles.buttonText}>Create store</Text>
            </TouchableOpacity>

          </KeyboardAwareScrollView>}
      </SafeAreaView>

      <Modal
        testID={'modal'}
        animationType="slide"
        transparent
        visible={imageOption}
        transparent
        propagateSwipe={true}
        backdropColor='rgba(0,0,0,0.9)'
        backdropOpacity={opacity}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setImageOption(!imageOption);
        }}
      >
        <View style={styles.imageoptionmodel}>
          <Text style={styles.headingModalButton}>Select Image</Text>

          <TouchableOpacity style={styles.takePhoto}
            onPress={() => captureImage('photo')}>
            <Text style={styles.ModalOptionButton}>Take Photo</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.takePhoto}
            onPress={() => chooseFile('photo')}>
            <Text style={styles.ModalOptionButton}>Choose from Library</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.takePhoto}
            onPress={() => setImageOption(false)}>
            <Text style={styles.cancelButton}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </>
  )
}

export default Store_Create

