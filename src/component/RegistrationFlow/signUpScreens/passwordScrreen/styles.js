import { StyleSheet } from 'react-native';
import Fonts from '../../../../assets/fonts';
import Color from '../../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        alignItems: 'center',
        justifyContent: 'space-between'

    },
    logo: {
        marginTop: 92,
        height: 35,
        width: 151,
        marginBottom: 100
    },
    main: {
        width: '100%',
        paddingVertical: 15,
        justifyContent: 'space-between',
    },
    headingText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: 'bold'
    },
    nextLineHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: 'bold'
    },
    inputBoxHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: 'Samsung Sharp Sans',
        marginTop: 35
    }, 
    InputViewStyle: {
        flexDirection: 'row',
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginVertical: 6.5,
        width: '90%',
        alignSelf: 'center'

    },
    input: {
        height: 50,
        borderRadius: 16,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '80%',
        paddingHorizontal: 10,
        color: Color.black,
        fontSize: 15,
        fontFamily: Fonts.Samsung,

    } ,
    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        marginTop: 8,
        color: Color.red,
        marginLeft: 25,
        
    },
    showHidePassword: {
        height: 50,
        borderLeftWidth: 1,
        alignContent: 'center',
        borderLeftColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '20%',
        borderTopRightRadius: 16,
        borderBottomRightRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
    } ,
    showHideText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
       
    },
    continueView: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 20,
        marginTop: 17,
        alignItems: 'center',
        width: 90,
        height: 25,
        alignSelf: 'flex-end'
    },
    nextLogo: {
        width: 13.33,
        height: 13.33,
        marginRight: 11
    },
    continueText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight:"600"
    },
    bottomView: {
        marginTop: 40,
        marginHorizontal: 20,
        marginBottom: 6,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bottomText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        // fontFamily: "Samsung Sharp Sans"
    },
    bottomSecText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        // fontFamily: "Samsung Sharp Sans"
    },
    border: {
        height: 3,
        backgroundColor: Color.skyBlue,
        width: '33.3%',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0
    }
})