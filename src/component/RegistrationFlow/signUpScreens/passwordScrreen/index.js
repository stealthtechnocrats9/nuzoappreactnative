
import React, { useState, useEffect } from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, KeyboardAvoidingView, ScrollView } from 'react-native'
import styles from './styles'
import Images from '../../../../utils/Images'
import { useMutation } from '@apollo/client'
import { CREATE_PASSWORD } from '../../../../graphql/mutation/SignUpMutation/userSignUp'
import Loader from '../../../../utils/loader'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function PasswordScrreen({ navigation }) {


    const [password, setPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState(null)
    const [isShow, setIsShow] = useState(true)
    const [userId, setUserId] = useState('')
    const [addPassword, { data, error, loading }] = useMutation(CREATE_PASSWORD)

    useEffect(async () => {
        await AsyncStorage.getItem("user_id").then((res) => setUserId(res))
    })
    console.log('----------userId', userId)

    const onNextScreen = () => {
        if (!password) {
            setErrorMessage("Password is required")
        }
        else if (password.length <= 5) {
            setErrorMessage("Minimum 6 character is required")
        }
        else {
            addPassword({
                variables: {
                    user_id: userId,
                    password: password
                }
            }).then((res) => {
                if (res.data.stepTwoSignUp.signup_status == "2") {
                   // navigation.navigate("nameRegister")
                    AsyncStorage.setItem("status", res.data.stepTwoSignUp.signup_status)
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'nameRegister'}],
                      });
                    
                }
            }).catch((err)=> {alert(err)})

        }
    }

    console.log('data', data)

    const onLogin = () => {
        navigation.navigate('login_Screen')
    }
    const onChangeText = (text) => {
        setPassword(text)
        setErrorMessage(null)
    }

    const onTogglePassword = () => {
        setIsShow(!isShow)
    }

    return (
        <SafeAreaView style={styles.container}>
            {loading
                ?
                <Loader />
                :
                <>
                    <View>
                        <Image source={Images.logo} style={styles.logo} />
                    </View>
                    <View style={styles.main}>
                        <ScrollView>
                            <Text style={styles.headingText}>We got your email,</Text>
                            <Text style={styles.nextLineHeading} >Create a password you’ll use to login</Text>

                            <Text style={styles.inputBoxHeading}>Password</Text>

                            <View style={styles.InputViewStyle}>
                                <TextInput
                                    style={styles.input}
                                    value={password}
                                    secureTextEntry={isShow}
                                    onChangeText={onChangeText}
                                />
                                <TouchableOpacity style={styles.showHidePassword}
                                    onPress={onTogglePassword}>
                                    {isShow ?
                                        <Text style={styles.showHideText}>Show</Text>
                                        :
                                        <Text style={styles.showHideText}>Hide</Text>
                                    }
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.errorText}>{errorMessage}</Text>

                            <TouchableOpacity onPress={onNextScreen} style={styles.continueView}>
                                <Image source={Images.nextArrow} style={styles.nextLogo} />
                                <Text style={styles.continueText}>Continue</Text>
                            </TouchableOpacity>

                            <View style={styles.bottomView}>
                                <Text style={styles.bottomText}>
                                    Already have an account? </Text>
                                <TouchableOpacity onPress={onLogin}>
                                    <Text style={styles.bottomSecText}>Log in</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>

                    <View style={styles.border} />
                </>
            }

        </SafeAreaView>

    )
}


