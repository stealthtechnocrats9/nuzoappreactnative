import React, { useState } from 'react'
import { Text, View, Image, FlatList, TouchableOpacity } from 'react-native'
import styles from './styles'
import Header from '../../../../utils/Header/Header'
import Images from '../../../../utils/Images'
import { countryCode } from './country'
import Fonts from '../../../../assets/fonts'

export default function Country_ModelBox({ navigation }) {

    const [selectItem, setSelectItem] = useState({name: 'Kenya', flag: '🇰🇪', code: 'KE', dial_code: '+254'})
    const [selectIndex, setSelectIndex] = useState(113)


    const selectCountry = (item, index) => {
        setSelectIndex(index)
        setSelectItem(item)
        navigation.navigate("phoneVerificationScreen",{ item : item})
    }



    console.log('=====', selectItem)

    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => selectCountry(item, index)}
                style={styles.countryDesign}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontSize: 25, marginRight: 13, fontFamily: Fonts.Samsung }}>{item.flag}</Text>
                    <Text style={styles.countryNameText}>{item.name}</Text>
                    <Text style={styles.countryNameText}>({item.dial_code})</Text>

                </View>
                {selectIndex == index &&
                    <Image source={Images.tick} style={{ height: 10.22, width: 14.05 }} />
                }
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.modelView}>
            <Header
                leftIcon={Images.backArrow}
                leftNextText="Select a country/region"
                onLeftPress={() => navigation.navigate("phoneVerificationScreen",{ item : setSelectItem})}
                leftIconStyle={styles.menuLeftIcon}
            />

            <FlatList
                data={countryCode}
                showsVerticalScrollIndicator={false}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    )
}


