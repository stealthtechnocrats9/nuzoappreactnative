import { StyleSheet } from 'react-native';
import Fonts from '../../../../../assets/fonts';
import Color from '../../../../../utils/Colors';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white

    },
    menuLeftIcon: {
        height: 16,
        width: 15.85
    },
    main: {
        marginHorizontal: 20,
    },
  
    logo: {
        alignSelf: 'center',
        height: 46.59,
        width: 46.64,
        marginVertical: 23.4
    },
    heading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        alignSelf: 'center',

    },
    title: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        alignSelf: 'center',

    },
    inputStyles: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginHorizontal: 10,
        marginTop: 40,
        marginBottom: 20

    },
    input: {
        height: 44,
        width: 44,
        borderColor: Color.borderColor,
        borderWidth: 1,
        borderRadius: 10,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5
    },
    instruction: {
        alignSelf: 'center',
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black_60
    },
    optText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: "700",
        color: Color.skyBlue,
        height: 44,
        width: 44,
        textAlign: 'center',
        textAlignVertical: 'center',

    },

    activeInstruction: {
        color: Color.skyBlue,
        fontFamily: Fonts.Samsung,

    },
    button: {
        backgroundColor: Color.skyBlue,
        height: 52,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20
    },
    buttonText: {
        fontFamily: Fonts.Samsung,

        color: Color.white,
        fontSize:17,
        fontWeight:"600"
    },
})