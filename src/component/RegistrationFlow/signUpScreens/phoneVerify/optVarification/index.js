import React, { useState, useEffect, useRef } from 'react'
import { Text, TouchableOpacity, View, Image, ScrollView, TextInput, KeyboardAvoidingView, Alert } from 'react-native'
import Header from '../../../../../utils/Header/Header'
import Images from '../../../../../utils/Images'
import styles from './styles'
import { useMutation } from '@apollo/client'
import { OTP_VERIFY, RESEND_OTP } from '../../../../../graphql/mutation/SignUpMutation/userSignUp'
import Loader from '../../../../../utils/loader'
import AsyncStorage from '@react-native-async-storage/async-storage'


export default function Otp_Varification({ navigation, route }) {

    const [pins, setPins] = useState('')
    const phonenumber = route.params.phone
    const pinLength = 6
    let textInput = useRef(null)
    const [verify_Otp, {data, error, loading }] = useMutation(OTP_VERIFY)
    const [userId, setUserId] = useState('')
    const [resend_otp, {data: resendOtpData, error: resendOtpError, loading: resendOtpLoading }] = useMutation(RESEND_OTP)

    useEffect(async () => {
        await AsyncStorage.getItem("user_id").then((res) => setUserId(res))
    })
    


   const lastindex = phonenumber.slice(-3)
    console.log('----', pins)
    console.log('route', userId)

    // console.log("====================",phonenumber.length)

    const onNextScreen = () => {
        // navigation.navigate("verify_account")
        if(!pins){
            alert("Please enter OTP")
        }else if(pins.length!=6){
            alert("Please enter valid OTP")

        }else{
        console.log(pins, userId)
        verify_Otp({
            variables: {
                user_id: userId,
                otp: pins,
            },
        }) .then((res) => {
            if (res.data.verifyOTP.signup_status == "5") {
                AsyncStorage.setItem("status", res.data.verifyOTP.signup_status)
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'verify_account'}],
                  });
                
               // navigation.navigate("verify_account")
            }
        }).catch((err)=> {alert("Please Enter Valid OTP")})
    }
    }



    

    const resendOtp = () => {
        Alert.alert(
            "",
            "You must enable SMS from our short code. To enable, dial *456*9*5# Enabling SMS is free and does not attract additional charges.",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress:  otpResendSuccess }
            ]
          );
     
    }

    const otpResendSuccess = () => {
        resend_otp({
            variables: {
                user_id: userId,
            },
        }).then((res)=> console.log(res))
        .catch((err) => alert(err))
    }

    console.log("====================data check", resendOtpData, resendOtpError)
    const onChangeText = (text) => {
        setPins(text)
    }

    useEffect(() => {
        // textInput.focus()
        setTimeout(() => { textInput.current.focus()}, 100)
    },[])

    return (
        <View>
            <Header
                leftIcon={Images.backArrow}
                leftNextText="OTP Verification"
                onLeftPress={() => navigation.goBack()}
                leftIconStyle={styles.menuLeftIcon}
            />
            {loading || resendOtpLoading ?
            <Loader />
            :
            <KeyboardAvoidingView 
                keyboardVerticalOffset= {50}
                behavior="padding"
                style={styles.main}>

                <Image source={Images.otp} style={styles.logo} />
                <Text style={styles.heading}>Enter your authentication code to proceed</Text>
                <Text style={styles.title}>Type the code sent to  *** **** *** {lastindex} in the field below.</Text>
             

                <TextInput
                    maxLength={pinLength}
                    style={{width: 0, height: 0}}
                    returnKeyType="done "
                    keyboardType="number-pad"
                    value={pins}
                    onChangeText={onChangeText}
                    ref={textInput} />

                <View style={styles.inputStyles}>
                    {
                        Array(pinLength).fill().map((data, index) => (
                            <View
                                key={index}
                                style={styles.input}>
                                <Text style={styles.optText}
                                 onPress={() => textInput.current.focus()}
                                 >
                                    {pins && pins.length > 0 ? pins[index] : ""}
                                </Text>
                            </View>
                        ))
                    }

                </View>

               
                <Text style={styles.instruction}>It may take a minute to receive your code.</Text>
                <Text style={styles.instruction}>Haven’t received it?
                    <Text style={styles.activeInstruction} onPress={resendOtp}>
                        Resend a new code</Text></Text>

                <TouchableOpacity style={styles.button}
                    onPress={onNextScreen}>
                    <Text style={styles.buttonText}>Verify</Text>
                </TouchableOpacity>
                </KeyboardAvoidingView>}
        
        </View>
    )
}

