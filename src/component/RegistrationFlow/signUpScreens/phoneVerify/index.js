import React, { useState, useEffect } from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, ScrollView, PixelRatio, Switch } from 'react-native'
import Images from '../../../../utils/Images'
import styles from './styles'
import { useMutation } from '@apollo/client'
import { ADD_PHONE } from '../../../../graphql/mutation/SignUpMutation/userSignUp'
import Fonts from '../../../../assets/fonts'
import Loader from '../../../../utils/loader'
import AsyncStorage from '@react-native-async-storage/async-storage'


export default function PhoneVerificationScreen({ navigation, route }) {

    const countryData = route.params.item
    const firstname = route.params.firstname

    const [phone, setPhone] = useState('')
    const [selectItem, setSelectItem] = useState('')
    const [userId, setUserId] = useState('')
    const [errorMessage, setErrorMessage] = useState(null)
    const [addPhoneNumber, { data, error, loading }] = useMutation(ADD_PHONE)
    const [country_code, setCountry_code] = useState('+254')
    const [country_name, setCountry_name] = useState('ke')

    useEffect(async () => {
        await AsyncStorage.getItem("user_id").then((res) => setUserId(res))
    })
    console.log('----------userId', userId, countryData)



    useEffect(() => {
        if (countryData) {
            setCountry_code(countryData.dial_code)
            setSelectItem(countryData.flag)
            setCountry_name(countryData.code.toLowerCase())

        }
    }, [countryData])

    const onNextScreen = () => {
        if (!phone) {
            setErrorMessage("Please enter phone number")
        }
        else if (!country_code) {
            setErrorMessage("Please select country code")
        }else if (!country_name) {
            setErrorMessage("Please select country code again")
        }else {
            addPhoneNumber({
                variables: {
                    user_id: userId,
                    phone: phone,
                    country_code: country_code,
                    country_shortCode: country_name
                },
            })
                .then((res) => {
                        if( res.data.stepFourSignUp == null){
                            setErrorMessage("Plese enter a valid phone number")
                        }
                        else if (res.data.stepFourSignUp.signup_status == "4") {
                            AsyncStorage.setItem("status", res.data.stepFourSignUp.signup_status)
                            navigation.navigate("otp_varification", {phone})

                        }
                }).catch((err) => { alert(err) })
        }

    }
    // console.log("-------data3", data, '--', error, userId)



    const onLogin = () => {
        navigation.navigate('login_Screen')
    }
    const onChangeText = (text) => {
        setPhone(text)
        setErrorMessage(null)
    }

    return (
        <SafeAreaView style={styles.container}>
            {loading ?
                <Loader />
                :
                <>
                    <View>
                        <Image source={Images.logo} style={styles.logo} />
                    </View>
                    <View style={styles.main}>
                        <ScrollView>
                            <Text style={styles.headingText}>Hi {firstname},</Text>
                            <Text style={styles.nextLineHeading} >We need your number to alert you about orders</Text>


                            <Text style={styles.inputBoxHeading}>Phone number</Text>
                            <View style={styles.phoneBoxStyle}>
                                <TouchableOpacity onPress={() => navigation.navigate('Country_ModelBox')}
                                    style={styles.countryView}>
                                    {selectItem ?
                                        <Text style={{ fontSize: 20, fontFamily: Fonts.Samsung }}>{selectItem}</Text>
                                        :
                                        <Image source={Images.flag} style={styles.flag} />
                                    }
                                    <Image source={Images.dropdown} style={styles.dropdown} />
                                </TouchableOpacity>
                                <Text style={styles.countrycode}>{country_code}</Text>
                                <TextInput
                                    style={styles.input}
                                    value={phone}
                                    onChangeText={onChangeText}
                                    keyboardType="phone-pad"
                                    maxLength = {15}
                                />
                            </View>
                            <Text style={styles.errorText}>{errorMessage}</Text>


                            <TouchableOpacity onPress={onNextScreen} style={styles.continueView}>
                                <Image source={Images.nextArrow} style={styles.nextLogo} />
                                <Text style={styles.continueText}>Continue</Text>
                            </TouchableOpacity>

                            <View style={styles.bottomView}>
                                <Text style={styles.bottomText}>
                                    Already have an account? </Text>
                                <TouchableOpacity onPress={onLogin}>
                                    <Text style={styles.bottomSecText}>Log in</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>

                    <View style={styles.border} />
                </>}

        </SafeAreaView>
    )
}

