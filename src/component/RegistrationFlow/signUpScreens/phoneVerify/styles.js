import { StyleSheet } from 'react-native';
import Fonts from '../../../../assets/fonts';
import Color from '../../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        alignItems: 'center',
        justifyContent: 'space-between',
        // borderBottomWidth: 8,
        // borderBottomColor: 'red',
    },
    logo: {
        marginTop: 92,
        height: 35,
        width: 151,
        marginBottom: 100
    },
    main: {
        width: '100%',
        paddingVertical: 15,
        justifyContent: 'space-between',
    },
    headingText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: 'bold'
    },
    nextLineHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: 'bold'
    },
    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        marginTop: 8,
        color: Color.red,
        marginLeft: 25,
        
    },
    inputBoxHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: 'Samsung Sharp Sans',
        marginTop: 35
    },

    phoneBoxStyle: {
        flexDirection: 'row',
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginHorizontal: 20,
        paddingHorizontal: 10,
        marginTop: 6.5,

    },
    countryView: {
        flexDirection: 'row',
        width: 68.8,
        borderColor: Color.borderColor,
        borderRightWidth: 1,
        borderRightColor: Color.skyBlue_5,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        paddingHorizontal: 5
    },
    flag: {
        height: 18,
        width: 20
    },
    dropdown: {
        height: 7.41,
        width: 12
    },
    countrycode: {
        alignSelf: 'center',
        alignItems: 'center',
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        marginLeft: 10
    },
    input: {
        width: '60%',
        alignSelf: 'center',
        alignItems: 'center',
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },

    continueView: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 20,
        marginTop: 17,
        alignItems: 'center',
        width: 90,
        height: 25,
        alignSelf: 'flex-end'
    },
    nextLogo: {
        width: 13.33,
        height: 13.33,
        marginRight: 11
    },

    modelView: {
        flex: 1,
        backgroundColor: Color.white
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85,
    },
    countryDesign: {
        flexDirection: 'row',
        height: 43,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 2,
        marginHorizontal: 10,
        paddingHorizontal: 12
    },
    countryNameText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        color: Color.skyBlue,
        marginHorizontal: 2
    },
    continueText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight:"600"
    },
    bottomView: {
        marginTop: 40,
        marginHorizontal: 20,
        marginBottom: 6,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bottomText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        // fontFamily: "Samsung Sharp Sans"
    },
    bottomSecText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        // fontFamily: "Samsung Sharp Sans"
    },
    border: {
        height: 3,
        backgroundColor: Color.skyBlue,
        width: '100%',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0
    }
})