import React, { useState, useEffect } from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native'
import { useMutation } from '@apollo/client'
import { CREATE_EMAIL } from '../../../../graphql/mutation/SignUpMutation/userSignUp'
import Images from '../../../../utils/Images'
import styles from './styles'
import Loader from '../../../../utils/loader'
import AsyncStorage from '@react-native-async-storage/async-storage'


const EmailScreen = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [errorMessage, setErrorMessage] = useState(null)
    // const dispatch = useDispatch();
    const [addEmail, { data, error, loading }] = useMutation(CREATE_EMAIL)
    // if (error) return `Submission error! ${error.message}`


    const onNextScreen = () => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (!email) {
            setErrorMessage('Please enter email address')
        }
        else if (reg.test(email) == false){
            setErrorMessage('Please enter a valid email address')
        }
        else {
            addEmail({
                variables: {
                    email: email
                }
            }).then((res) => {
                if (res.data.stepOneSignUp.signup_status != "1") {
                  alert('This email is already registered. Please login.')
                }
                if (res.data.stepOneSignUp.signup_status == "1") {
                    AsyncStorage.setItem("status", res.data.stepOneSignUp.signup_status)
                    AsyncStorage.setItem("user_id", res.data.stepOneSignUp._id)
                    AsyncStorage.setItem("token", res.data.stepOneSignUp.token)
                    navigation.navigate("passwordScrreen")
                }
              
            }).catch((err)=> {alert(err)})
        }
    }




    console.log('data1------', loading, data)



    const onLogin = () => {
        navigation.navigate('login_Screen')
    }

    const onChangeText = (text) => {
            setEmail(text)
            setErrorMessage(null)
        }

    

    return (
        <SafeAreaView style={styles.container}>
            {loading ?
                <Loader />
                :
                <>
                    <View>
                        <Image source={Images.logo} style={styles.logo} />
                    </View>

                    <View style={styles.main}>

                        <ScrollView>
                            {/* <Loader /> */}

                            <Text style={styles.headingText}>Hi,</Text>
                            <Text style={styles.nextLineHeading} >Let’s get you started with an account</Text>

                            <Text style={styles.inputBoxHeading}>Email</Text>
                            <TextInput
                                style={styles.input}
                                value={email}
                                onChangeText={onChangeText}
                            />
                            <Text style={styles.errorText}>{errorMessage}</Text>

                            <TouchableOpacity onPress={onNextScreen} style={styles.continueView}>
                                <Image source={Images.nextArrow} style={styles.nextLogo} />
                                <Text style={styles.continueText}>Continue</Text>
                            </TouchableOpacity>

                            <View style={styles.bottomView}>
                                <Text style={styles.bottomText}>
                                    Already have an account? </Text>
                                <TouchableOpacity onPress={onLogin}>
                                    <Text style={styles.bottomSecText}>Log in</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>

                </>
            }
        </SafeAreaView>
    )
}

export default EmailScreen


