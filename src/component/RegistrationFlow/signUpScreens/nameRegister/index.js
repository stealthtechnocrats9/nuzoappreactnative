import React, { useState, useEffect } from 'react'
import { SafeAreaView, Image, ScrollView, Text, View, TextInput, TouchableOpacity } from 'react-native'
import Images from '../../../../utils/Images'
import styles from './styles'
import { useMutation } from '@apollo/client'
import { CREATE_NAME } from '../../../../graphql/mutation/SignUpMutation/userSignUp'
import Loader from '../../../../utils/loader'
import AsyncStorage from '@react-native-async-storage/async-storage'


const NameRegister = ({ navigation }) => {

   
    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [firstnameError, setFirstnameError] = useState(null)
    const [lastnameError, setLastnameError] = useState(null)
    const [userId, setUserId] = useState('')

    const [addUserName, { data, error, loading }] = useMutation(CREATE_NAME)

    useEffect(async () => {
        await AsyncStorage.getItem("user_id").then((res) => setUserId(res))
    })
    console.log('----------userId', userId)

    const onNextScreen = () => {
        console.log(firstname, lastname)
        if(!firstname){
            setFirstnameError("Please enter first name")
        }else if(!lastname){
            setLastnameError("Please enter last name")
        }else{
        addUserName({
            variables: {
                user_id: userId,
                firstname: firstname,
                lastname: lastname
            },
        }).then((res) => {
            if (res.data.stepThreeSignUp.signup_status == "3") {
                navigation.navigate("phoneVerificationScreen", {firstname})
                AsyncStorage.setItem("status", res.data.stepThreeSignUp.signup_status)
            }
        }).catch((err)=> {alert(err)})
    }
    }

    // useEffect(() => {
    //     if (data != undefined) {
    //         console.log('data', data)
    //         let id = data.stepThreeSignUp._id
    //         navigation.navigate("phoneVerificationScreen", { id })
    //     }
    // })

    console.log("-------data3", data, userId)


    const onLogin = () => {
        navigation.navigate('login_Screen')
    }

    return (
        <SafeAreaView style={styles.container}>
            {loading ?
                <Loader />
                :
                <>
                    <View>
                        <Image source={Images.logo} style={styles.logo} />
                    </View>
                    <View style={styles.main}>
                        <ScrollView>
                            <Text style={styles.headingText}>We’ll like to know a bit about you,</Text>
                            <Text style={styles.nextLineHeading} >What do we call you?</Text>



                            <View style={styles.TextInputView}>
                                <View style={styles.setBoxInput}>
                                    <Text style={styles.inputBoxHeading}>First name</Text>
                                    <TextInput
                                        style={styles.input}
                                        value={firstname}
                                        onChangeText={(text) => {setFirstname(text);setFirstnameError(null)} }
                                    />
                                    <Text style={styles.errorText}>
                                        {firstnameError}
                                    </Text>
                                </View>
                                <View style={styles.setBoxInput}>
                                    <Text style={styles.inputBoxHeading}>Last name</Text>
                                    <TextInput
                                        style={styles.input}
                                        value={lastname}
                                        onChangeText={(text) => {setLastname(text);setLastnameError(null)}}
                                    />
                                     <Text style={styles.errorText}>
                                        {lastnameError}
                                    </Text>
                                    
                                </View>
                            </View>
                            <TouchableOpacity style={styles.continueView}
                                onPress={onNextScreen}>
                                <Image source={Images.nextArrow} style={styles.nextLogo} />
                                <Text style={styles.continueText}>Continue</Text>
                            </TouchableOpacity>

                            <View style={styles.bottomView}>
                                <Text style={styles.bottomText}>
                                    Already have an account? </Text>
                                <TouchableOpacity onPress={onLogin}>
                                    <Text style={styles.bottomSecText}>Log in</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                    <View style={styles.border} />
                </>
            }
        </SafeAreaView>

    )
}

export default NameRegister;
