import { StyleSheet } from 'react-native';
import Fonts from '../../../../assets/fonts';
import Color from '../../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        alignItems: 'center',

    },
    backIcon: {
        height: 16,
        width: 15.85
    },

    main: {
        width: '100%',
        paddingHorizontal: 20,

    },
    headingText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: 'bold',
        marginTop: 12,
        marginBottom: 4
    },

    inputBoxHeading: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        // fontFamily: 'Samsung Sharp Sans',
        fontWeight: 'bold',
        marginTop: 18.5,
        marginBottom: 6.5,
    },

    input: {
        height: 44,
        borderRadius: 10,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        paddingHorizontal: 10,
        color: Color.black,
        fontSize: 10,
        fontFamily: Fonts.Samsung,
    },
    InputViewStyle: {
        flexDirection: 'row',
        height: 44,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginVertical: 6.5,
        width: '100%',
        alignSelf: 'center'

    },
    passwordInput: {
        height: 40,
        borderRadius: 10,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '85%',
        paddingHorizontal: 10,
        color: Color.black,
        fontSize: 10,
        fontFamily: Fonts.Samsung,
    },
    showHidePassword: {
        height: 42,
        borderLeftWidth: 1,
        alignContent: 'center',
        borderLeftColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '15%',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    showHideText: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,

    },

    button: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center',
        height: 44,
        backgroundColor: Color.skyBlue,
        borderRadius: 10,
        marginHorizontal: 20
    },

    buttonText: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText
    },

})