import React, { useState } from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native'
// import styles from './styles'
import Images from '../../../../utils/Images'
import styles from './styles'
import Header from '../../../../utils/Header/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default function Set_NewPassword({ navigation }) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isShow, setIsShow] = useState(true)

    const onLogin = () => {
        navigation.navigate('store_create')
    }

    const onTogglePassword = () => {
        setIsShow(!isShow)
    }

    return (
        <SafeAreaView style={styles.container}>
                <Header
                    leftIcon={Images.backArrow}
                    leftIconStyle={styles.backIcon}
                    onLeftPress={() => navigation.goBack()}
                    leftNextText="Reset Password" />

                <View style={styles.main}>

                        <Text style={styles.headingText}>Enter your new password</Text>

                        <Text style={styles.inputBoxHeading}>Email Address</Text>
                        <TextInput
                            style={styles.input}
                            value={email}
                            onChangeText={(text) => setEmail(text)}

                        />

                        <Text style={styles.inputBoxHeading}>Password</Text>
                        <View style={styles.InputViewStyle}>
                            <TextInput
                                style={styles.passwordInput}
                                value={password}
                                secureTextEntry={isShow}
                                onChangeText={(text) => setPassword(text)}

                            />
                            <TouchableOpacity style={styles.showHidePassword}
                                onPress={onTogglePassword}>
                                {isShow ?
                                    <Text style={styles.showHideText}>Show</Text>
                                    :
                                    <Text style={styles.showHideText}>Hide</Text>
                                }
                            </TouchableOpacity>
                        </View>


                        <TouchableOpacity onPress={onLogin} style={styles.button}>
                            <Text style={styles.buttonText}>Reset password</Text>
                        </TouchableOpacity>

                </View>
        </SafeAreaView>
    )
}

