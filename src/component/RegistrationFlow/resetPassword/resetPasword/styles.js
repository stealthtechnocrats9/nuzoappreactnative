import { StyleSheet } from 'react-native';
import Fonts from '../../../../assets/fonts';
import Color from '../../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        alignItems: 'center',
    },
    successPage: {
        flex: 1,
        backgroundColor: Color.white,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backIcon: {
        height: 16,
        width: 15.85
    },
    logo: {
        height: 210,
        width: 210,
        alignSelf: 'center'

    },
    main: {
        width: '100%',
        paddingVertical: 15,
    },
    headingText: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: '600',
        marginTop: 50, marginBottom: 8
    },
    nextLineHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        marginBottom: 4
    },
    inputBoxHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: 'Samsung Sharp Sans',
        marginTop: 18.5,
        marginBottom: 6.5,
    },

    input: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginHorizontal: 20,
        paddingHorizontal: 10,
        color: Color.black,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },
    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        marginTop: 8,
        color: Color.red,
        marginLeft: 25,

    },

    button: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center',
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,
        marginHorizontal: 20
    },

    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontWeight: "600"
    },
    bottomView: {
        marginTop: 16,
        marginHorizontal: 20,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bottomText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        // fontFamily: "Samsung Sharp Sans"
    },
    bottomSecText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        // fontFamily: "Samsung Sharp Sans"
    }
})