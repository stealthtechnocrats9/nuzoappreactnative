import React, { useState } from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, ScrollView, Alert } from 'react-native'
// import styles from './styles'
import Images from '../../../../utils/Images'
import styles from './styles'
import Header from '../../../../utils/Header/Header'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { FORGOT_PASSWORD } from '../../../../graphql/mutation/loginMutation'
import { useMutation } from '@apollo/client'


export default function ResetPassword({ navigation }) {
    const [email, setEmail] = useState('')
    const [forgotPassword, { data, loading, error }] = useMutation(FORGOT_PASSWORD)
    const [errorName, setErrorName] = useState(null)

    const onLogin = () => {
        navigation.navigate('login_Screen')
    }

    const onConfirm = () => {

        if (!email) {
            setErrorName('Please enter email address')
        }
        else {
            forgotPassword({
                variables: {
                    email: email,
                },
            }).then((res) => {
                Alert.alert(
                    "Alert Title",
                    "Password link sent your email address please check ",
                    [
                      { text: "OK", onPress: () => navigation.navigate("login_Screen") }
                    ]
                  );
                // console.log('----res', res)
                
            }).catch((err)=>{
                alert("Invalid Email Id")
            })
        }
    }
    console.log('data', data, "email", email)
    return (
        <SafeAreaView style={styles.container}>
       
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Header
                        leftIcon={Images.backArrow}
                        leftIconStyle={styles.backIcon}
                        onLeftPress={() => navigation.goBack()}
                        leftNextText="Reset Password" />
                    <View style={styles.main}>
                        <View>
                            <Image source={Images.reset_password} style={styles.logo} />
                        </View>

                        <KeyboardAwareScrollView>

                            <Text style={styles.headingText}>Reset password</Text>
                            <Text style={styles.nextLineHeading} >Enter the email associated with your account and we will send you and email with instructions to reset your password</Text>

                            <Text style={styles.inputBoxHeading}>Enter email</Text>
                            <TextInput
                                style={styles.input}
                                value={email}
                                onChangeText={(text) => setEmail(text)}

                            />
                            <Text style={styles.errorText}>{errorName}</Text>


                            <TouchableOpacity onPress={onConfirm} style={styles.button}>
                                <Text style={styles.buttonText}>Send</Text>
                            </TouchableOpacity>

                            <View style={styles.bottomView}>
                                <Text style={styles.bottomText}>
                                    Remember password? </Text>
                                <TouchableOpacity onPress={onLogin}>
                                    <Text style={styles.bottomSecText}>Login</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAwareScrollView >

                    </View>
                </ScrollView>

        </SafeAreaView>
    )
}

