import { StyleSheet } from 'react-native';
import Fonts from '../../../../assets/fonts';
import Color from '../../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        alignItems: 'center',
    },
    backIcon: {
        height: 16,
        width: 15.85
    },
    logo: {
        height: 210,
         width: 210,
         alignSelf: 'center'
         
    },
    main: {
        width: '100%',
        paddingVertical: 15,
    },
    headingText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: 'bold',
        marginTop: 50,marginBottom: 8
    },
    nextLineHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginHorizontal: 20,
        // fontFamily: "Samsung Sharp Sans",
        marginBottom: 4
    },
    
    
    button: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center',
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,
        marginHorizontal: 20
    },
    inputStyles: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginHorizontal: 10,
        marginTop: 40,
        marginBottom: 20

    },
    input: {
        height: 44,
        width: 44,
        borderColor: Color.borderColor,
        borderWidth: 1,
        borderRadius: 10,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5
    },
    instruction: {
        alignSelf: 'center',
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black_60
    },
    optText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: "700",
        color: Color.skyBlue,
        height: 44,
        width: 44,
        textAlign: 'center',
        textAlignVertical: 'center',

    },

    activeInstruction: {
        color: Color.skyBlue
    },

    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText
    },
    bottomView: {
        marginTop: 16,
        marginHorizontal: 20,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bottomText: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        // fontFamily: "Samsung Sharp Sans"
    },
    bottomSecText: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        // fontFamily: "Samsung Sharp Sans"
    }
})