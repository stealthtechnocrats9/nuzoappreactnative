import React, { useState, useEffect, useRef } from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, ScrollView, KeyboardAvoidingView } from 'react-native'
// import styles from './styles'
import Images from '../../../../utils/Images'
import styles from './styles'
import Header from '../../../../utils/Header/Header'


export default function Opt_ForResetPassword({ navigation }) {

    const [pins, setPins] = useState('')
    const pinLength = 6
    let textInput = useRef(null)


    const resendOtp = () => {
        alert('resendOtp')
    }
    const onChangeText = (text) => {
        setPins(text)
        setTimeout(() => { textInput.current.focus() }, 100)

    }

    useEffect(() => {
        // textInput.focus()
        setTimeout(() => { textInput.current.focus() }, 100)
    }, [])



    const onConfirm = () => {
        navigation.navigate('set_newPassword')
    }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Header
                    leftIcon={Images.backArrow}
                    leftIconStyle={styles.backIcon}
                    onLeftPress={() => navigation.goBack()}
                    leftNextText="Reset Password" />
                <View style={styles.main}>
                    <View>
                        <Image source={Images.resetPassword_otp} style={styles.logo} />
                    </View>

                    <KeyboardAvoidingView
                        keyboardVerticalOffset={50}
                        behavior="padding">

                        <Text style={styles.headingText}>Email has been sent!</Text>
                        <Text style={styles.nextLineHeading} >Enter the authentication code in the email to reset your password</Text>


                        <TextInput
                            maxLength={pinLength}
                            style={{ width: 0, height: 0 }}
                            returnKeyType="done "
                            keyboardType="number-pad"
                            value={pins}
                            onChangeText={onChangeText}
                            ref={textInput} />

                        <View style={styles.inputStyles}>
                            {
                                Array(pinLength).fill().map((data, index) => (
                                    <View
                                        key={index}
                                        style={styles.input}>
                                        <Text style={styles.optText}
                                            onPress={() => textInput.current.focus()}
                                        >
                                            {pins && pins.length > 0 ? pins[index] : ""}
                                        </Text>
                                    </View>
                                ))
                            }

                        </View>


                        <Text style={styles.instruction}>It may take a minute to receive your code.</Text>
                        <Text style={styles.instruction}>Haven’t received it?
                            <Text style={styles.activeInstruction} onPress={resendOtp}>
                                Resend a new code</Text></Text>

                        <TouchableOpacity onPress={onConfirm} style={styles.button}>
                            <Text style={styles.buttonText}>Verify</Text>
                        </TouchableOpacity>

                    </KeyboardAvoidingView >


                </View>
            </ScrollView>


        </SafeAreaView>
    )
}

