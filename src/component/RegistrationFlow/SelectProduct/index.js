import React, { useState, useEffect } from 'react'
import { Text, View, FlatList, Image, TouchableOpacity, ScrollView, TextInput } from 'react-native'
import Header from '../../../utils/Header/Header'
import Images from '../../../utils/Images'
import styles from './styles'
import { useQuery } from '@apollo/client'
import { GET_ALL_PRODUCT, GET_SEARCH_PRODUCT } from '../../../graphql/queries/getProduct/index'
import Loader from '../../../utils/loader/index'
import AsyncStorage from '@react-native-async-storage/async-storage'


export default function Select_Product({ navigation }) {
    const [storeId, setStoreId] = useState('')
    const [showSearch, setShowSearch] = useState(false)
    const [searchItem, setSearchItem] = useState('')
    const { data, error, loading } = useQuery(GET_ALL_PRODUCT, {
        variables: {
            store_id: storeId
        }
    });
  
    const [allProduct, setAllProduct] = useState([])
    const [searchProduct, setSearchProduct] = useState([])
    const [mainAllContacts, setMainAllContacts] = useState([])

    const [productImage, setProductImage] = useState(null)



    // console.log('-------storeId', storeId)

    useEffect(async () => {
        if (data) {
            setAllProduct(data.products)
            setMainAllContacts(data.products)
        }
        await AsyncStorage.getItem("store_id").then((res) => setStoreId(res)).catch((err) => { console.log(err) })
    })

    console.log('=== allProduct-----------', allProduct)


    const setTextInputValue = (text) => {
        let filtercont = mainAllContacts.filter(product => {
            let lowerCase = (product.product_name).toLowerCase()
            console.log('-------', product, 'lowerCase', lowerCase)
            let searchToLowerCase = text.toLowerCase()
            return lowerCase.indexOf(searchToLowerCase) > -1
        }
        )
        console.log('=============filtercont', filtercont)

        setSearchProduct(filtercont)
        setSearchItem(text)

    }

    const showSearchBox = () => {
        setSearchProduct(mainAllContacts)

        setShowSearch(!showSearch)
    }

    const selectProduct = (item) => {
        console.log('=-=-=-', item)

        navigation.navigate('product_offer', { product: item })
    }


    console.log('product data is===searchProducts=========', searchItem)
    const numberOfItem = searchProduct.length




    const renderItem = ({ item }) => {
        let img = item.product_image[0]
        const selectImage = img != undefined ? img.product_image : null

        // console.log('-----------img', selectImage)
        return (
            <TouchableOpacity style={styles.flatlistView} onPress={() => selectProduct(item)}>
                <View style={styles.titleView}>
                    {selectImage ?
                        <Image source={{ uri: 'https://nuzo.co/product_images/' + selectImage }}
                            style={{ height: 35, width: 35, marginRight: 10, borderRadius: 4 }} />
                        :
                        <View style={{ height: 35, width: 35, marginRight: 10, borderRadius: 4 }} />
                    }

                    <Text style={styles.titleText}>{item.product_name}</Text>
                </View>


                <View style={styles.priceView}>
                    <Text style={styles.priceText}>{item.currency}  {item.product_price}</Text>
                </View>

            </TouchableOpacity>
        )
    };




    const search_renderItem = ({ item }) => {
        let img = item.product_image[0]
        const selectImage = img != undefined ? img.product_image : null

        return (
            <TouchableOpacity style={styles.flatlistView} onPress={() => selectProduct(item)}>
                <View style={styles.titleView}>
                    {selectImage ?
                        <Image source={{ uri: 'https://nuzo.co/product_images/' + selectImage }}
                            style={{ height: 35, width: 35, marginRight: 10, borderRadius: 4 }} />
                        :
                        <View style={{ height: 35, width: 35, marginRight: 10, borderRadius: 4 }} />
                    }

                    <Text style={styles.titleText}>{item.product_name}</Text>
                </View>
                <View style={styles.priceView}>
                    <Text style={styles.priceText}>{item.currency}  {item.product_price}</Text>
                </View>

            </TouchableOpacity>
        )
    };


    return (
        <View>
            {loading ?
                <Loader />
                :
                <ScrollView>
                    <Header
                        leftIcon={Images.backArrow}
                        leftNextText="Select Product"
                        onLeftPress={() => navigation.goBack()}
                        leftIconStyle={styles.menuLeftIcon}
                        rightNextSecIcon={Images.searchIcon}
                        onRightNextPress={showSearchBox}
                        rightNextIconStyle={styles.rightNextIconStyle} />

                    {
                        showSearch &&
                        <>
                            <TextInput
                                placeholder="search product"
                                value={searchItem}
                                onChangeText={(text) => setTextInputValue(text)}
                                style={styles.searchBox}
                            />

                            <View style={styles.main}>
                                <FlatList
                                    data={searchProduct}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={search_renderItem}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        </>
                    }

                    {
                        !showSearch &&

                        <View style={styles.main}>
                            <FlatList
                                data={allProduct}
                                showsVerticalScrollIndicator={false}
                                renderItem={renderItem}
                                style={{ marginBottom: 15 }}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    }

                </ScrollView>
            }
        </View>
    )
}

