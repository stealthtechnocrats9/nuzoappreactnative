import { StyleSheet } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        justifyContent: 'space-evenly'
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85,
    },
    rightNextIconStyle: {
        height: 15, width: 15
    },
    searchBox: {
        height: 52,
        borderWidth: 1,
        borderRadius: 16,
        marginHorizontal: 15,
        paddingLeft: 15,
        borderColor: Color.borderColor,
        color: Color.black_60,
        fontSize:15

    },
    main: {
        marginHorizontal: 20
    },
    flatlistView: {
        flexDirection: 'row',
        height: 50,
        marginVertical: 10,
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
    },
    titleView: {
        width: '55%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    priceView: {
        // backgroundColor: 'red',
        width: '20%',
        justifyContent: 'flex-end',

    },
    titleText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.black
    },
    priceText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.green
    }
})