import React from 'react'
import { SafeAreaView, Text, View, Image, TouchableOpacity } from 'react-native'
import styles from './styles'
import Images from '../../../utils/Images'

const Store_Verify = ({ navigation }) => {

    const onNextScreen = () => {
        //navigation.navigate('product_create', {storeId})
        navigation.reset({
            index: 0,
            routes: [{ name: 'product_create' }],
          });
    }


    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.imageStyle}>
                <Image source={Images.Closed_sign} style={styles.image} />
                <Text style={styles.heading}>Store created successfully!</Text>
                <Text style={styles.title}>Create a product your customers can shop</Text>
            </View>
            <TouchableOpacity style={styles.button}
                onPress={onNextScreen}>
                <Text style={styles.buttonText}>Create a product</Text>
            </TouchableOpacity>

        </SafeAreaView>
    )
}

export default Store_Verify