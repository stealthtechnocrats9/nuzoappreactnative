    import React from 'react'
    import { SafeAreaView, Text, View, Image, TouchableOpacity } from 'react-native'
    import styles from './styles'
    import Images from '../../../utils/Images'
    
    export default function Campaign_Created({ navigation , route}) {
    
        const storeId = route.params.storeId
        console.log('----12345', storeId)
        const onNextScreen = () => {
            navigation.navigate('userRegistration_bottomTabs',{storeId})
        }
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.imageStyle}>
                    <Image source={Images.email_campaign} style={styles.image} />
                    <Text style={styles.heading}>Campaign created successfully!</Text>
                </View>
               
                <TouchableOpacity style={styles.button}
                    onPress={onNextScreen}>
                    <Text style={styles.buttonText}>Go back to campaigns</Text>
                </TouchableOpacity>
               
            </SafeAreaView>
        )
    }
    
    

