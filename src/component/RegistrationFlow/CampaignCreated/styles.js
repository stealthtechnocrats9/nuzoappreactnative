import { StyleSheet } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        justifyContent: 'space-evenly'
    }, 
    imageStyle: {
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center'
    },
    image: {
        height: 210, 
        width: 210,
        marginBottom: 52
    },
    heading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: 'bold',
        color: Color.black,
        // fontFamily: "samsung Sharp Sans"
    },

    button: {
        height: 44,
        backgroundColor: Color.skyBlue,
        borderRadius: 10,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontWeight: "bold"
    },
    backButton: {
        alignSelf: 'center', 
        marginTop: 29,
        color: Color.skyBlue,
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        fontWeight: "700"
    }
})