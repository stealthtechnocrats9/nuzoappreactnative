import React, { useEffect, useState } from 'react'
import { SafeAreaView, Text, Image, TouchableOpacity, View, TextInput, ScrollView, FlatList, Pressable, Platform,Alert } from 'react-native'
import Images from '../../../utils/Images'
import styles from '../createProduct/styles'
import Header from '../../../utils/Header/Header'
import Modal from 'react-native-modal'
import Color from '../../../utils/Colors'
import { DELETE_PRODUCT, UPDATE_PRODUCT, REMOVE_PRODUCT_IMAGE, DUPLICATE_PRODUCT } from '../../../graphql/mutation/products/index'
import Loader from '../../../utils/loader/index'
import { useMutation } from '@apollo/client'
import { ReactNativeFile } from 'apollo-upload-client'
import MultipleImagePicker from '@baronha/react-native-multiple-image-picker'


function generateRNFile(uri, type, name) {
    console.log("i=uri is", uri, type, name)
    return uri
        ? new ReactNativeFile({
            uri,
            type,
            name,
        })
        : null;
}



export default function Product_Update({ navigation, route }) {
    const props = route.params.item
    const productimage = route.params.item.product_image
    const [name, setName] = useState(props.product_name)
    const [nameError, setNameError] = useState(null)
    const [priceError, setPriceError] = useState(null)
    const [address, setAddress] = useState(props.product_description)
    const [item, setItem] = useState('Ksh')
    const [price, setPrice] = useState(props.product_price)
    const [isShow, setIsShow] = useState(false)
    const [modalVisible, setModalVisible] = useState(false);
    const [modalDuplicateVisible, setModalDuplicateVisible] = useState(false)
    const [productId, setProductId] = useState('')
    const [images, setImages] = useState(productimage);
    const [localimages, setlocalImages] = useState();
    const [deleteItemProduct, { data, loading, error }] = useMutation(DELETE_PRODUCT)
    const [updateProduct, { data: updateProductData, loading: updateLoading, error: updateError }] = useMutation(UPDATE_PRODUCT)
    const [removeProductImage, { data: removeProductImageData, loading: removeProductImageLoading, error: removeProductImageError }] = useMutation(REMOVE_PRODUCT_IMAGE)
    const [duplicateProduct, { data: duplicateProductData, loading: duplicateProductLoading, error: duplicateProductError }] = useMutation(DUPLICATE_PRODUCT)

    console.log('props data is', productimage, images)

    useEffect(() => {
        setProductId(props._id)
    })
    // console.log('-----editData-----', editData, productId, productImage, '--------productImage', props.product_image)


    // console.log("=====", route.params.storeId)
    // const storeId = route.params.storeId


    const openPicker = async () => {
        try {
           // var localimages = []
        //    console.log('-------------i local images ', localimages)
        //     for (i = 0; i < images.length; i++) {
        //         if (images[i].product_image==undefined) {
        //            // console.log('-------------i val', images[i].product_image)
        //           // setlocalImages( [ ...localimages, images[i]]);
                   
        //             setlocalImages(images[i])
        //         }
        //     }
            const response = await MultipleImagePicker.openPicker({
                selectedAssets: localimages,
                isExportThumbnail: true,
                maxVideo: 1,
                usedCameraButton: false,
                isCrop: true,
                isCropCircle: true,
                // selectedColor: '#f9813a',
            });

            console.log("before", response)
            response.map(image => {
                image.path =
                    Platform.OS === "android" ? "file://" + image.realPath : image.realPath;
                return image
            });
            console.log(' Platform.OS', Platform.OS);
            console.log('done: ', response);

            setImages(productimage)
            for (i = 0; i < response.length; i++) {
                console.log('loop --- : ', i,images);

                    setImages(images=> [ ...images, response[i]]);
               
            }
            setlocalImages(response)


        } catch (e) {
            console.log(e);
        }
    };



    // useEffect(() => {
    // setProductImage([...productImage.push(filePath)])
    // props.product_image.map((i) => setProductImage(i.product_image))

    // }, [filePath])


    // console.log('----productImage----', productImage.fileName)
    const showMoreOption = () => {
        setIsShow(!isShow)
    }


    const onUpdateScreen = () => {
        if(localimages==undefined && images.length<1){
            alert("Please add atleast one image to update the product")
        }else if (!name) {
            setNameError("Please enter product name")
        }else if (!price) {
            setPriceError("Please enter product price")
        }else{

        let fileData=[]
        
        if(localimages!=undefined){
         fileData = localimages.map(i => {
            let rimage = generateRNFile(i.path, i.mime, i.filename)
            console.log("rimage", rimage)
            return rimage
        });
        }
        console.log("onNextScreen fileData", fileData)
        let vardata = {
            product_name: name,
            product_description: address,
            product_price: price,
            product_id: productId,
            product_image: fileData
        }
        // navigation.navigate("product_verify")
        updateProduct({
            variables: vardata
        }).then((res) => {
            if (res.data.updateProduct) {
                Alert.alert(
                    'Success',
                    'Product updated successfully', // <- this part is optional, you can pass an empty string
                    [
                      {text: 'OK', onPress: () => {console.log('OK Pressed')
                      navigation.navigate('userRegistration_bottomTabs', { screen: 'ProductsScreen' })
                      }},
                    ],
                    {cancelable: false},
                  );
            }
        }).catch((err)=> {console.log(err)})
    }
    }


    const removeProductImageItem = async (value ,id) => {
      //  console.log('9-----------',id)
        await removeProductImage({
            variables: {
                image_id: id,
                product_id: productId
            }
        }).then((res) => {
            if (res.data.removeProductImage) {
                onDeleteserverimage(value)
               // navigation.navigate('userRegistration_bottomTabs', { screen: 'ProductsScreen' })
            }
        }).catch((err)=> {console.log(err)})
    
    }


    const onDuplicateProduct = () => {
        setModalDuplicateVisible(!modalDuplicateVisible)
    }

    const onDuplicateConfirm = () => {
        duplicateProduct({
            variables: {
                product_id: productId
            },
        }).then((res) => {
            if (res.data.duplicateProduct) {
                navigation.navigate('userRegistration_bottomTabs', { screen: 'ProductsScreen' })
            }
        }).catch((err)=> {console.log(err)})
    }


    const onDeleteProduct = () => {
        setModalVisible(!modalVisible)

    }

    const onDeleteConfirm = () => {
        deleteItemProduct({
            variables: {
                product_id: productId
            },
        }).then((res) => {
          //  console.log('--------res', res)
            if (res.data.deleteProduct) {
                navigation.navigate('userRegistration_bottomTabs', { screen: 'ProductsScreen' })
            }
        }).catch((err)=> {console.log(err)})
    }


    const renderItem = ({ item }) => {
       //  console.log('-------------item', item)
        return (
            <View>
                {item.product_image != undefined ?
                    <Image
                        source={{ uri: 'https://nuzo.co/product_images/' + item.product_image }}
                        // source={{
                        //             uri: item?.type === 'video' ? item?.thumbnail ?? '' : item?.path,
                        //         }}
                        style={styles.productIcon} />
                    :
                    <Image
                        source={{
                            uri: item?.type === 'video' ? item?.thumbnail ?? '' : item?.path,
                        }}
                        style={styles.productIcon} />
                }

                <TouchableOpacity onPress={() => 
                {

                    if(item._id==undefined){
                        console.log('-------run if')

                        onDelete(item)}else{
                            console.log('-------run else')

                            removeProductImageItem(item,item._id)}
                        }}
                    style={styles.deleteProductView}>
                    <Image source={Images.deletePeoduct}
                        style={styles.deleteProduct} />
                </TouchableOpacity>
            </View>
        )
    };

    
    const onDeleteserverimage = (value) => {
        const data = images.filter(
            (item) =>
                //item?.localIdentifier &&
                item?.product_image !== value?.product_image
        );
        setImages(data);
    };
    const onDelete = (value) => {
        const data = images.filter(
            (item) =>
                //item?.localIdentifier &&
                item?.localIdentifier !== value?.localIdentifier
        );
        setImages(data);
    };

    return (
        <ScrollView>
            <SafeAreaView style={styles.container}>
                <Header leftNextText="Edit Product"
                    leftIcon={Images.backArrow}
                    leftIconStyle={styles.leftIconStyle}
                    onLeftPress={() => navigation.navigate('userRegistration_bottomTabs', { screen: 'ProductsScreen' })}
                    rightNextSecIcon={Images.more}
                    onRightNextPress={showMoreOption}
                    rightNextIconStyle={styles.rightNextIconStyle} />
                {loading || removeProductImageLoading ?
                    <Loader />
                    :
                    <>

                        {
                            isShow &&
                            <View style={styles.deleteBox}>
                                <TouchableOpacity onPress={onDuplicateProduct} style={styles.deleteButton}>
                                    <Text style={styles.deleteButtonText}>Duplicate</Text>
                                </TouchableOpacity>

                                <Modal
                                    testID={'modal'}
                                    isVisible={modalDuplicateVisible}
                                    swipeDirection={['left']}
                                    transparent={true}
                                    propagateSwipe={true}
                                    onBackdropPress={() => setModalDuplicateVisible(false)}
                                >
                                    <View style={styles.modelView}>
                                        <Text style={styles.modalHeading}>Duplicate product?</Text>
                                        <Text style={styles.modalText}> Are you sure you want to create a new Duplicate product?
                                            This action cannot be undone</Text>

                                        <View style={styles.modelButton}>
                                            <Pressable
                                                onPress={() => setModalDuplicateVisible(false)}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Cancel</Text>
                                                )}
                                            </Pressable>
                                            <Pressable
                                                onPress={onDuplicateConfirm}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Duplicate product</Text>
                                                )}
                                            </Pressable>

                                        </View>
                                    </View>
                                </Modal>
                                <View style={styles.borderView} />
                                <TouchableOpacity onPress={onDeleteProduct} style={styles.deleteButton}>
                                    <Text style={styles.deleteButtonText}>Delete</Text>
                                </TouchableOpacity>


                                <Modal
                                    testID={'modal'}
                                    isVisible={modalVisible}
                                    swipeDirection={['left']}
                                    transparent={true}
                                    propagateSwipe={true}
                                    onBackdropPress={() => setModalVisible(false)}
                                >
                                    <View style={styles.modelView}>
                                        <Text style={styles.modalHeading}>Delete product?</Text>
                                        <Text style={styles.modalText}> Are you sure you want to delete this product?
                                            This action cannot be undone</Text>

                                        <View style={styles.modelButton}>
                                            <Pressable
                                                onPress={() => setModalVisible(false)}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Cancel</Text>
                                                )}
                                            </Pressable>
                                            <Pressable
                                                onPress={onDeleteConfirm}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Delete product</Text>
                                                )}
                                            </Pressable>

                                        </View>
                                    </View>
                                </Modal>

                            </View>
                        }

                        <View style={styles.mainView}>
                            <Text style={styles.textHeading}>Product image</Text>

                            <View style={styles.imageView}>
                                <TouchableOpacity onPress={openPicker} style={styles.cameraView}>
                                    <Image source={Images.cameraIcon} style={styles.cameraIcon} />
                                </TouchableOpacity>


                                <FlatList
                                    data={images}
                                    horizontal={true}
                                    renderItem={renderItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsHorizontalScrollIndicator={false}
                                />

                            </View>
                        </View>

                        <View style={styles.mainView}>
                            <Text style={styles.textHeading}>Product name</Text>
                            <TextInput
                                style={styles.input}
                                value={name}
                                onChangeText={(text) => {setName(text);setNameError(null)}}
                            />
                        </View>
                        <Text style={styles.errorText}>{nameError}</Text>

                        <View style={styles.mainView}>
                            <Text style={styles.textHeading}>Product description</Text>
                            <TextInput
                                style={styles.descriptioninput}
                                value={address}
                                onChangeText={(text) => setAddress(text)}
                                multiline={true}
                            />
                        </View>


                        <View style={styles.mainView}>
                            <View style={styles.priceTextView}>
                                <Text style={styles.textHeading}>Price</Text>
                                <Image source={Images.questionIcon} style={styles.questionIcon} />
                            </View>
                            <View style={styles.priceBoxStyle}>

                                <TextInput
                                    style={styles.inputItem}
                                    value={item}
                                    editable={false}
                                   // onChangeText={() => setItem(text)}
                                />

                                <TextInput
                                    style={styles.inputPrice}
                                    value={price}
                                    onChangeText={(text) => {setPrice(text);setPriceError(null)}}
                                    keyboardType='phone-pad'
                                />
                            </View>
                            <Text style={styles.errorText}>{priceError}</Text>

                        </View>

                        <TouchableOpacity style={styles.button}
                            onPress={onUpdateScreen}>
                            <Text style={styles.buttonText}>Update</Text>
                        </TouchableOpacity>
                    </>
                }
            </SafeAreaView>

        </ScrollView>
    )
}




