import React from 'react'
import { SafeAreaView, Text, View, Image, TouchableOpacity } from 'react-native'
import Images from '../../../utils/Images'
import styles from './styles'


export default function Verify_account({ navigation }) {



    const onNextScreen = () => {
       // navigation.navigate("store_create", {userId})
        navigation.reset({
            index: 0,
            routes: [{ name: 'store_create' }],
          });
        
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.imageStyle}>
                <Image source={Images.accountVerify} style={styles.image}/>
                <Text style={styles.heading}>Account created successfully!</Text>
                <Text style={styles.title}>Let’s set up your store</Text>
            </View>
            <TouchableOpacity style={styles.button}
            onPress={onNextScreen}>
                <Text style={styles.buttonText}>Set up store</Text>
            </TouchableOpacity>

        </SafeAreaView>
    )
}

