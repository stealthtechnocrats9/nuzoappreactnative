import { StyleSheet } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        justifyContent: 'space-evenly'
    }, 
    imageStyle: {
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center'
    },
    image: {
        height: 210, 
        width: 210
    },
    heading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: 'bold',
        color: Color.black,
        // fontFamily: "samsung Sharp Sans"
    },
    title: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: 'bold',
        color: Color.black_80,
        // fontFamily: "samsung Sharp Sans"
    },
    button: {
        height: 44,
        backgroundColor: Color.skyBlue,
        borderRadius: 10,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: "bold"
    }
})