import { StyleSheet } from 'react-native';
import Fonts from '../../../../../assets/fonts';
import Color from '../../../../../utils/Colors';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
    },
    leftIconStyle: {
        height: 16,
        width: 16
    },
    rightNextIconStyle: {
        height: 20, width: 20
    },
    main: {
        marginHorizontal: 20
    },
    heading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 24
    },
    deleteBox: {
        paddingVertical: 7,
        width: 100,
        backgroundColor: Color.white,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: Color.borderColor,
        position: 'absolute',
        right: 20,
        top: 40,
        alignItems: 'center',
        zIndex: 1,
        backgroundColor: Color.white,
        flex: 1,
    },
    deleteButton: {
        width: '100%',
        paddingVertical: 10,
        alignItems: 'center',
    },
    borderView: {
        height: 1,
        width: 85,
        alignSelf: 'center',
        backgroundColor: Color.borderColor,
        marginVertical: 6
    },

    deleteButtonText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue
    },
    shareBoxView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 17
    },
    shareIcon: {
        width: 16,
        height: 16
    },
    editCampaignText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue
    },
    smsBox: {
        height: 72,
        width: 100,
        backgroundColor: Color.skyBlue_5,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    clickReateBox: {
        height: 72,
        width: 100,
        backgroundColor: Color.greenTransparent,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxResultText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        fontWeight:"600"
    },
    boxTitleText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60
    },
    weekBoxMainView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 17,
        marginBottom: 25
    },
    weekBoxView: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Color.skyBlue_5,
        width: 80,
        height: 24,
        borderRadius: 10,
        justifyContent: 'space-around'

    },
    weeklyText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginRight: 5
    },
    dropdown: {
        height: 6.17,
        width: 10
    },
    hideDropDownBox: {
        zIndex: 1,
        backgroundColor: Color.white,
        borderWidth: 1,
        width: 85,
        flex: 1,
        borderRadius: 10,
        borderColor: Color.borderColor,
        position: 'absolute',
        top: 40,
        right: 0,
        alignSelf: 'flex-end'
    },
    setdropDownBox: {
        justifyContent: 'flex-start',
        marginHorizontal: 6,
    },
    setdropDownBorderBox: {
        justifyContent: 'flex-start',
        marginHorizontal: 6,
        borderBottomWidth: 1,
        borderBottomColor: Color.borderColor
    },
    setSelectdropDownBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 6,
    },
    setSelectdropDownBorderBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 6,
        borderBottomWidth: 1,
        borderBottomColor: Color.borderColor

    },
    hideDropDownBoxTitle: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        paddingVertical: 16.5
    },
    dottedLineIcon: {
        width: '100%',
        marginBottom: 40
    },
    firstLine: {
        marginBottom: 40
    },
    inLineText: {
        position: 'absolute',
        alignSelf: 'center',
        top: 45
    },
    LineIcon: {
        width: '100%',
        marginBottom: 10
    },
    weeksDaysStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    weeksText: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black_60
    },
    instructions: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 40
    },
    modelView: {
        width: '100%',
        backgroundColor: Color.white,
        borderRadius: 10,
        paddingVertical: 25
    },
    modalHeading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        alignSelf: 'center',
        fontWeight: '600'
    },
    modalText: {
        alignSelf: 'center',
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 16,
        marginHorizontal: 50,
        textAlign: 'center',
        marginBottom: 41
    },
    modelButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 16

    },
    modelEditButton: {
        height: 52,
        borderColor: Color.red,
        width: 152,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelSendButton: {
        height: 52,
        width: 152,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight: '600'

    },
    modelActiveButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.white,
        fontWeight: '600'

    },
    productName: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: "700",
        color: Color.blue_7,
        marginTop: 8,
        marginBottom: 8
    },

    orderName: {
        fontSize: 13,

        fontFamily: Fonts.Samsung, 
        color: Color.black,
    },
    orderprice: {
        fontSize: 13,

        fontFamily: Fonts.Samsung, 
        color: Color.blue_8,
        fontWeight: "700"
    },
    detailText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginRight: 5
    },
    currency: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.green
    },
    orderDeliverdeStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung, 
        color: Color.black_60,
        paddingHorizontal: 8,
        backgroundColor: Color.black_5,
        paddingVertical: 4,
        borderRadius: 16
    },
    orderActiveStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.green,
        paddingHorizontal: 8,
        backgroundColor: Color.lightGreen,
        paddingVertical: 4,
        borderRadius: 16
    },
    orderReadyStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.green,
        paddingHorizontal: 8,
        backgroundColor: Color.lightGreen,
        paddingVertical: 4,
        borderRadius: 16
    },
    orderDeliveredStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.green,
        paddingHorizontal: 8,
        backgroundColor: Color.black_60,
        paddingVertical: 4,
        borderRadius: 16
   
    },
    orderCancelStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
         color: Color.red,
        paddingVertical: 4,
        paddingHorizontal: 8,
        backgroundColor: Color.lightRed,
        borderRadius: 16
    },

    orderpendingStatusText: {
        fontSize: 13,

        fontFamily: Fonts.Samsung, color: Color.yellow_30,
        paddingVertical: 4,
        paddingHorizontal: 8,
        backgroundColor: Color.yellow_50,
        borderRadius: 16
    },
})