import React, { useState, useEffect } from 'react'
import { Image, Text, View, Share, ScrollView, Dimensions, FlatList, TouchableOpacity, Pressable , ActivityIndicator} from 'react-native'
import Header from '../../../../../utils/Header/Header'
import Images from '../../../../../utils/Images'
import styles from './styles'
import Color from '../../../../../utils/Colors'
import { LineChart } from "react-native-chart-kit";
import { useQuery, useMutation } from '@apollo/client'
import {
    GET_CAMPAIGN_DETAIL,
    GET_GRAPH_DAILY_DETAIL,
    GET_GRAPH_WEEKLY_DETAIL,
    GET_GRAPH_MONTHLY_DETAIL,
    GET_GRAPH_YEARLY_DETAIL
} from '../../../../../graphql/queries/campaign/index'
import Loader from '../../../../../utils/loader/index'
import Modal from 'react-native-modal'
import { DELETE_CAMPAIGN, PAUSE_OFFER, CONTINUE_OFFER } from '../../../../../graphql/mutation/createOffer/index'

export default function Campaign_Detail({ navigation, route }) {

    const offer_id = route.params.item.offer_id

    const editData = route.params.item
    const storeName=route.params.storeName
    const status = route.params.item.status

    const [isShow, setIsShow] = useState(false)
    const [graphShow, seFraphShow] = useState(true)
    const [isShowMore, setIsShowMore] = useState(false)
    const [modalVisible, setModalVisible] = useState(false);
    const [modalPauseVisible, setModalPauseVisible] = useState(false)
    const [graphTime, setGraphTime] = useState([
        "Daily",
        "Weekly",
        "Monthly",
        "Yearly"
    ])

    const [reruirements, setRequirement] = useState(0)
    const [graphText, setGraphText] = useState('Daily')
    const requireGraph = graphText.toLowerCase()

    const [dailyGraphData, setDailyGraphData] = useState([])
    const [weeklyGraphData, setWeeklyGraphData] = useState([])
    const [monthlyGraphData, setMonthlyGraphData] = useState([])
    const [yearlyyGraphData, setYearlyyGraphData] = useState([])


    const [allCampaign, setAllCampaign] = useState([])
    const { data, loading, error } = useQuery(GET_CAMPAIGN_DETAIL, {
        variables: {
            offer_id: offer_id
        }
    });

    const { data: dailyData, error: dailyError } = useQuery(GET_GRAPH_DAILY_DETAIL, {
        variables: {
            offer_id: offer_id,
            type: requireGraph
        }
    });

    const { data: weeklyData, error: weeklyError } = useQuery(GET_GRAPH_WEEKLY_DETAIL, {
        variables: {
            offer_id: offer_id,
            type: requireGraph
        }
    });

    const { data: monthlyData, error: monthlyError } = useQuery(GET_GRAPH_MONTHLY_DETAIL, {
        variables: {
            offer_id: offer_id,
            type: requireGraph
        }
    });

    const { data: yearlyData, error: yearlyError } = useQuery(GET_GRAPH_YEARLY_DETAIL, {
        variables: {
            offer_id: offer_id,
            type: requireGraph
        }
    });

    const [deleteCampaign, { data: deleteCampData, loading: DeleteLoading }] = useMutation(DELETE_CAMPAIGN)

    const [pauseCampaign, { data: pauseCampData, loading: pauseLoading }] = useMutation(PAUSE_OFFER)

    const [continueCampaign, { data: continueCampData, loading: continueLoading }] = useMutation(CONTINUE_OFFER)


    useEffect(() => {
        if (data) {
            setAllCampaign(data.getOfferDetailsByID[0])
        }
        if (dailyData) {
            setDailyGraphData(dailyData.clicksGraph)
        }
        if (weeklyData) {
            setWeeklyGraphData(weeklyData.clicksGraph)
        }
        if (monthlyData) {
            setMonthlyGraphData(monthlyData.clicksGraph)
        }
        if (yearlyData) {
            setYearlyyGraphData(yearlyData.clicksGraph)
        }
    })


    const showMoreOption = () => {
        setIsShowMore(!isShowMore)
    }


    const onPauseCampaign = () => {
        setModalPauseVisible(!modalPauseVisible)
    }
    const onConfirmPauseCampaign = () => {
        pauseCampaign({
            variables: {
                offer_id: offer_id
            }
        }).then((res) => {
            if (res.data) {
                navigation.navigate("CampaignScreen")
                setModalPauseVisible(false)
            }
        }).catch((err) => { console.log(err) })
    }
    const onConfirmContinueCampaign = () => {
        continueCampaign({
            variables: {
                offer_id: offer_id
            }
        }).then((res) => {
            if (res.data) {
                navigation.navigate("CampaignScreen")
                setModalPauseVisible(false)
            }
        }).catch((err) => { console.log(err) })
    }
    console.log('--------continueCampData--', continueCampData)

    const onDeleteProduct = () => {
        setModalVisible(!modalVisible)
    }

    const onDeleteConfirm = () => {
        deleteCampaign({
            variables: {
                offer_id: offer_id
            },
        }).then((res) => {
            if (res.data) [
                navigation.navigate("CampaignScreen")
            ]
            setModalVisible(false)
            setIsShowMore(false)
        }).catch((err) => { console.log(err) })

    }

    // console.log('----------delete', deleteCampData)


    const showRequirememt = (item, index) => {
        setRequirement(index)
        setGraphText(item)
        setIsShow(false)
    }

   // console.log('hdbhjdjbshcvbjdsjbkc',allCampaign.product_id)

    const onSelectGraphValue = () => {
        setIsShow(!isShow)
    }

    const editCampaign = () => {
        navigation.navigate("product_offer", { editData })
    }

    const shareItem = async () => {
        try {
            const result = await Share.share({
                message:
                    'http://app.nuzo.co/'+storeName+"-id"+ allCampaign.product_id,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    }

    return (
        <View style={styles.container}>
            {loading ?
                <Loader />
                :
                <View style={styles.container}>
                    <Header
                        leftIcon={Images.backArrow}
                        leftNextText="Campaign Details"
                        onLeftPress={() => navigation.goBack()}
                        leftIconStyle={styles.leftIconStyle}
                        rightNextSecIcon={Images.more}
                        onRightNextPress={showMoreOption}
                        rightNextIconStyle={styles.rightNextIconStyle}
                    />



                    {
                        isShowMore &&
                        <View style={styles.deleteBox}>

                            {/* <TouchableOpacity onPress={onPauseCampaign} style={styles.deleteButton}>
                                <Text style={styles.deleteButtonText}>{status == 1 ? "Pause" : "Continue"}</Text>
                            </TouchableOpacity>
                            <View style={styles.borderView} /> */}

                            <TouchableOpacity onPress={onDeleteProduct} style={styles.deleteButton}>
                                <Text style={styles.deleteButtonText}>Delete</Text>
                            </TouchableOpacity>

                            <Modal
                                testID={'modal'}
                                isVisible={modalVisible}
                                swipeDirection={['left']}
                                transparent={true}
                                propagateSwipe={true}
                                onBackdropPress={() => setModalVisible(false)}
                            >
                                <View style={styles.modelView}>
                                    <Text style={styles.modalHeading}>Delete Campaign?</Text>
                                    <Text style={styles.modalText}> Are you sure you want to delete this campaign?
                                        You'll lose all your campaign data by taking this action.</Text>

                                    <View style={styles.modelButton}>
                                        <Pressable
                                            onPress={() => setModalVisible(false)}
                                            style={({ pressed }) => [
                                                {
                                                    backgroundColor: pressed
                                                        ? Color.red
                                                        : Color.white
                                                },
                                                styles.modelEditButton
                                            ]}>

                                            {({ pressed }) => (
                                                <Text style={
                                                    pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Cancel</Text>
                                            )}
                                        </Pressable>
                                        <Pressable
                                            onPress={onDeleteConfirm}
                                            style={({ pressed }) => [
                                                {
                                                    backgroundColor: pressed
                                                        ? Color.red
                                                        : Color.white
                                                },
                                                styles.modelEditButton
                                            ]}>

                                            {({ pressed }) => (
                                                <Text style={
                                                    pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Delete Campaign</Text>
                                            )}
                                        </Pressable>

                                    </View>
                                </View>
                            </Modal>

                            {/* 
                            <Modal
                                testID={'modal'}
                                isVisible={modalPauseVisible}
                                swipeDirection={['left']}
                                transparent={true}
                                propagateSwipe={true}
                                onBackdropPress={() => setModalPauseVisible(false)}
                            >
                                <View style={styles.modelView}>
                                    {status == 1 ?
                                        <>
                                            <Text style={styles.modalHeading}>Pause campaign?</Text>
                                            <Text style={styles.modalText}> Are you sure you want to Pause this campaign?
                                                This action cannot be undone</Text>
                                        </>
                                        :
                                        <>
                                            <Text style={styles.modalHeading}>Continue campaign?</Text>
                                            <Text style={styles.modalText}> Are you sure you want to Continue this campaign?
                                                This action cannot be undone</Text>
                                        </>
                                    }

                                    <View style={styles.modelButton}>
                                        <Pressable
                                            onPress={() => setModalPauseVisible(false)}
                                            style={({ pressed }) => [
                                                {
                                                    backgroundColor: pressed
                                                        ? Color.red
                                                        : Color.white
                                                },
                                                styles.modelEditButton
                                            ]}>

                                            {({ pressed }) => (
                                                <Text style={
                                                    pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Cancel</Text>
                                            )}
                                        </Pressable>

                                        {status == 1 ?
                                            <Pressable
                                                onPress={onConfirmPauseCampaign}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Pause</Text>
                                                )}
                                            </Pressable>
                                            :
                                            <Pressable
                                                onPress={onConfirmContinueCampaign}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Continue</Text>
                                                )}
                                            </Pressable>
                                        }

                                    </View>
                                </View>
                            </Modal>
*/}
                        </View>
                    }

                    <ScrollView style={styles.main}
                        showsVerticalScrollIndicator={false}>
                        <Text style={styles.heading}>{allCampaign.product_name}</Text>

                        <TouchableOpacity
                            onPress={shareItem}
                            style={styles.shareBoxView}>
                            <Image source={Images.share} style={styles.shareIcon} />
                            {/* <TouchableOpacity style={{ padding: 2 }} onPress={editCampaign}>
                                <Text style={styles.editCampaignText}>Edit Campaign</Text>
                            </TouchableOpacity> */}
                        </TouchableOpacity>


                        <View style={styles.shareBoxView}>

                            <View style={styles.smsBox}>
                                <Text style={styles.boxResultText}>{allCampaign.sent}</Text>
                                <Text style={styles.boxTitleText}>{"SMS Sent"}</Text>

                            </View>
                            <View style={styles.smsBox}>
                                <Text style={styles.boxResultText}>{allCampaign.total_clicks}</Text>
                                <Text style={styles.boxTitleText}>{"Clicks"}</Text>
                            </View>
                            <View style={styles.clickReateBox}>
                                <Text style={styles.boxResultText}>{allCampaign.ctr + "%"}</Text>
                                <Text style={styles.boxTitleText}>{"Click rate"}</Text>
                            </View>

                        </View>

                        <View>
                            <View style={styles.weekBoxMainView}>
                                <Text style={styles.weeklyText}>Number of clicks this week</Text>
                                <TouchableOpacity
                                    onPress={onSelectGraphValue} style={styles.weekBoxView}>
                                    <Text style={styles.weeklyText}>{graphText}</Text>
                                    <>
                                        <Image source={Images.dropdown} style={styles.dropdown} />
                                    </>
                                </TouchableOpacity>
                            </View>

                            {
                                isShow == true &&
                                <FlatList
                                    style={styles.hideDropDownBox}
                                    data={graphTime}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({ item, index }) => (
                                        <View >
                                            {reruirements != index ?
                                                <TouchableOpacity style={graphTime.length == index + 1 ? styles.setdropDownBox : styles.setdropDownBorderBox}
                                                    onPress={() => showRequirememt(item, index)}>
                                                    <Text style={styles.hideDropDownBoxTitle}>{item}</Text>
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity style={graphTime.length == index + 1 ? styles.setSelectdropDownBox : styles.setSelectdropDownBorderBox}
                                                    onPress={() => showRequirememt(item, index)}>
                                                    <Text style={styles.hideDropDownBoxTitle}>{item}</Text>
                                                    <Image source={Images.tick} />
                                                </TouchableOpacity>}
                                        </View>
                                    )}
                                />
                            }




                            {graphText == "Daily" &&
                                <LineChart
                                    data={{
                                        labels: dailyGraphData.map(i => i.time),
                                        datasets: [
                                            {
                                                data: dailyGraphData.map(i => i.total_clicks),
                                                color: () => `rgba(0, 128, 246, 1)`,
                                                withDots: false,
                                                withShadow: false,
                                                withVerticalLines: false,
                                            }
                                        ],
                                    }}
                                    width={Dimensions.get("window").width - 5} // from react-native
                                    height={220}
                                    yAxisInterval={1} // optional, defaults to 1
                                    segments={3}
                                    withHorizontalLabels={false}
                                    withVerticalLines={false}
                                    withShadow={false}
                                    withOuterLines={false}
                                    withInnerLines={true}
                                    chartConfig={{
                                        backgroundColor: "transparent",
                                        backgroundGradientFrom: '#fff',
                                        backgroundGradientTo: '#fff',
                                        backgroundGradientFromOpacity: 0,
                                        backgroundGradientToOpacity: 0,

                                        propsForBackgroundLines: {
                                            strokeDasharray: 3,
                                            strokeDashoffset: 1,
                                            strokeWidth: 1
                                        },
                                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        style: {
                                            borderRadius: 10,
                                        },
                                        propsForDots: {
                                            r: "6",
                                            strokeWidth: "12",
                                            paddingRight: 0
                                        }
                                    }}
                                    style={{
                                        marginVertical: 18,
                                        borderRadius: 0,
                                        paddingRight: 0
                                    }}
                                />
                            }



                            {graphText == "Weekly" &&
                                <LineChart
                                    data={{
                                        labels: weeklyGraphData.map(i => i.day),
                                        datasets: [
                                            {
                                                data: weeklyGraphData.map(i => i.total_clicks),
                                                color: () => `rgba(0, 128, 246, 1)`,
                                                withDots: false,
                                                withShadow: false,
                                                withVerticalLines: false,
                                            }
                                        ],
                                    }}
                                    width={Dimensions.get("window").width - 5} // from react-native
                                    height={220}
                                    yAxisInterval={1} // optional, defaults to 1
                                    segments={3}
                                    withHorizontalLabels={false}
                                    withVerticalLines={false}
                                    withShadow={false}
                                    withOuterLines={false}
                                    withInnerLines={true}
                                    chartConfig={{
                                        backgroundColor: "transparent",
                                        backgroundGradientFrom: '#fff',
                                        backgroundGradientTo: '#fff',
                                        backgroundGradientFromOpacity: 0,
                                        backgroundGradientToOpacity: 0,

                                        propsForBackgroundLines: {
                                            strokeDasharray: 3,
                                            strokeDashoffset: 1,
                                            strokeWidth: 1
                                        },
                                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        style: {
                                            borderRadius: 10,
                                        },
                                        propsForDots: {
                                            r: "6",
                                            strokeWidth: "12",
                                            paddingRight: 0
                                        }
                                    }}
                                    style={{
                                        marginVertical: 18,
                                        borderRadius: 0,
                                        paddingRight: 0
                                    }}
                                />
                            }







                            {graphText == "Monthly" &&
                                <LineChart
                                    data={{
                                        labels: monthlyGraphData.map(i => i.month),
                                        datasets: [
                                            {
                                                data: monthlyGraphData.map(i => i.total_clicks),
                                                color: () => `rgba(0, 128, 246, 1)`,
                                                withDots: false,
                                                withShadow: false,
                                                withVerticalLines: false,
                                            }
                                        ],
                                    }}
                                    width={Dimensions.get("window").width - 5} // from react-native
                                    height={220}
                                    yAxisInterval={1} // optional, defaults to 1
                                    segments={3}
                                    withHorizontalLabels={false}
                                    withVerticalLines={false}
                                    withShadow={false}
                                    withOuterLines={false}
                                    withInnerLines={true}
                                    chartConfig={{
                                        backgroundColor: "transparent",
                                        backgroundGradientFrom: '#fff',
                                        backgroundGradientTo: '#fff',
                                        backgroundGradientFromOpacity: 0,
                                        backgroundGradientToOpacity: 0,

                                        propsForBackgroundLines: {
                                            strokeDasharray: 3,
                                            strokeDashoffset: 1,
                                            strokeWidth: 1
                                        },
                                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        style: {
                                            borderRadius: 10,
                                        },
                                        propsForDots: {
                                            r: "6",
                                            strokeWidth: "12",
                                            paddingRight: 0
                                        }
                                    }}
                                    style={{
                                        marginVertical: 18,
                                        borderRadius: 0,
                                        paddingRight: 0
                                    }}
                                />
                            }



                            {graphText == "Yearly" &&
                                <LineChart
                                    data={{
                                        labels: yearlyyGraphData.map(i => i.month),
                                        datasets: [
                                            {
                                                data: weeklyGraphData.map(i => i.total_clicks),
                                                color: () => `rgba(0, 128, 246, 1)`,
                                                withDots: false,
                                                withShadow: false,
                                                withVerticalLines: false,
                                            }
                                        ],
                                    }}
                                    width={Dimensions.get("window").width - 5} // from react-native
                                    height={220}
                                    yAxisInterval={1} // optional, defaults to 1
                                    segments={3}
                                    withHorizontalLabels={false}
                                    withVerticalLines={false}
                                    withShadow={false}
                                    withOuterLines={false}
                                    withInnerLines={true}
                                    chartConfig={{
                                        backgroundColor: "transparent",
                                        backgroundGradientFrom: '#fff',
                                        backgroundGradientTo: '#fff',
                                        backgroundGradientFromOpacity: 0,
                                        backgroundGradientToOpacity: 0,

                                        propsForBackgroundLines: {
                                            strokeDasharray: 3,
                                            strokeDashoffset: 1,
                                            strokeWidth: 1
                                        },
                                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        style: {
                                            borderRadius: 10,
                                        },
                                        propsForDots: {
                                            r: "6",
                                            strokeWidth: "12",
                                            paddingRight: 0
                                        }
                                    }}
                                    style={{
                                        marginVertical: 18,
                                        borderRadius: 0,
                                        paddingRight: 0
                                    }}
                                />
                            }

                        </View>


                        <Text style={styles.instructions}>Orders from campaign</Text>

                    </ScrollView>
                </View>
            }
        </View>
    )
}






