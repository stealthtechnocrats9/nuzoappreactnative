import React, { useState, useEffect, useCallback } from 'react'
import { FlatList, Image, Text, TouchableOpacity, View, RefreshControl, ScrollView, TextInput, Alert } from 'react-native'
import Header from '../../../../../utils/Header/Header'
import Images from '../../../../../utils/Images'
import styles from './styles'
import { useQuery } from '@apollo/client'
import { GET_CAMPAIGN_OFFERS } from '../../../../../graphql/queries/campaign/index'
import Loader from '../../../../../utils/loader'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { updateContact } from 'react-native-contacts'



const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

export default function CampaignScreen({ navigation, route }) {

    const [storeId, setStoreId] = useState('')
    const [storeName, setStoreName] = useState('')
    const [refreshing, setRefreshing] = useState(false);
    const [showSearch, setShowSearch] = useState(false)
    const [searchItem, setSearchItem] = useState('')
    const [allProduct, setAllProduct] = useState([])
    const [searchallProduct, setSearchAllProduct] = useState([])

    const { data, loading, error, refetch } = useQuery(GET_CAMPAIGN_OFFERS, {
        variables: {
            store_id: storeId,
        }
    });

    useEffect(async () => {
        await AsyncStorage.getItem("store_id").then((res) => setStoreId(res)).catch((err) => { console.log(err) })
    })

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            refetch()
            // The screen is focused
            // Call any action
        });
        return unsubscribe;
    }, [navigation]);

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        refetch()
        wait(200).then(() => setRefreshing(false));
    }, []);


    useEffect(() => {
        if (data) {
            setAllProduct(data.getOffers);
            if(data.getOffers.length>0){
                setStoreName(data.getOffers[0].store_name);
            }
        }
    })

    //console.log("=====--allProduct--====", '-÷sss----', allProduct)

    const checkDetail = (item) => {
        navigation.navigate("campaign_detail", { item ,storeName})
    }

    const offerCreate = () => {
        navigation.navigate("product_offer", { storeId })
    }

    const showSearchBox = () => {
        setSearchAllProduct(allProduct)

        setShowSearch(!showSearch)
    }
    const setSearchItemVal = (text) => {
        let filtercont = allProduct.filter(product => {
            let lowerCase = (product.product_name).toLowerCase()
            console.log('-------', product, 'lowerCase', lowerCase)
            let searchToLowerCase = text.toLowerCase()
            return lowerCase.indexOf(searchToLowerCase) > -1
        })

        console.log('=============filtercont', filtercont)

        setSearchAllProduct(filtercont)
        setSearchItem(text)
    }


    const onlogout = async () => {
        Alert.alert(  
            '',  
            'Are you sure to log out',  
            [  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'OK', onPress: onlogoutSuccess},  
            ]  
        );  

    }

    const onlogoutSuccess = async() => {
        await AsyncStorage.removeItem('store_id')
        // await navigation.navigate('login_Screen')
        AsyncStorage.getAllKeys()
            .then(keys => AsyncStorage.multiRemove(keys))
            .then(() => {
               // alert('success')
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'login_Screen'}],
                  });
                
               // navigation.navigate('login_Screen')
            });
    }

    const renderItem = ({ item }) => {
        return (
            <TouchableOpacity style={styles.flatlistView}
                onPress={() => checkDetail(item)}>
                <View style={styles.nameBox}>
                    <Image source={{ uri: 'https://nuzo.co/product_images/' + item.product_image }} style={styles.product} />
                    <Text style={styles.resultText}> {item.product_name} {item.status == "2" && <Text style={styles.pauseText} >{"  (pause)"}</Text>}</Text>
                </View>

                <View style={styles.sentBox}>
                    <Text style={styles.resultText}>{item.sent}</Text>
                </View>

                <View style={styles.clickBox}>
                    <Text style={styles.resultText}>{item.total_clicks}</Text>
                </View>

                <View style={styles.ctrBox}>
                    <Text style={styles.resultText}>{item.total_orders}</Text>
                </View>
            </TouchableOpacity>
        )
    };

    return (
        <View style={styles.container}>
            <Header
                // leftIcon={Images.header_menu}
                // leftIconStyle={styles.menuLeftIcon}
                leftText="   Campaign"
                // onLeftPress={() => alert('open')}
                rightIcon={Images.searchIcon}
                rightIconStyle={styles.rightNextIconStyle}
                rightNextSecIcon={Images.logout}
                onRightPress={showSearchBox}
                onRightNextPress={onlogout}
                rightNextIconStyle={styles.rightNextLogoutStyle} />


            {showSearch ?
                <>
                    <TextInput
                        placeholder="search product"
                        value={searchItem}
                        onChangeText={(text) => setSearchItemVal(text)}
                        style={styles.searchBox}
                    />

                    <ScrollView
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                            />
                        }>
                        <View style={styles.flatlistView}>
                            <View style={styles.nameBox}>
                                <Text style={styles.textNameHeading}> {"Name"}</Text>
                            </View>

                            <View style={styles.sentBox}>
                                <Text style={styles.textHeading}>{"Sent"}</Text>
                            </View>

                            <View style={styles.clickBox}>
                                <Text style={styles.textHeading}>{"Clicks"}</Text>
                            </View>

                            <View style={styles.ctrBox}>
                                <Text style={styles.textHeading}>{"Orders"}</Text>
                            </View>
                        </View>

                        <FlatList
                            data={searchallProduct}
                            renderItem={renderItem}
                            keyExtractor={item => item.id}
                        />
                    </ScrollView>
                </>
                :

                <>
                    {loading ?
                        <Loader />
                        :
                        <ScrollView
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={onRefresh}
                                />
                            }>
                            <View style={styles.flatlistView}>
                                <View style={styles.nameBox}>
                                    <Text style={styles.textNameHeading}> {"Name"}</Text>
                                </View>

                                <View style={styles.sentBox}>
                                    <Text style={styles.textHeading}>{"Sent"}</Text>
                                </View>

                                <View style={styles.clickBox}>
                                    <Text style={styles.textHeading}>{"Clicks"}</Text>
                                </View>

                                <View style={styles.ctrBox}>
                                    <Text style={styles.textHeading}>{"Orders"}</Text>
                                </View>
                            </View>

                            <FlatList
                                data={allProduct}
                                renderItem={renderItem}
                                keyExtractor={item => item.id}
                            />
                        </ScrollView>

                    }
                </>
            }

            <TouchableOpacity style={styles.button}
                onPress={offerCreate}>
                <Text style={styles.buttonText}>Create an offer</Text>
            </TouchableOpacity>
        </View >
    )
}


