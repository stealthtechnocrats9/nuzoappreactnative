import { StyleSheet } from 'react-native';
import Fonts from '../../../../../assets/fonts';
import Color from '../../../../../utils/Colors';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
    },

    menuLeftIcon: {
        height: 16.5,
        width: 18.75,
        marginRight: 6
      },
 
    rightNextIconStyle: {
        height: 15, width: 15
    },
    rightNextLogoutStyle: {
        height: 20, width: 20

    },
    searchBox: {
        height: 52,
        borderWidth: 1,
        borderRadius: 16,
        marginHorizontal: 15,
        paddingLeft: 15,
        borderColor: Color.borderColor,
        color: Color.black_60,
        fontSize:16

    },
    flatlistView: {
        flexDirection: 'row',
        width: '90%',
        // backgroundColor: 'red',
        alignSelf: 'center',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10,
        paddingVertical: 8,
        
    },
    nameBox: {
        width: '55%',
        alignItems: 'center',
        flexDirection: 'row',
        paddingRight: 30,
    },
    product: {
        height: 35,
        width: 35,
        borderRadius: 35/2,
    },
    sentBox: {
        width: "13%",
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    clickBox: {
        width: "13%",
        alignItems: 'center',
    },
    ctrBox: {
        width: "18%",
        alignItems: 'center',
    },
    textHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue
    },
    textNameHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        marginLeft: 35
    },
    resultText:{
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        paddingHorizontal: 8
    },
    pauseText: {
        fontFamily: Fonts.Samsung,
        color: Color.red, 
         
    },
    button: {
        height: 52, 
        width: 152,
        backgroundColor: Color.skyBlue,
        alignItems: 'center',
         justifyContent: "center",
         borderRadius: 16,
         position: 'absolute',
         bottom: 20,
         right: 10
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.white,
        fontWeight:"600"
    }
})