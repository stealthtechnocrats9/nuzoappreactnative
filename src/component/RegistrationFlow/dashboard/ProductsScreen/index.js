
import React, { useState, useEffect, useCallback } from 'react'
import { Text, View, FlatList, Image, TouchableOpacity, TextInput, RefreshControl, ScrollView } from 'react-native'
import Header from '../../../../utils/Header/Header'
import Images from '../../../../utils/Images'
import styles from './styles'
import { useQuery } from '@apollo/client'
import { GET_ALL_PRODUCT } from '../../../../graphql/queries/getProduct/index'
import Loader from '../../../../utils/loader/index'
import { useIsFocused } from "@react-navigation/native";
import AsyncStorage from '@react-native-async-storage/async-storage'

// import { environment } from 'src/environments/environment';
// import firebase from '@react-native-firebase/app';
// import analytics from '@react-native-firebase/analytics';

// import crashlytics from "@react-native-firebase/crashlytics";
// import * as firebase from 'firebase';
// const firebaseConfig = {
//     apiKey: "AIzaSyBXUl9pjzwFlOyeIGL6AkVewGAC0nzRquE",
//     authDomain: "rn-web-89329.firebaseapp.com",
//     databaseURL: "https://rn-web-89329-default-rtdb.firebaseio.com",
//     projectId: "rn-web-89329",
//     storageBucket: "rn-web-89329.appspot.com",
//     messagingSenderId: "1071911615151",
//     appId: "1:1071911615151:web:a797d903add47978e8b25a",
//     measurementId: "G-ME9QCRHZPR"
//   };


const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}


export default function ProductsScreen({ navigation, route }) {
    const [storeId, setStoreId] = useState('')
    const isFocused = useIsFocused();
    const [refreshing, setRefreshing] = useState(false);
    const [showSearch, setShowSearch] = useState(false)
    const [searchItem, setSearchItem] = useState('')
    const { data, loading, refetch } = useQuery(GET_ALL_PRODUCT, {
        variables: {
            store_id: storeId
        }
    });

    const [allProduct, setAllProduct] = useState([])
    const [searchProduct, setSearchProduct] = useState([])
    const [isFetching, setIsFetching] = useState(false)


    useEffect(async () => {
        
       // const app= firebase.app("NUZOANDROIDAPP");

       // console.log(" ---- app",app);
        await AsyncStorage.getItem("store_id").then((res) => setStoreId(res)).catch((err) => { console.log(err) })

       // analytics().setCurrentScreen('Analytics');
    

    })


    //  console.log('-------storeId', storeId)
    useEffect(() => {
        if (data != undefined) {
            setAllProduct(data.products)
        }
    })

    
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            refetch()
            // The screen is focused
            // Call any action
        });

        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    const onRefreshScreen = useCallback(() => {
        setRefreshing(true);
        refetch()
        wait(200).then(() => setRefreshing(false));
    }, []);

    const onRefresh = () => {
        // this.setState({isFetching: true,},() => {this.getApiData();});
        setIsFetching(true), () => refetch()
    }

    const setTextInputValue = (text) => {
        let filtercont = allProduct.filter(product => {
            let lowerCase = (product.product_name).toLowerCase()
            console.log('-------', product, 'lowerCase', lowerCase)
            let searchToLowerCase = text.toLowerCase()
            return lowerCase.indexOf(searchToLowerCase) > -1
        })

        console.log('=============filtercont', filtercont)

        setSearchProduct(filtercont)
        setSearchItem(text)
    }

    const showSearchBox = () => {
        setSearchProduct(allProduct)

        setShowSearch(!showSearch)
    }

    const offerCreate = () => {
        navigation.navigate("product_create", { storeId, value: "dashboard" })

    }


    // console.log('product data is============', searchProducts,'error', searchError)
    //console.log('product data is===searchProducts=========', searchItem)
    const numberOfItem = searchProduct.length


    // console.log('product data is============', allProduct)
    const onEditProduct = (item) => {
        navigation.navigate('Product_Update', { item })
    }

    const handleRefresh = () => {

    }

    const renderItem = ({ item }) => {
        let img = item.product_image
        //  console.log('img', img)
        return (
            <TouchableOpacity style={styles.flatlistView}
                onPress={() => onEditProduct(item)}>
                <View style={styles.titleView}>
                    {img[0] ?
                        <Image source={{ uri: 'https://nuzo.co/product_images/' + img[0].product_image }}
                            style={{ height: 35, width: 35, marginRight: 10, borderRadius: 4 }} />
                        : <View style={{ height: 35, width: 35, marginRight: 10, borderRadius: 4 }} />}

                    <Text style={styles.titleText}>{item.product_name}</Text>
                </View>


                <View style={styles.priceView}>
                    <Text style={styles.priceText}>{item.currency} {item.product_price}</Text>
                </View>

            </TouchableOpacity>
        )
    };



    return (
        <View style={styles.container}>
            {loading ?
                <Loader />
                :
                <ScrollView showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefreshScreen}
                        />
                    }>
                    <Header
                        //leftIcon={Images.header_menu}
                        // leftIconStyle={styles.menuLeftIcon}
                        leftText="   Product"
                        // onLeftPress={() => alert('open')}
                        rightNextSecIcon={Images.searchIcon}
                        onRightNextPress={showSearchBox}
                        rightNextIconStyle={styles.rightNextIconStyle} />
                    {
                        showSearch ?
                            <>
                                <TextInput
                                    placeholder="search product"
                                    value={searchItem}
                                    onChangeText={(text) => setTextInputValue(text)}
                                    style={styles.searchBox}
                                />
                                <View style={styles.main}>
                                    <FlatList
                                        data={searchProduct}
                                        showsVerticalScrollIndicator={false}
                                        renderItem={renderItem}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>
                            </>

                            :
                            <View style={styles.main}>
                                <FlatList
                                    data={allProduct}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={renderItem}
                                    style={{ marginBottom: 50 }}
                                    keyExtractor={(item, index) => index.toString()}
                                    onRefresh={onRefresh}
                                    refreshing={isFetching}// onRefresh={handleRefresh}
                                />

                            </View>
                    }


                </ScrollView>
            }
            <TouchableOpacity style={styles.button}
                onPress={offerCreate}>
                <Text style={styles.buttonText}>Add product</Text>
            </TouchableOpacity>
        </View>
    )
}

