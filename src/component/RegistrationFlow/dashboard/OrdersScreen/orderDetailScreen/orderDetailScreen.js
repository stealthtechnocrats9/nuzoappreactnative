import React, { useState, useEffect } from 'react'
import { Text, View, Image, FlatList, Pressable } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Header from '../../../../../utils/Header/Header'
import Images from '../../../../../utils/Images'
import styles from './styles'
import Modal from 'react-native-modal'
import Color from '../../../../../utils/Colors'
import { useQuery, useMutation } from '@apollo/client'
import Loader from '../../../../../utils/loader/index'
import { GET_ORDERLIST_BY_ORDER_ID } from '../../../../../graphql/queries/orders/getOrderDetailbyOrderId'
import { CHANGE_ORDER_STATUS } from '../../../../../graphql/mutation/orders/index'
import moment from 'moment'
import { updateContact } from 'react-native-contacts'


export default function orderDetailScreen({ navigation, route }) {

    const order_id = route.params.id
    const [modalVisible, setModalVisible] = useState(false);
    const [modalCancelVisible, setModalCancelVisible] = useState(false)
    const [allData, setAllData] = useState()
    const [orderStatus_changed, { data: statusData, loading, error: errorInDataGet }] = useMutation(CHANGE_ORDER_STATUS)
    const { data: getOrderList, loading: getAllOrederLOading, error: getAllerror, refetch } = useQuery(GET_ORDERLIST_BY_ORDER_ID, {
        variables: {
            order_id: order_id
        }
    });

  
    useEffect(() => {
        if (getOrderList) {
            setAllData(getOrderList.getOrderDetailsByOrderId)
        }
    })


    const onOrderReject = () => {
        setModalCancelVisible(!modalCancelVisible)
    }

    const onOrderSuccess = () => {
        setModalVisible(!modalVisible)
    }


    const onOrderRejectSuccess = () => {
        orderStatus_changed({
            variables: {
                order_id: order_id,
                status: "2"
            }
        }).then((res) => {
            if (res.data.changeOrderStatus) {
                alert('Order cancelled successfully')
                navigation.navigate('userRegistration_bottomTabs', { screen: 'OrdersScreen' })
                setModalVisible(false)
            }
        })
    }
    const onOrderRecievedSuccess = () => {
        orderStatus_changed({
            variables: {
                order_id: order_id,
                status: "1"
            }
        }).then((res)=> {
            alert('Payment received successfully')
            navigation.navigate("orderDetailStatusChange", { order_id })
            setModalCancelVisible(false)
            setModalVisible(false)
        }).catch((err)=> console.log('err', err))
    }

    // console.log('------res data', allData, order_id, getOrderList)
    // console.log('------res getOrderList', getOrderList)

    const startDate = new Date().getTime();
    const d = allData ? allData.offer_detail.expire_date : new Date()
    const expireDate = new Date(d).getTime()
    const timeDiff = expireDate - startDate
    const totalDay = Math.floor(timeDiff / (1000 * 60 * 60 * 24))
    const totalHours = Math.floor((timeDiff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))



    return (
        <View style={styles.container}>
            <Header
                leftNextText="Order Details"
                leftIcon={Images.backArrow}
                leftIconStyle={styles.leftIconStyle}
                onLeftPress={() => navigation.goBack()} />


            {getAllOrederLOading
                ?
                <Loader />
                :
                <>
                    {allData &&
                        <View style={styles.main}>
                            {/* <View style={styles.instructionBox}>
                                <Image source={Images.warning} style={styles.warningLogo} />
                                <Text style={styles.warningText}>Orders not accepted within 60 minutes are automatically cancelled.</Text>
                            </View> */}

                            <View style={styles.responseButtons}>
                                <TouchableOpacity style={styles.rejectButton} onPress={onOrderReject}>
                                    <Text style={styles.rejectButtonText}>Payment Failed</Text>
                                </TouchableOpacity>

                                <Modal
                                    testID={'modal'}
                                    isVisible={modalCancelVisible}
                                    swipeDirection={['left']}
                                    transparent={true}
                                    propagateSwipe={true}
                                    onBackdropPress={() => setModalCancelVisible(false)}
                                >
                                    <View style={styles.modelView}>
                                        <Text style={styles.modalHeading}>Reject order?</Text>
                                        <Text style={styles.modalText}>Are you sure you want to reject this order?</Text>

                                        <View style={styles.modelButton}>
                                            <Pressable
                                                onPress={onOrderRejectSuccess}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Reject order</Text>
                                                )}
                                            </Pressable>
                                            <Pressable
                                                onPress={() => setModalCancelVisible(false)}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Go back</Text>
                                                )}
                                            </Pressable>

                                        </View>
                                    </View>
                                </Modal>


                                <TouchableOpacity style={styles.acceptButton} onPress={onOrderSuccess}>
                                    <Text style={styles.acceptButtonText}>Payment Received</Text>
                                </TouchableOpacity>


                                <Modal
                                    testID={'modal'}
                                    isVisible={modalVisible}
                                    swipeDirection={['left']}
                                    transparent={true}
                                    propagateSwipe={true}
                                    onBackdropPress={() => setModalVisible(false)}
                                >
                                    <View style={styles.modelView}>
                                        <Text style={styles.modalHeading}>Payment Received?</Text>
                                        <Text style={styles.modalText}>Are you sure you want to received this order?</Text>

                                        <View style={styles.modelButton}>
                                            <Pressable
                                                onPress={onOrderRecievedSuccess}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Payment Received</Text>
                                                )}
                                            </Pressable>
                                            <Pressable
                                                onPress={() => setModalVisible(false)}
                                                style={({ pressed }) => [
                                                    {
                                                        backgroundColor: pressed
                                                            ? Color.red
                                                            : Color.white
                                                    },
                                                    styles.modelEditButton
                                                ]}>

                                                {({ pressed }) => (
                                                    <Text style={
                                                        pressed ? styles.modelActiveButtonTextStyle : styles.modelButtonTextStyle}>Go back</Text>
                                                )}
                                            </Pressable>

                                        </View>
                                    </View>
                                </Modal>

                            </View>

                            <View style={styles.orderHeadingBox}>
                                <View style={styles.orderName}>
                                    <Text style={styles.orderNameText}>
                                        {allData ? allData.customer_detail.name : null}
                                    </Text>
                                </View>
                                <View style={styles.timeLeft}>
                                    {totalDay && totalHours >= 0 ?
                                    <Text style={styles.orderDetailText}> {totalDay ? totalDay + " " + "days" : null}
                                        {totalHours ? " " + totalHours + " " + "hours" : null} left</Text>
                                        :
                                        <Text style={styles.orderDetailText}>0 day 0 hours left</Text>
                                    }
                                </View>
                                <View style={styles.orderDetail}>
                                    <Text style={styles.orderDetailStatus}>
                                        {allData ? allData.order_status == 0 ?
                                            "Pending" : allData.order_status == 1 ?
                                                "Active" : allData.order_status == 2 ?
                                                    "Rejected" : allData.order_status == 3 ?
                                                        "Ready" : "Delivered" : null}
                                    </Text>
                                </View>
                            </View>


                            <View style={styles.orderAllDetailBox}>
                                <View style={styles.orderDetailHeading}>
                                    <Text style={styles.orderNameText}>Order details - #{allData.order_no}</Text>
                                    <Text style={styles.orderNameText}>Delivery</Text>
                                </View>
                                <Text style={styles.orderedItemText}>{moment.unix(allData.created_at / 1000).format("DD MMM YYYY hh:mm a")}</Text>
                                <View style={styles.orderDetailHeading}>
                                    <Text style={styles.allorderDetailHeading}>item</Text>
                                    <Text style={styles.allorderDetailHeading}>Qty</Text>
                                </View>
                                <View style={styles.orderDetailHeading}>
                                    <View style={{ width: '70%' }}>
                                        <Text style={styles.orderedItemText}>{allData ? allData.product_detail.product_name : null}</Text>
                                    </View>
                                    <View style={{ width: '30%', alignItems: 'flex-end' }}>
                                        <Text style={styles.orderedItemText}>{"1"}</Text>
                                    </View>
                                </View>

                            </View>
                        </View>
                    }
                </>
            }
        </View>
    )
}



