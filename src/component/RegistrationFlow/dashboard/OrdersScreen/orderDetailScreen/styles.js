import { StyleSheet } from 'react-native';
import Fonts from '../../../../../assets/fonts';
import Color from '../../../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
    },
    leftIconStyle: {
        height: 16,
        width: 15.87
    },
    main: { marginHorizontal: 20 },
    instructionBox: {
        height: 40,
        backgroundColor: Color.lightYellow,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12,
        marginVertical: 16,
        flexDirection: 'row',
        alignItems: 'center'
    },
    warningLogo: {
        width: 16.67,
        height: 16.67,
        marginRight: 9.7
    },
    warningText: {
        fontSize: 9,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        fontWeight: "500"
    },
    responseButtons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 5
    },
    rejectButton: {
        width: 152,
        height: 52,
        borderWidth: 1,
        borderColor: Color.red,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    acceptButton: {
        width: 152,
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    rejectButtonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.red
    },
    acceptButtonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.white
    },
    orderHeadingBox: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        marginTop: 22,
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        justifyContent: 'space-between'
    },
    orderName: {
        width: '40%',

    },
    timeLeft: {
        width: '35%',
        alignItems: 'flex-end'

    },
    orderDetail: {
        width: '25%',
        alignItems: 'flex-end'

    },
    orderNameText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.black_60
    },
    orderDetailText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.black
    },
    orderDetailStatus: {
        color: Color.orange,
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        paddingHorizontal: 8,
         paddingVertical: 2,
         backgroundColor: Color.lightYellow
    },
    orderAllDetailBox: {
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        marginTop: 22,
        marginBottom: 15,
        padding: 15
    },
    orderDetailHeading: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    orderedItemText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginVertical: 16
    },
    allorderDetailHeading: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue
    },
    modelView: {
        width: '100%',
        backgroundColor: Color.white,
        borderRadius: 10,
        paddingVertical: 25
    },
    modalHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        alignSelf: 'center',
        fontWeight: '700'
    },
    modalText: {
        alignSelf: 'center',
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 16,
        marginHorizontal: 50,
        textAlign: 'center',
        marginBottom: 41
    },
    modelButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 16

    },
    modelEditButton: {
        height: 44,
        borderColor: Color.red,
        width: 152,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelSendButton: {
        height: 44,
        width: 152,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelButtonTextStyle: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue
    },
    modelActiveButtonTextStyle: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.white
    }
})