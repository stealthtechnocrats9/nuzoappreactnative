import React, { useState, useEffect, useCallback } from 'react'
import { SafeAreaView, Text, View, ScrollView, RefreshControl, TouchableOpacity, FlatList, TextInput } from 'react-native'
import styles from './styles'
import Images from '../../../../../utils/Images'
import Header from '../../../../../utils/Header/Header'
import { useQuery, useMutation } from '@apollo/client'
import Loader from '../../../../../utils/loader/index'
import { GET_ALL_ORDER } from '../../../../../graphql/queries/orders/getAllOrder'
import moment from 'moment'
import AsyncStorage from '@react-native-async-storage/async-storage'
//import analytics from '@react-native-firebase/analytics';


const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

export default function OrdersScreen({ navigation, route }) {

    const [storeId, setStoreId] = useState('')
    const [refreshing, setRefreshing] = useState(false);
    const [showSearch, setShowSearch] = useState(false)
    const [searchItem, setSearchItem] = useState('')
    const [searchItemval, setSearchItemVal] = useState('')

    const [headerText, setHeaderText] = useState(["All", "Active", "Ready", "Pending", "Delivered", "Cancelled"])
    const [isActive, setIsActive] = useState(0)
    const [allData, setAllData] = useState([])
    const [searchData, setSearchData] = useState([])
    const { data: getALLOrder, loading: getAllOrederLOading, error: getAllerror, refetch } = useQuery(GET_ALL_ORDER)


    const onRefresh = useCallback(() => {
        setRefreshing(true);
        refetch()
        wait(200).then(() => setRefreshing(false));
    }, []);


    useEffect(async () => {
        await AsyncStorage.getItem("store_id").then((res) => setStoreId(res)).catch((err) => { console.log(err) })

        if (getALLOrder) {
            await setAllData(getALLOrder.orders)
           // await setSearchData(getALLOrder.orders)
        }
    })

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            refetch()
            // The screen is focused
            // Call any action
        });

        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);


    const showSearchBox = () => {
            console.log('-----------is active status-----', isActive)

        if(isActive==0){
            setSearchData(allData)
        }else if(isActive==1){
            setSearchData(ActiveData)
        }else if(isActive==2){
            setSearchData(readyData)
        }else if(isActive==3){
            setSearchData(PendingData)
        }else if(isActive==4){
            setSearchData(deliveredData)
        }else if(isActive==5){
            setSearchData(cancelledData)
        }else{
            setSearchData(allData)
        }
        setSearchItemVal(searchData)

        setShowSearch(!showSearch)
        // analytics.logEvent('test event called');
    }


    const setSearchItemchangeVal = (text) => {
        let filtercont = searchData.filter(product => {
            let lowerCase = (product.product_detail.product_name).toLowerCase()
            console.log('-------', product, 'lowerCase', lowerCase)
            let searchToLowerCase = text.toLowerCase()
            return lowerCase.indexOf(searchToLowerCase) > -1
        })

        console.log('=============filtercont', filtercont)

        setSearchItemVal(filtercont)
        setSearchItem(text)
    }


    // console.log('-----------getALLOrder', getALLOrder)

    console.log('-------alldata', storeId, allData)


    const ActiveData = allData.filter(function (item) {
        return item.order_status == 1
    })

    const readyData = allData.filter(function (item) {
        return item.order_status == 3
    })

    const PendingData = allData.filter(function (item) {
        return item.order_status == 0
    })

    const deliveredData = allData.filter(function (item) {
        return item.order_status == 4
    })

    const cancelledData = allData.filter(function (item) {
        return item.order_status == 2
    })

    const onDetailCheck = (id, item) => {
        console.log("--logo--", item)
        if (item.order_status == "0") {
            navigation.navigate("orderDetailScreen", { id })
        }
        else {
            navigation.navigate("orderDetailStatusChange", { order_id: id, order_status: item.order_status })
        }
    }

    const renderAllItem = ({ item }) => {
        // console.log("====items", item)
        // console.log('val, val', val)
        return (
            <TouchableOpacity style={styles.orderDetailView}
                onPress={() => onDetailCheck(item._id, item)}>
                <View style={styles.insideBoxView}>
                    <Text style={styles.orderName}>{moment.unix(item.created_at / 1000).format("DD MMM YYYY hh:mm a")}</Text>


                    <Text style={item.order_status == 0 ? styles.orderpendingStatusText :
                        item.order_status == 2 ? styles.orderCancelStatusText :
                            item.order_status == 4 ? styles.orderDeliverdeStatusText :
                                styles.orderActiveStatusText}>
                        {item.order_status == 0 ? "Pending" : item.order_status == 1 ? "Active"
                            : item.order_status == 2 ? "Cancelled" : item.order_status == 3 ? "Ready" : "Delivered"}</Text>

                </View>

                <Text style={styles.productName}>{item.product_detail.product_name}</Text>

                <View style={styles.insideBoxView}>
                    <Text style={styles.orderprice}> {item.product_detail.currency} {item.total}</Text>
                    <Text style={styles.orderprice}>{"Pickup"}</Text>
                </View>
            </TouchableOpacity>
        )
    }





    const renderPendingItem = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => onDetailCheck(item._id, item)} style={styles.orderDetailView}>
                <View style={styles.insideBoxView}>
                    <Text style={styles.orderName}>{moment.unix(item.created_at / 1000).format("DD MMM YYYY hh:mm a")}</Text>
                    <Text style={item.order_status == 0 ?
                        styles.orderpendingStatusText : item.order_status == 1 ?
                            styles.orderActiveStatusText : item.order_status == 3 ?
                                styles.orderReadyStatusText : item.order_status == 2 ?
                                    styles.orderCancelStatusText : styles.orderDeliverdeStatusText}>

                        {item.order_status == 0 ? "Pending" : item.order_status == 1 ? "Active"
                            : item.order_status == 2 ? "Cancelled" : item.order_status == 3 ? "Ready" : "Delivered"}</Text>
                </View>
                <Text style={styles.productName}>{item.product_detail.product_name}</Text>
                <View style={styles.insideBoxView}>
                    <Text style={styles.orderprice}> {item.product_detail.currency} {item.total}</Text>
                    <Text style={styles.orderprice}>{"Pickup"}</Text>
                </View>

            </TouchableOpacity>
        )
    }


    const onSelectHeaderItem = (index) => {
        setIsActive(index)
    }
    // console.log('----------------', isActive)

    const renderHeadingItem = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => onSelectHeaderItem(index)}>
                {isActive != index ?
                    <View style={styles.hesaindTextView}>
                        <Text style={styles.headingText} >{item}</Text>
                    </View>
                    :
                    <View style={styles.hesaindTextView}>
                        <Text style={styles.activeHeadingText} >{item}</Text>
                    </View>
                }
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.container}>
            <Header
                //  leftIcon={Images.header_menu}
                //leftIconStyle={styles.menuLeftIcon}
                leftText="   Orders"
                // onLeftPress={() => alert('open')}
                rightNextSecIcon={Images.searchIcon}
                onRightNextPress={showSearchBox}
                rightNextIconStyle={styles.rightNextIconStyle} />

            {
                showSearch ?
                    <>
                        <TextInput
                            placeholder="search product"
                            value={searchItem}
                            onChangeText={(text) => setSearchItemchangeVal(text)}
                            style={styles.searchBox}
                        />
                        <FlatList
                            data={searchItemval}
                            renderItem={renderAllItem}
                            keyExtractor={(item, index) => index.toString()}
                            showsVerticalScrollIndicator={false}
                        />
                    </>


                    :

                    <ScrollView
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                            />
                        }>


                        <View style={styles.header}>
                            {
                                getAllOrederLOading ?
                                    <Loader />
                                    :
                                    <FlatList
                                        data={headerText}
                                        horizontal
                                        renderItem={renderHeadingItem}
                                        keyExtractor={(item, index) => index.toString()}
                                        showsHorizontalScrollIndicator={false}
                                    />
                            }
                        </View>


                        <View style={styles.flatlistView}>
                            {isActive == 0 &&
                                <FlatList
                                    data={allData}
                                    renderItem={renderAllItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsVerticalScrollIndicator={false}
                                />
                            }

                            {isActive == 1 &&
                                <FlatList
                                    data={ActiveData}
                                    renderItem={renderPendingItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsVerticalScrollIndicator={false}
                                />
                            }

                            {isActive == 2 &&
                                <FlatList
                                    data={readyData}
                                    renderItem={renderPendingItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsVerticalScrollIndicator={false}
                                />
                            }

                            {isActive == 3 &&
                                <FlatList
                                    data={PendingData}
                                    renderItem={renderPendingItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsVerticalScrollIndicator={false}
                                />
                            }

                            {isActive == 4 &&
                                <FlatList
                                    data={deliveredData}
                                    renderItem={renderPendingItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsVerticalScrollIndicator={false}
                                />
                            }

                            {isActive == 5 &&
                                <FlatList
                                    data={cancelledData}
                                    renderItem={renderPendingItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    showsVerticalScrollIndicator={false}
                                />
                            }



                        </View>
                    </ScrollView>
            }
        </View>
    );
}