import { StyleSheet } from 'react-native';
import Fonts from '../../../../../assets/fonts';
import Color from '../../../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.webbackgroud,
    },
    menuLeftIcon: {
        height: 16.5,
        width: 18.75,
        marginRight: 6
    },
    rightNextIconStyle: {
        height: 15,
        width: 15
    },
    searchBox: {
        height: 52,
        borderWidth: 1,
        borderRadius: 16,
        marginHorizontal: 15,
        paddingLeft: 15,
        borderColor: Color.borderColor,
        color: Color.black

    },

    header: {
        height: 70,
        flexDirection: 'row',
        alignItems: 'center',

    },

    headingText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        fontWeight: "500",
    },
    hesaindTextView: {
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0,
        width: 100,

    },
    activeHeadingText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontWeight: "500",
        backgroundColor: Color.skyBlue,
        paddingHorizontal: 16,
        paddingVertical: 8,
        borderRadius: 23
    },
    imageStyle: {
        alignItems: 'center'
    },
    image: {
        height: 192,
        width: 290,
        marginBottom: 18,
    },
    heading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: 'bold',
        color: Color.black,
        // fontFamily: "samsung Sharp Sans"
    },
    title: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: 'bold',
        color: Color.black_80,
        // fontFamily: "samsung Sharp Sans"
    },
    button: {
        height: 52,
        width: 152,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,

        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: '600',
    },
    backButton: {
        alignSelf: 'center',
        marginTop: 29,
        color: Color.skyBlue,
        // fontFamily: "Samsung Sharp Sans",
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        fontWeight: "700"
    },
    flatlistView: {
        marginHorizontal: 20
    },
    orderDetailView: {
        marginVertical: 8,
        backgroundColor: Color.white,
        shadowColor: "#000000",
        paddingBottom: 8,
        paddingTop: 12,
        borderRadius: 3,
        shadowOpacity: 0.05,
        shadowRadius: 12,
        shadowOffset: {
            height: 0,
            width: 0
        },
        elevation: 1,
        paddingLeft: 16,
        paddingRight: 18,
    },
    insideBoxView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    productName: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: "700",
        color: Color.blue_7,
        marginTop: 8,
        marginBottom: 8
    },

    orderName: {
        fontSize: 13,

        fontFamily: Fonts.Samsung, 
        color: Color.black,
    },
    orderprice: {
        fontSize: 13,

        fontFamily: Fonts.Samsung, 
        color: Color.blue_8,
        fontWeight: "700"
    },
    detailText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginRight: 5
    },
    currency: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.green
    },
    orderDeliverdeStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung, 
        color: Color.black_60,
        paddingHorizontal: 8,
        backgroundColor: Color.black_5,
        paddingVertical: 4,
        borderRadius: 16
    },
    orderActiveStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.green,
        paddingHorizontal: 8,
        backgroundColor: Color.lightGreen,
        paddingVertical: 4,
        borderRadius: 16
    },
    orderReadyStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.green,
        paddingHorizontal: 8,
        backgroundColor: Color.lightGreen,
        paddingVertical: 4,
        borderRadius: 16
    },
    orderDeliveredStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.green,
        paddingHorizontal: 8,
        backgroundColor: Color.black_60,
        paddingVertical: 4,
        borderRadius: 16
   
    },
    orderCancelStatusText: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
         color: Color.red,
        paddingVertical: 4,
        paddingHorizontal: 8,
        backgroundColor: Color.lightRed,
        borderRadius: 16
    },

    orderpendingStatusText: {
        fontSize: 13,

        fontFamily: Fonts.Samsung, color: Color.yellow_30,
        paddingVertical: 4,
        paddingHorizontal: 8,
        backgroundColor: Color.yellow_50,
        borderRadius: 16
    },
})