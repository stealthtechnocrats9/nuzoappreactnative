import { StyleSheet } from 'react-native';
import Color from '../../../../../utils/Colors'
import Fonts from '../../../../../assets/fonts'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
    },
    leftIconStyle: {
        height: 16,
        width: 15.87
    },
    main: { marginHorizontal: 20 },
    instructionBox: {
        height: 52,
        backgroundColor: Color.lightYellow,
        borderRadius: 16,
        paddingVertical: 10,
        paddingHorizontal: 12,
        marginVertical: 16,
        flexDirection: 'row',
        alignItems: 'center'
    },
    warningLogo: {
        width: 16.67,
        height: 16.67,
        marginRight: 9.7
    },
    dropdownBox: {
        width: 150,
        paddingVertical: 8,
        borderRadius: 1,
        position: 'absolute',
        backgroundColor: Color.white,
        top: 60,
        right: 5,
        zIndex: 1,
        shadowColor: "#000000",
        shadowOpacity: 0.05,
        shadowRadius: 12,
        shadowOffset: {
            height: 0,
            width: 0
        },
        elevation: 1,
    },
    dropdownVal: {
        marginVertical: 3,
        height: 25,
    },
    dropdownValText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.black_60,
        textAlign: 'center'
    },
    warningText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        fontWeight: "500"
    },
    responseButtons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 5
    },
    rejectButton: {
        width: 152,
        height: 52,
        borderWidth: 1,
        borderColor: Color.red,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    acceptButton: {
        width: 152,
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    rejectButtonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,

        fontWeight: "600",
        color: Color.red
    },
    acceptButtonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,

        fontWeight: "600",
        color: Color.white
    },
    orderHeadingBox: {
        height: 52,
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        marginTop: 2,
        alignItems: 'center',
        marginBottom: 15,
        flexDirection: 'row',
        paddingHorizontal: 15,
        justifyContent: 'space-between'
    },

    orderStatusText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.black,
        fontWeight: 'bold',
        fontFamily: Fonts.Samsung,

    },
    orderNameText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.black_60,
        fontWeight: '600',
        fontFamily: Fonts.Samsung,

    },

    orderDetailStatus: {
        color: Color.orange,
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        paddingHorizontal: 8,
        paddingVertical: 2,
        backgroundColor: Color.lightYellow
    },
    orderAllDetailBox: {
        borderRadius: 15,
        borderWidth: 1,
        borderColor: Color.borderColor,
        marginTop: 22,
        marginBottom: 15,
        padding: 15
    },
    orderDetailHeading: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    insideBoxLeftItem: {
        width: "70%",
        flexDirection: 'row',
        alignItems: 'center',
    },

    check_box: {
        height: 16,
        width: 16,
        marginRight: 10
    },
    orderedItemText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.black_60,
        marginVertical: 16
    },
    allorderDetailHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.skyBlue
    },
    modelView: {
        width: '100%',
        backgroundColor: Color.white,
        borderRadius: 10,
        paddingVertical: 25
    },
    modalHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.black,
        alignSelf: 'center',
        fontWeight: '700'
    },
    modalText: {
        alignSelf: 'center',
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 16,
        marginHorizontal: 50,
        textAlign: 'center',
        marginBottom: 41
    },
    modelButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 16

    },
    modelEditButton: {
        height: 52,
        borderColor: Color.red,
        width: 152,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelSendButton: {
        height: 52,
        width: 152,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modelButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: '600',

        color: Color.skyBlue
    },
    modelActiveButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: '600',

        color: Color.white
    },
    orderDeliverdeStatusText: {
        fontSize: 15,

        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        paddingHorizontal: 8,
        backgroundColor: Color.black_5,
        paddingVertical: 4,
        borderRadius: 16
    },
    orderActiveStatusText: {
        fontSize: 15,

        fontFamily: Fonts.Samsung,
        color: Color.green,
        paddingHorizontal: 8,
        backgroundColor: Color.lightGreen,
        paddingVertical: 4,
        borderRadius: 16


    },
    orderCancelStatusText: {
        fontSize: 15,

        fontFamily: Fonts.Samsung,
        color: Color.red,
        paddingVertical: 4,
        paddingHorizontal: 8,
        backgroundColor: Color.lightRed,
        borderRadius: 16
    },

    orderpendingStatusText: {
        fontSize: 15,

        fontFamily: Fonts.Samsung,
        color: Color.yellow_30,
        paddingVertical: 4,
        paddingHorizontal: 8,
        backgroundColor: Color.yellow_50,
        borderRadius: 16
    },
})