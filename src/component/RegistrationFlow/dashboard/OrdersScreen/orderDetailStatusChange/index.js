import React, { useState, useEffect } from 'react'
import { Text, View, Image, FlatList, Pressable, Alert } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Header from '../../../../../utils/Header/Header'
import Images from '../../../../../utils/Images'
import styles from './styles'
import { useQuery, useMutation } from '@apollo/client'
import { useIsFocused } from "@react-navigation/native";
import Loader from '../../../../../utils/loader/index'
import { GET_ORDERLIST_BY_ORDER_ID } from '../../../../../graphql/queries/orders/getOrderDetailbyOrderId'
import moment from 'moment'
import { CHANGE_ORDER_STATUS } from '../../../../../graphql/mutation/orders/index'



export default function orderDetailStatusChange({ navigation, route }) {
    const order_id = route.params.order_id
    const order_status = route.params.order_status
    const isFocused = useIsFocused();
    const [order_status_main, setOrder_status_main] = useState(route.params.order_status)
    const [allData, setAllData] = useState()
    const [isModelVisibal, setModalVisibal] = useState(false)
    const [orderStatus_changed, { data: statusData, loading, error: errorInDataGet }] = useMutation(CHANGE_ORDER_STATUS)
    const { data: getOrderList, loading: getAllOrederLOading, error: getAllerror, refetch } = useQuery(GET_ORDERLIST_BY_ORDER_ID, {
        variables: {
            order_id: order_id
        }
    });
    const [actualStatusVal, setActualStatusVal] = useState('')



    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            refetch()
        });
        return unsubscribe;
    }, [navigation, statusData, getOrderList]);


    //   useEffect(() => {
    //     const unsubscribe = navigation.addListener('focus', () => {
    //         refetch()
    //     });
    //     return unsubscribe;
    //   }, [data]);

    useEffect(() => {
        if (getOrderList) {
            setAllData(getOrderList.getOrderDetailsByOrderId)
        }
        if (allData) {
            setOrder_status_main(allData.order_status)
        }
    })
    console.log('------------all data bal', route.params, 'all data', allData)
    console.log('------------all statusData bal', getOrderList)


    const onStatusChange = () => {
        orderStatus_changed({
            variables: {
                order_id: order_id,
                status: "3"
            }
        }).then((res) => {
            refetch()
            Alert.alert(
                'Alert',
                'Order status changed successfully', // <- this part is optional, you can pass an empty string
                [
                    {
                        text: 'OK', onPress: () => {
                            console.log('OK Pressed', res)
                            refetch()
                        }
                    },
                ],
                { cancelable: false },
            );
            //  }
        }).catch((err) => { console.log(err) })

    }

    const isModelOpen = () => {
        console.log(actualStatusVal)
        if (order_status == 1 || order_status == 2 || order_status == 4) {
            setModalVisibal(false)
        }
        else {
            setModalVisibal(!isModelVisibal)
        }
    }

    const onStatusChangeToDelivered = () => {
        orderStatus_changed({
            variables: {
                order_id: order_id,
                status: "4"
            }
        }).then((res) => {
            //  if (res.data.updateProduct) {
            Alert.alert(
                'Alert',
                'Order delivered successfully', // <- this part is optional, you can pass an empty string
                [
                    {
                        text: 'OK', onPress: () => {
                            console.log('OK Pressed')
                            navigation.navigate('userRegistration_bottomTabs', { screen: 'OrdersScreen' })
                        }
                    },
                ],
                { cancelable: false },
            );
            //  }
        }).catch((err) => { console.log(err) })

    }

    const onStatusChangeToCancel = () => {
        orderStatus_changed({
            variables: {
                order_id: order_id,
                status: "2"
            }
        }).then((res) => {
            console.log("------cancelled--", res.data)
            if (res.data) {
                Alert.alert(
                    'Alert',
                    'Order cancelled successfully', // <- this part is optional, you can pass an empty string
                    [
                        {
                            text: 'OK', onPress: () => {
                                console.log('OK Pressed')
                                navigation.navigate('userRegistration_bottomTabs', { screen: 'OrdersScreen' })
                            }
                        },
                    ],
                    { cancelable: false },
                );
            }
        }).catch((err) => { console.log(err) })

    }


    return (
        <View style={styles.container}>
            <Header
                leftNextText="Order Details"
                leftIcon={Images.backArrow}
                leftIconStyle={styles.leftIconStyle}
                onLeftPress={() => navigation.navigate('userRegistration_bottomTabs', { screen: 'OrdersScreen' })} />

            {getAllOrederLOading || loading ?
                <Loader />
                :
                <View style={styles.main}>
                    <View style={styles.orderHeadingBox}>
                        <Text style={styles.orderNameText}>
                            {allData ? allData.customer_detail.name : null}
                        </Text>
                        <TouchableOpacity onPress={isModelOpen}>
                            <Text style={order_status_main == 2 ? styles.orderCancelStatusText :
                                order_status_main == 4 ? styles.orderDeliverdeStatusText :
                                    styles.orderActiveStatusText}>
                                {order_status_main == 1 ? "Active"
                                    : order_status_main == 2 ? "Cancelled" : order_status_main == 3 ? "ready" : "Delivered"}</Text>
                        </TouchableOpacity>
                    </View>
                    {isModelVisibal &&
                        <View style={styles.dropdownBox}>
                            <TouchableOpacity style={styles.dropdownVal} onPress={onStatusChangeToDelivered}>
                                <Text style={styles.dropdownValText}>{"Delivered"}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.dropdownVal} onPress={onStatusChangeToCancel}>
                                <Text style={styles.dropdownValText}>{"Cancel"}</Text>
                            </TouchableOpacity>

                        </View>

                    }

                    {/* <View style={styles.instructionBox}>
                        <Image source={Images.warning} style={styles.warningLogo} />
                        <Text style={styles.warningText}>Orders not accepted within 60 minutes are automatically cancelled.</Text>
                    </View> */}



                    {/* {order_status == 2 || order_status == 4 ? */}
                    {order_status == 2 ?
                        <View style={styles.orderAllDetailBox}>
                            <View style={styles.orderDetailHeading}>
                                <Text style={styles.orderNameText}>Order details - #{allData ? allData.order_no : null}</Text>
                                {/* <Text style={styles.orderStatusText}>Delivery</Text> */}
                            </View>
                            <Text style={styles.orderedItemText}>{allData ? moment.unix(allData.created_at / 1000).format("DD MMM YYYY hh:mm a") : null}</Text>

                        </View>
                        : order_status == 4 ?

                            <View style={styles.orderAllDetailBox}>
                                <View style={styles.orderDetailHeading}>
                                    <Text style={styles.orderNameText}>Order details - #{allData ? allData.order_no : null}</Text>
                                    {/* <Text style={styles.orderStatusText}>Delivery</Text> */}
                                </View>
                                <Text style={styles.orderedItemText}>{allData ? moment.unix(allData.created_at / 1000).format("DD MMM YYYY hh:mm a") : null}</Text>
                                <View style={styles.orderDetailHeading}>
                                    <Text style={styles.allorderDetailHeading}>item</Text>
                                    <Text style={styles.allorderDetailHeading}>Qty</Text>
                                </View>


                                <View style={styles.orderDetailHeading}>
                                    <View style={styles.insideBoxLeftItem}>
                                        <Image source={Images.select_check_box} style={styles.check_box} />

                                        <Text style={styles.orderedItemText}>{allData ? allData.product_detail.product_name : null}</Text>
                                    </View>
                                    <View style={{ width: '10%', alignItems: 'flex-end' }}>
                                        <Text style={styles.orderedItemText}>{'1'}</Text>
                                    </View>
                                </View>

                            </View>
                            :

                            <View style={styles.orderAllDetailBox}>
                                <View style={styles.orderDetailHeading}>
                                    <Text style={styles.orderNameText}>Order details - #{allData ? allData.order_no : null}</Text>
                                    {/* <Text style={styles.orderStatusText}>Delivery</Text> */}
                                </View>
                                <Text style={styles.orderedItemText}>{allData ? moment.unix(allData.created_at / 1000).format("DD MMM YYYY hh:mm a") : null}</Text>
                                <View style={styles.orderDetailHeading}>
                                    <Text style={styles.allorderDetailHeading}>item</Text>
                                    <Text style={styles.allorderDetailHeading}>Qty</Text>
                                </View>


                                <View style={styles.orderDetailHeading}>
                                    <View style={styles.insideBoxLeftItem}>
                                        {allData ?
                                            <>

                                                {
                                                    allData.order_status != 3 &&
                                                    <TouchableOpacity onPress={onStatusChange}>
                                                        <Image source={Images.check_box} style={styles.check_box} />
                                                    </TouchableOpacity>
                                                }
                                                {allData.order_status == 3 &&
                                                        <Image source={Images.select_check_box} style={styles.check_box} />
                                                }
                                            </>

                                            : null}
                                        <Text style={styles.orderedItemText}>{allData ? allData.product_detail.product_name : null}</Text>
                                    </View>
                                    <View style={{ width: '10%', alignItems: 'flex-end' }}>
                                        <Text style={styles.orderedItemText}>{'1'}</Text>
                                    </View>
                                </View>

                            </View>
                    }

                </View>
            }
        </View>
    )
}
