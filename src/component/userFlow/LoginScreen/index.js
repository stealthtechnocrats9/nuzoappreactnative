import React, { useState, useEffect } from 'react'
import { Text, View, Image, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native'
import styles from './styles'
import back_arrow from '../../../assets/back_arrow.png'
import loginNuzoPoints from '../../../assets/loginNuzoPoints.png'
import { CUSTOMER_LOGIN } from '../../../graphql/customerMutation/customerLogin'
import { useMutation } from '@apollo/client'
import Color from '../../../utils/Colors'

import dropdown from '../../../assets/dropdown.png'

export default function LoginScreen({ navigation, route }) {
    const [email, setEmail] = useState('')
    const [code, setCode] = useState('+254')
    const [country_name, setCountry_name] = useState('ke')
    const [errorMessage, setErrorMessage] = useState(null)
    const [userLogin, { data, loading, error }] = useMutation(CUSTOMER_LOGIN)
    // const countryData = route.params.item
    // console.log('----------userId', countryData)

    // window.FB.CustomerChat.hide()

    useEffect(async () => {
        if (route.params.item != undefined) {
            await setCode(route.params.item.dial_code)
            await setCountry_name(route.params.item.code.toLowerCase())
        }
        else {
            setCode("+254")
        }
    })




    const onLoginConfirm = async () => {
        if (!email) {
            setErrorMessage('Please enter your mobile number')
        }
       else if (isNaN(email) ) {
            setErrorMessage('Please enter only number')
        }
        else if (!country_name ) {
            setErrorMessage('Please select Country code')
        }
        else {
            userLogin({
                variables: {
                    
                    phone: email,
                    country_code: code,
                    country_shortCode: country_name
                }
            }).then((res) => {
                navigation.navigate("Otp_Screen", { id: res.data.customerLogin._id, token: res.data.customerLogin.token, phone: email })
            }).catch(err => setErrorMessage('Please enter a valid mobile number'))
        }
    }



    console.log('loginValue is', data)
    return (
        <>
            {
                loading ?
                    <View style={styles.indicator}>
                        < ActivityIndicator size="large" color={Color.skyBlue} />
                    </View>
                    :
                    <View style={styles.container}>
                        <View style={{ marginLeft: 20, marginRight: 20 }}>
                            <TouchableOpacity onPress={() => navigation.goBack()}>
                                <Image source={{ uri: back_arrow }} style={styles.backLogo} />
                            </TouchableOpacity>
                            <Image source={{ uri: loginNuzoPoints }} style={styles.logo} />

                            <Text style={styles.heading}>Login to see your Nuzo Points</Text>
                            <Text style={styles.textStyle}>Please provide your phone numbers to sign in. </Text>

                            <View style={styles.inputViewStyle}>
                                <Text style={styles.textStyle}>Phone Number</Text>

                                <View style={styles.textInputView}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Country_ModelBoxs')}
                                        style={styles.inputCodeBoxView}>
                                        <Text
                                            style={styles.inputCodeBox}>{code}</Text>
                                        <Image source={{ uri: dropdown }} style={styles.dropdown} />

                                    </TouchableOpacity>

                                    <TextInput
                                        style={styles.inputBox}
                                        value={email}
                                        onChangeText={(text) => {setEmail(text);setErrorMessage(null)}}
                                        keyboardType="phone-pad"
                                    />
                                </View>
                                <Text style={styles.errorText}>{errorMessage}</Text>
                            </View>


                            <TouchableOpacity style={styles.button}
                                onPress={onLoginConfirm}>
                                <Text style={styles.ButtonTextStyle}>Login</Text>
                            </TouchableOpacity>


                        </View>
                    </View>
            }
        </>
    )
}

