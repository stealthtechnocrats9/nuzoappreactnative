import { StyleSheet } from "react-native";
import Fonts from "../../../assets/fonts";
import Color from "../../../utils/Colors";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    backLogo: {
        width: 15.85,
        height: 16,
        marginLeft: 3.9,
        marginTop: 63
    },
    logo: {
        height: 258,
        width: 258,
        alignSelf: 'center'
    },
    heading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.black,
        marginBottom: 8
    },

    inputViewStyle: {
        marginTop: 26.5,
        marginBottom: 24
    },

    textStyle: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60
    },
    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        marginTop: 8,
        color: Color.red,
        marginLeft: 5,

    },
    textInputView: {
        height: 52,
        borderWidth: 1,
        borderColor: Color.borderColor,
        borderRadius: 16,
        marginTop: 6.5,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    inputCodeBoxView: {
        width: '25%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderRightColor: Color.borderColor,
        paddingLeft: 10,
        paddingRight: 10,
    },

    inputCodeBox: {
        width: '59%',
       fontSize: 12,
       color: Color.skyBlue,
       textAlign: 'center',
       alignContent: 'flex-end'
    },
    dropdown: {
        height: 7.41,
        width: 12,
        marginLeft: 10
    },
    inputBox: {
        paddingLeft: 15,
        paddingRight: 15,
        width: '78%',
        fontSize: 12,
        color: Color.skyBlue
    },
    button: {
        height: 52,
        borderRadius: 16,
        backgroundColor: Color.skyBlue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    },


})