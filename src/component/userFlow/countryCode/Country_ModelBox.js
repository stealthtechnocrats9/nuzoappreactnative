
import React, { useState, useEffect } from 'react'
import { Text, View, Image, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import flag from '../../../assets/flag.png'
import back_arrow from '../../../assets/back_arrow.png'
import check from '../../../assets/correct_tick.png'
import { countryCode } from './country'
import Fonts from '../../../assets/fonts'
import Color from '../../../utils/Colors'

export default function Country_ModelBoxs({ navigation, route }) {
    const [selectItem, setSelectItem] = useState({ name: 'Kenya', flag: '🇰🇪', code: 'KE', dial_code: '+254' })
    const [selectIndex, setSelectIndex] = useState(113)
    const [show, setShow] = useState(true)



    useEffect(() => {
        const timer = setTimeout(() => {
            setShow(false)
        }, 1000);
        return () => clearTimeout(timer);
    }, []);

    console.log('route-----------', route.params)
    const selectCountry = (item, index) => {
        setSelectIndex(index)
        setSelectItem(item)
        if (route.params == undefined) {
            navigation.navigate("LoginScreen", { item: item })
        }
        else {
            navigation.navigate("productCheckOut", { item: item })
        }
    }
    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                onPress={() => selectCountry(item, index)}
                style={styles.countryDesign}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontSize: 25, marginRight: 13, fontFamily: Fonts.Samsung }}>{item.flag}</Text>
                    <Text style={styles.countryNameText}>{item.name}</Text>
                    <Text style={styles.countryNameText}>({item.dial_code})</Text>

                </View>
                {selectIndex == index &&
                    <Image source={{ uri: check }} style={{ height: 10.22, width: 14.05 }} />
                }
            </TouchableOpacity>
        )
    }
    return (
        <View>
            {show ?
                <View style={styles.indicator}>
                    < ActivityIndicator size="large" color={Color.skyBlue} />
                </View>
                :
                <>
                    <Header

                        leftIcon={{ uri: back_arrow }}
                        leftNextText="Select a country/region"
                        onLeftPress={() => navigation.goBack()}
                        leftIconStyle={styles.menuLeftIcon}
                    />
                    <FlatList
                        data={countryCode}
                        showsVerticalScrollIndicator={false}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </>
            }
        </View>
    )
}

