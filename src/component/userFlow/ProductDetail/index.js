import React, { useEffect, useState, useRef, } from 'react'
import { Text, View, Image, Alert, FlatList, useWindowDimensions, Animated, TouchableOpacity, ScrollView, ActivityIndicator, Linking } from 'react-native'
import styles from './styles'
import { useQuery } from '@apollo/client'
import { GET_ALLPRODUCT_BY_TOKEN } from '../../../graphql/customerQueries/getProductDetailByToken'
import Header from '../../../utils/Header/Header'
import SlideScreen from './SlideScreen'
import Slider from './Slider'
import { Helmet } from "react-helmet";
import Color from '../../../utils/Colors'




const User_Product_Detail = ({ navigation }) => {
    const { width } = useWindowDimensions();
   // const [userToken, setUserToken] = useState('')
    var userToken="";
    const [url, setURL] = useState('')
    const [percentage, setPercentge] = useState(null)
    const [product_img, setProductImage] = useState(null)
    const [showLearnMore, setShowLearnMore] = useState(false)

    const queryParams = window.location.href;
    const lastSegment = queryParams.split("/").pop();
    userToken=lastSegment;

    // Linking.getInitialURL().then((url) => {
    //     let data = url
    //     setURL(url)
    //     const lastSegment = data.split("/").pop();
    //     setUserToken(lastSegment)

    // });



    const { data, error: getProducterrorError, loading: getProductLoading } = useQuery(GET_ALLPRODUCT_BY_TOKEN, {
        variables: {
            token: userToken
        }
    });

    const [allProduct, setAllProduct] = useState([])

    const [currentIndex, setCurrentIndex] = useState(0);
    const scrollX = useRef(new Animated.Value(0)).current;
    const slidesRef = React.useRef(null);


    // window.FB.CustomerChat.show()
    useEffect(() => {
        const unsubscribe = navigation.addListener('didfocus', () => {
            // The screen is focused
            window.location.reload(true);
        });

        return unsubscribe;
    }, [navigation]);


    useEffect(async () => {

        await setAllProduct(data.getProductDetailByToken[0])
        let percentVal = allProduct.offer_price
        let percentToget = 1
        let res = (percentToget / 100) * percentVal

        setPercentge(Math.floor(res))
        await setProductImage(allProduct.product_image[0].product_image)


    })


    console.log('--------------product_img-', data, 'error', getProducterrorError)

    const startDate = new Date().getTime();
    const d = allProduct.expire_date;
    const endDate = new Date(d).getTime();
    const timeDiff = (endDate+(1000 * 60 * 60 * 24)) - startDate
    var totalDay = Math.floor(timeDiff / (1000 * 60 * 60 * 24))
    var totalHours = Math.floor((timeDiff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
    // const totalMinute = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60))




    const slidesData = allProduct.product_image

    const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 150 }).current;

    const scrollTo = () => {
        if (currentIndex < slidesData.length - 1) {
            slidesRef.current.scrollToIndex({ index: currentIndex + 1 })
        }

    }


    const onLearnMore = () => {

        setShowLearnMore(true)
    }


    const onConfirmProduct = () => {

        navigation.navigate("productCheckOut", { allProduct })
    }
    //  console.log("allProduct product  data ---", allProduct)
    //  let url ="https://app.nuzo.co/"+userToken;
    // let description = allProduct.product_description
    // let imgurl ="https://api.nuzo.co/product_images/"+allProduct.product_image[0]

    return (
        <View style={styles.container}>
            <Helmet>
                <meta charSet="utf-8" />
                <title>{allProduct.product_name}</title>
                <meta property="og:description" content={allProduct.product_description}></meta>
                <meta property="og:url" content={"https://app.nuzo.co/" + userToken} />
                <meta property="og:image" content={"https://nuzo.co/product_images/" + product_img} />
            </Helmet>
            {getProductLoading ?
                <View style={styles.indicator}>
                    < ActivityIndicator size="large" color={Color.skyBlue} />
                </View>
                :

                <ScrollView showsVerticalScrollIndicator={false} style={showLearnMore ? { opacity: 0.6 } : null}>
                    <Header
                        leftText="Product details" />

                    {getProducterrorError ?

                        <Text style={{ alignSelf: 'center', marginTop: 100}}>Token not exist</Text>

                        :

                        <View style={styles.main}>
                            <View>
                                <View style={styles.sliderStyle}>
                                    <FlatList
                                        data={slidesData}
                                        renderItem={({ item }) => <SlideScreen item={item} currentIndex={currentIndex} />}
                                        horizontal
                                        showsHorizontalScrollIndicator={false}
                                        pagingEnabled
                                        bounces={false}
                                        keyExtractor={(item, index) => index.toString()}
                                        onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
                                            useNativeDriver: false
                                        })}
                                        scrollEventThrottle={35}
                                        viewabilityConfig={viewConfig}
                                        ref={slidesRef} />

                                    {slidesData != undefined ?
                                        <Slider data={slidesData} scrollX={scrollX} scrollTo={scrollTo} currentIndex={currentIndex} />
                                        : null}


                                </View>
                                <Text style={styles.textHeading}>{allProduct.product_name}</Text>

                                
                                    <View style={styles.productDetailView}>
                                        <View style={styles.detailView}>
                                            <Text style={styles.productDetailItem}>{'Price'}</Text>
                                            <Text style={styles.price}>{allProduct.currency} {allProduct.product_price}</Text>

                                        </View>
                                        <View style={styles.detailView}>
                                            <Text style={styles.productDetailItem}>{'Promo'}</Text>
                                            <Text style={styles.priceGreen}>{allProduct.currency} {allProduct.offer_price}</Text>

                                        </View>
                                        <View style={styles.detailView}>
                                            <Text style={styles.productDetailSaveItem}>{'You save'}</Text>
                                            <Text style={styles.savePrice}>{allProduct.currency} {allProduct.discount}</Text>
                                        </View>
                                        <View style={styles.detailView}>
                                            <Text style={styles.productDetailItem}>{'Ends in'}</Text>
                                            <Text style={styles.checkTime}>
                                                {totalDay >= 0 ? totalDay + " " + "days" : "0 days"}
                                                {totalHours >= 0 ? " " + totalHours + " " + "hours" : " 0 hours"}
                                                {/* {totalMinute ? " " + totalMinute + " " + "minutes" : null} */}
                                            </Text>
                                        </View>
                                    </View>

                              

                                <TouchableOpacity onPress={onConfirmProduct} style={styles.button}>
                                    <Text style={styles.buttonText}>Buy this item (Earn {percentage} Nuzo Points)</Text>
                                </TouchableOpacity>

                                <View style={styles.learnMoreButton}>
                                    <Text style={styles.learnMoreText}>This order qualifies for {percentage} Nuzo Points.
                                        <TouchableOpacity onPress={onLearnMore}>
                                            <Text style={styles.clickableText}> Learn more.</Text>
                                        </TouchableOpacity>
                                    </Text>
                                </View>
                                {showLearnMore &&
                                    <View style={styles.alertBox}>
                                        <Text style={styles.alertText}>You earn Nuzo Points for shopping. Nuzo Points can be exchanged for cUSD which earns 50% interest from our partner Valora. Visit <Text
                                            style={styles.clickableText}
                                            onPress={() => {
                                                Linking.openURL('https://www.valoraapp.com');
                                            }}>
                                            www.valoraapp.com
                                        </Text>  for more details</Text>
                                        <View style={styles.alertButtonsView}>
                                            <TouchableOpacity onPress={() => setShowLearnMore(false)} style={styles.buttonAlert} >
                                                <Text style={styles.alertButtonText}>Ok</Text>
                                            </TouchableOpacity>

                                        </View>
                                    </View>
                                }



                                <View style={styles.discription}>
                                    <Text style={styles.productDetailItemText}>Description</Text>
                                    <Text style={styles.discriptionText}>{allProduct.product_description}</Text>

                                </View>
                            </View>

                        </View>
                    }
                </ScrollView>
            }
        </View >
    )
}

export default User_Product_Detail
