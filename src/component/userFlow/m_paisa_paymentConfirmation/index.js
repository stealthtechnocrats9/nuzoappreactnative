import React, { useState, useEffect, useRef } from 'react'
import { Text, View, Image, TextInput, Animated, TouchableOpacity, ActivityIndicator } from 'react-native'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import backIcon from '../../../assets/back_arrow.png'
import swipeArrow from '../../../assets/swipeArrow.png'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import { CREATE_PAYMENT } from '../../../graphql/customerMutation/createPayment'
import { useMutation } from '@apollo/client'
import Color from '../../../utils/Colors'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function mPaisaPaymentConfirmation({ navigation, route }) {

    const merchantDetail = route.params.itemDetail
    const Checkout_id = route.params.id
    const customerId = route.params.customer_id
    const [pins, setPins] = useState(null)
    const [pinLength, setPinLength] = useState(10)
    let textInput = useRef(null)
    const [percentge, setPercentge] = useState('')
    const nuzopoints = merchantDetail.offer_price

    const [createPayment, { data, loading, error }] = useMutation(CREATE_PAYMENT)

    useEffect(() => {
        setPins(merchantDetail.payment_option)
        if (pins != null) {
            setPinLength(pins.length)
        }

        let percentVal = merchantDetail.offer_price
        let percentToget = 1
        let res = (percentToget / 100) * percentVal

        setPercentge(Math.floor(res))
    });

    // console.log('percentge------', percentge)
    // console.log('-----customerId--', customerId)

    // console.log('=Checkout_id===', Checkout_id, merchantDetail)
    const onPaymentConfirm = () => {
        createPayment({
            variables: {
                order_id: Checkout_id,
                customer_id: customerId,
                amount: merchantDetail.currency,
                type: "m-pesa"
            },
        }).then((res) => {
            navigation.navigate("Payment_Processing", { percentge ,merchantDetail})

        }).catch((err) => {
            console.log('error', err)

        })
    }

    // useEffect(()=>{
    //   if(data != undefined){
    //   }
    // })

    console.log('data-------', data, 'error', error)

    const LeftSwipeActions = () => {
        return (
            <TouchableOpacity
                style={styles.swipeConfirmButton} >
                <View style={{ width: 30 }} />
                <Text style={styles.swipeConfirmText}>Confirming payment</Text>
                <Image source={swipeArrow} style={styles.swipeConirmArrow} />
            </TouchableOpacity>
        );
    };


    const swipeFromLeftOpen = () => {
        // alert('Swipe from left');
    };

    return (
        <>
            {
                loading ?
                    <View style={styles.indicator}>
                        < ActivityIndicator size="large" color={Color.skyBlue} />
                    </View>
                    :
                    <View style={styles.container}>
                        <Header
                            leftIcon={{ uri: backIcon }}
                            onLeftPress={() => navigation.goBack()}
                            leftIconStyle={styles.menuLeftIcon}
                            leftNextText="Checkout"
                        />
                        <View style={styles.main}>
                            <Text style={styles.paymentTitle}>Pay with M-Pesa</Text>

                            <Text style={styles.heading}>Please send the following amount to the merchant’s details below;</Text>

                            <View style={styles.accountDetailBox}>

                                <Text style={styles.amountTopHeading}>Amount to send</Text>
                                <Text style={styles.amountDetail}>{merchantDetail.currency} {merchantDetail.offer_price}</Text>
                                <Text style={styles.amountTopTitle}>PAY WITH</Text>
                                <Text style={styles.cardName}>M-PESA</Text>
                                {/* <Text style={styles.amountTitle}>Pay *{merchantDetail.offer_price}#</Text> */}
                                <Text style={styles.amountHeading}>Merchant Name</Text>
                                <View style={styles.amountDetailInsideBox}>
                                    <Text style={styles.amountDetail}>{merchantDetail.marchant_name}</Text>
                                </View>
                                <Text style={styles.amountHeading}>Merchant Phone Number</Text>

                                

                                <View style={styles.inputStyles}>
                                    {
                                        Array(pinLength).fill().map((data, index) => (
                                            <View
                                                key={index}
                                                style={styles.input}>
                                                <Text style={styles.optText}
                                                    onPress={() => textInput.focus()}
                                                >
                                                    {pins && pins.length > 0 ? pins[index] : ""}
                                                </Text>
                                            </View>
                                        ))
                                    }

                                </View>

                            </View>

                            <Text style={styles.amountDetail}>Pickup instructions</Text>

                            <Text style={styles.heading}>To collect this order please contact {merchantDetail.marchant_name} on {merchantDetail.payment_option} or visit {merchantDetail.store_detail.shop_address}</Text>


                            <Swipeable
                                renderLeftActions={LeftSwipeActions}
                                onSwipeableLeftOpen={swipeFromLeftOpen}
                                onSwipeableLeftOpen={onPaymentConfirm}
                            >
                                <View style={styles.swipeButton} >

                                    <View style={styles.swipeView}>
                                        <Image source={swipeArrow} style={styles.swipeArrow} />
                                    </View>
                                    <Text style={styles.swipeText}>Swipe to confirm payment</Text>
                                    <View style={{ height: 5, width: 20 }} />
                                </View>
                            </Swipeable>






                        </View>
                    </View>
            }
        </>
    )
}

