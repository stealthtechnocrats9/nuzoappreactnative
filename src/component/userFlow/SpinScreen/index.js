import React, { useState } from 'react'
import { Text, View, TextInput, Image, TouchableOpacity, FlatList } from 'react-native'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import backIcon from '../../../assets/back_arrow.png'
import dropdown from '../../../assets/dropdown.png'
import tick from '../../../assets/correct_tick.png'
import spinner from '../../../assets/spinner.png'


export default function Spin_Screen({ navigation }) {
    const [value, setValue] = useState("1X Spin - 50 Nuzo Points")
    const [isShow, setIsShow] = useState(false)

    const [items, setItems] = useState([
        "1X Spin - 50 Nuzo Points",
        "2X Spin - 90 Nuzo Points",
        "4X Spin - 175 Nuzo Points"
    ])
    const [selectItem, setSelectItem] = useState(0)

    const togglePress = () => {
        setIsShow(!isShow)
    }

    const onSelectDta = (item, index) => {
        setSelectItem(index)
        setValue(item)
    }

    const onUnSelectDta = (item, index) => {
        console.log('item is', item, index)
        setSelectItem(index)
        setValue(item)
    }

    const onConfirmProduct = () => {
        navigation.navigate("mauzoPointsScreen")
    }

    return (
        <View style={styles.container}>
            <Header
                leftIcon={{ uri: backIcon }}
                onLeftPress={() => navigation.goBack()}
                leftIconStyle={styles.menuLeftIcon}
                leftNextText="Checkout"
            />
            <View style={styles.main}>
                <Text style={styles.heading}> Spin the wheel to win amazing gift including coupon, Nuzo points and airtime</Text>
                <Text style={styles.resultText}> You have <Text style={styles.result}>750</Text> points</Text>

                <View>
                    <View style={styles.inputView}>
                        <View style={styles.input}>
                            <Text style={styles.inputText}>{value}</Text>
                        </View>

                        <TouchableOpacity style={styles.dropdown} onPress={togglePress} >
                            <Image source={{ uri: dropdown }} style={styles.dropdownLogo} />
                        </TouchableOpacity>

                    </View>

                    {isShow &&
                        <View style={styles.hiddenView}>
                            <FlatList
                                data={items}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item, index }) => (
                                    <>
                                        {selectItem != index ?
                                            <TouchableOpacity style={styles.hiddenBoxView}
                                                onPress={() => onSelectDta(item, index)}>
                                                <Text>{item}</Text>
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity style={styles.hiddenBoxView} onPress={() => onUnSelectDta(item, index)}>

                                                <Text>{item}</Text>
                                                <Image source={{ uri: tick }} style={styles.checkTickIcon} />
                                            </TouchableOpacity>
                                        }
                                        {console.log("length", items.length)}
                                        {items.length - 1 !== index && <View style={styles.hiddenBorder} />
                                        }

                                    </>
                                )}
                            />
                        </View>
                    }
                </View>


                <Image source={{ uri: spinner }} style={isShow == false ? { height: 320, width: 320, alignSelf: 'center', }
                    : { height: 320, width: 320, alignSelf: 'center', opacity: 0.5, zIndex: -1, }} /> 

                <TouchableOpacity onPress={onConfirmProduct} style={styles.button}>
                    <Text style={styles.buttonText}>Spin wheel</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}




