import { StyleSheet } from 'react-native'
import Fonts from '../../../assets/fonts'
import Color from '../../../utils/Colors'


export default StyleSheet.create({
    container: {
        backgroundColor: Color.red,
        flex: 1
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85
    },
    main: {
        marginLeft: 20,
        marginRight: 20
    },
    heading: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginTop: 24,
        alignItems: 'flex-start'
    },
    resultText: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 22,
        marginBottom: 9
    },
    result: {
        fontSize: 12,

        fontFamily: Fonts.Samsung,
    },
    inputView: {
        height: 48,
        backgroundColor: Color.skyBlue_5,
        borderRadius: 10,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderTopColor: Color.borderColor,
        borderBottomColor: Color.borderColor,
        borderLeftColor: Color.borderColor,
        borderRightColor: Color.borderColor,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 5,

    },
    input: {
        height: 40,
        width: '84%',
        paddingLeft: 10,
        paddingRight: 15,
        justifyContent: 'center'

    },
    inputText: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
    },

    dropdownLogo: {
        height: 7.41,
        width: 12,
    },
    dropdown: {
        width: '15%',
        alignItems: 'center',
        height: 45,
        justifyContent: 'center'
    },
    hiddenView: {
        borderWidth: 2,
        borderRadius: 10,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 58,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderTopColor: Color.borderColor,
        borderBottomColor: Color.borderColor,
        borderLeftColor: Color.borderColor,
        borderRightColor: Color.borderColor,
        zIndex: -1,
        backgroundColor: Color.white,
    },
    hiddenBoxView: {
        height: 46,
        backgroundColor: Color.white,
        borderRadius: 10,
        borderBottomColor: Color.borderColor,
        justifyContent: 'space-between',
        marginLeft: 8,
        marginRight: 20,
        alignItems: 'center',
        flexDirection: 'row'
    },
    hiddenBorder: {
        height: 0.8,
        backgroundColor: Color.borderColor,
        marginLeft: 10,
        marginRight: 10
    },
    checkTickIcon: {
        height: 8.52,
        width: 11.71
    },
    button: {
        height: 52,
        width: '100%',
        backgroundColor: Color.skyBlue,
        marginBottom: 40,
        marginTop: 36.68,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    },
})