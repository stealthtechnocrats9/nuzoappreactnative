import { StyleSheet } from "react-native";
import Fonts from "../../../assets/fonts";
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.webbackgroud
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    filterIcon: {
        height: 20,
        width: 20,
    },
    main: {
        marginLeft: 20,
        marginRight: 20
    },
    totalPoints: {
        height: 76,
        width: '100%',
        borderRadius: 15,
        backgroundColor: 'red',
        marginTop: 23
    },
    linearGradient: {
        height: 76,
        borderRadius: 15,
        marginTop: 23,
        justifyContent: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 30
    },
    linearGradientInsideView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    goldIcon: {
        height: 44,
        width: 44,
        marginRight: 8
    },

    linearGradienttitle: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.whiteText
    },
    linearGradienttitleResultView: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 2
    },
    linearGradiebtResult: {
        fontSize: 28,
        fontFamily: Fonts.Samsung,
        fontWeight: '700',
        color: Color.whiteText,
        alignItems: 'center'
    },
    linearGradiebtTitleResult: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: '700',
        color: Color.whiteText,
        marginLeft: 8,
        textAlign: 'center'

    },
    nextLogo: {
        height: 13.33,
        width: 13.33,
        marginLeft: 8
    },

    nextLogoStyle: {
        height: 13.33,
        width: 13.33,
        marginLeft: 8,
        tintColor: Color.skyBlue
    },

    label: {
        fontSize: 23,
        fontFamily: Fonts.Samsung,
        color: "black",
        position: "absolute",
        zIndex: 1,
        alignSelf: "center",
    },
    boxtitle: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        fontWeight: '600',
        marginBottom: 24,
        color: Color.black

    },
    boxes: {
        height: 108,
        width: 152,
        borderRadius: 15,
        backgroundColor: Color.white,
        shadowColor: "#000000",
        shadowOpacity: 0.05,
        shadowRadius: 12,
        shadowOffset: {
            height: 0,
            width: 0
        },
        elevation: 1,
        justifyContent: 'center',
        paddingLeft: 12,
        paddingRight: 5

    },
    nuzoPointsPayment: {
        marginVertical: 15,
        backgroundColor: Color.white,
        shadowColor: "#000000",
        paddingBottom: 15,
        paddingTop: 15,
        borderRadius: 13,
        shadowOpacity: 0.2,
        shadowRadius: 12,
        shadowOffset: {
            height: 0,
            width: 0
        },
        elevation: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },

    voloraImage: {
        width: '18%',
        alignItems: 'center'
    },
    icon: {
        height: 30,
        width: 30,
        alignSelf: 'center',
      
    },
    voloraText: {
        width: '82%',
        paddingRight: 12

    },

    voloraPayment: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7
    },
    voloraPoints: {
        marginTop: 3,
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.black_60
    },
    boxName: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: '500',
        color: Color.black,
        marginBottom: 2
    },
    boxTitleView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'

    },
    boxTitle: {
        fontSize: 7,
        fontWeight: '500',
        fontFamily: Fonts.Samsung, 
        color: Color.black_60

    },
    loginHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        marginBottom: 24
    },
    button: {
        height: 52,
        borderRadius: 16,
        // borderColor: Color.borderColor,
        // borderWidth: 1,
        backgroundColor: Color.skyBlue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    },
    buttonAlert: {
        height: 40,
        width: '45%',
        backgroundColor: Color.skyBlue,
        marginTop: 10,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    alertButtonsView: {
        flexDirection: 'row',
        marginLeft:10,
        marginRight:10,
        justifyContent: 'space-around'
    },
   
    alertBox: {
        height: 160,
        width: "90%",
        backgroundColor: Color.white,
        alignSelf: 'center',
        position: 'absolute',
        justifyContent: 'center',
        borderRadius: 16
        
    },

    alertText: {
        fontSize: 14,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        margin:15,

        fontStyle: 'normal',
        textAlign: 'center'
    },

    alertButtonText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    },
   
    // ActiveButtonTextStyle: {
    //     fontSize: 12,
    //fontFamily: Fonts.Samsung,
    //     fontWeight: "700",
    //     color: Color.white
    // }

    detailBox: {
        padding: 10,
        borderWidth: 0,
        borderColor: Color.black,
        marginBottom: 22,
        backgroundColor: Color.white,
        shadowColor: Color.shadowColor,
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 15,
        elevation: 1,
    },
    DetailBoxRowStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    timeDetail: {
        color: Color.black_60,
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        
    },
    readyStatusText: {
        color: Color.green,
        fontSize: 13,
        fontWeight: '600',
        fontFamily: Fonts.Samsung, 
        backgroundColor: Color.green_50,
        paddingTop: 4,
        paddingLeft: 8,
        paddingRight: 8,
        paddingBottom: 4,
        borderRadius: 8
    },

    deliveredStatusText: {
        color: Color.black_60,
        fontSize: 13,
        fontWeight: '600',
        fontFamily: Fonts.Samsung,
         backgroundColor: Color.black_5,
        paddingTop: 4,
        paddingLeft: 8,
        paddingRight: 8,
        paddingBottom: 4,
        borderRadius: 8
    },
    processingStatusText: {
        color: Color.yellow_30,
        fontSize: 13,
        fontWeight: '600',
        fontFamily: Fonts.Samsung,
         backgroundColor: Color.yellow_50,
        paddingTop: 4,
        paddingLeft: 8,
        paddingRight: 8,
        paddingBottom: 4,
        borderRadius: 8
    },

    canceledStatusText: {
        color: Color.red,
        fontSize: 13,
        fontWeight: '600',
        fontFamily: Fonts.Samsung,
         backgroundColor: Color.yellow_50,
        paddingTop: 4,
        paddingLeft: 8,
        paddingRight: 8,
        paddingBottom: 4,
        borderRadius: 8
    },

    activeStatusText: {
        color: Color.green,
        fontSize: 13,
        fontWeight: '600',
        fontFamily: Fonts.Samsung,
         backgroundColor: Color.yellow_50,
        paddingTop: 4,
        paddingLeft: 8,
        paddingRight: 8,
        paddingBottom: 4,
        borderRadius: 8
    },

    detailBoxTitle: {
        color: Color.blue_7,
        fontSize: 13,
        fontFamily: Fonts.Samsung,
        fontWeight: '600',
        marginTop: 8,
        marginBottom: 8,

        fontStyle: 'normal',

    },
    storePrice: {
        color: Color.blue_8,
        fontSize: 13,
        fontWeight: '600',
        fontFamily: Fonts.Samsung,
        fontStyle: 'normal',
/* or 10px */

textAlign: 'right'
    }



})