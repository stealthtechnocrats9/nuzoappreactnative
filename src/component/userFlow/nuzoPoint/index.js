import React, { useState, useEffect } from 'react'
import { Text, View, Image, FlatList, Pressable, TouchableOpacity, ScrollView, Linking, ActivityIndicator } from 'react-native'
import styles from './styles'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useQuery } from '@apollo/client'
import { GET_ORDERLIST_BY_CUSTOMER_ID } from '../../../graphql/customerQueries/getOrderListByCutomerId'
import { GET_REWARDS_POINTS } from '../../../graphql/customerQueries/getRewardedPoints.js'
import Header from '../../../utils/Header/Header'
import filterIcon from '../../../assets/logout.png'
import LinearGradient from 'react-native-web-linear-gradient';
import Color from '../../../utils/Colors'
import gold from '../../../assets/gold.png'
import valora from '../../../assets/payWith_Valora.png'
import nextLogo from '../../../assets/arrow_next.png'
import { useIsFocused } from "@react-navigation/native";

import moment from 'moment'

export default function Nuzo_Points({ navigation }) {

    const [token, setToken] = useState(null)
    const isFocused = useIsFocused();

    const [customer_id, setCustomer_id] = useState('')
    const [nuzoReward, setNuzoReward] = useState('')
    const [allData, setAllData] = useState([])
    const [withdrawVilora, setWithdrawVilora] = useState(false)


    const { data: orderListData, error, loading, refetch } = useQuery(GET_ORDERLIST_BY_CUSTOMER_ID, {
        variables: {
            customer_id: customer_id
        }
    });


    const { data: nuzoPoint_data, error: getpointError, loading: getPointLoading } = useQuery(GET_REWARDS_POINTS, {
        variables: {
            customer_id: customer_id
        }
    });
   
    //window.FB.CustomerChat.hide()

    useEffect(() => {
        if (nuzoPoint_data) {
            setNuzoReward(nuzoPoint_data.getRewardPoints)
        }
        if (orderListData) {
            setAllData(orderListData.getOrdersListByCustomerId)

        }
    })


    useEffect(async () => {
        let tokenValue = await AsyncStorage.getItem('usertoken')
        let customerData = await AsyncStorage.getItem('customer_id')
        setToken(tokenValue)
        setCustomer_id(customerData)
        console.log('------1')
    })

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            refetch()
            console.log('------2')

        });
        return unsubscribe;
    }, [navigation])

    const onLoginConfirm = () => {
        navigation.navigate("LoginScreen")
    }


    const signOut = async () => {
        await AsyncStorage.clear()
        alert('Logout success')
        refetch()
    }


    const onLearnMore = () => {

        setWithdrawVilora(true)
    }



    const renderDetailItem = ({ item, index }) => {
       console.log('i---t--e---m -------------------', item, index)
        return (
            <View style={styles.detailBox}>
                <View style={styles.DetailBoxRowStyle}>
                    <Text style={styles.timeDetail}>
                        {moment.unix(item.created_at / 1000).format("DD MMM YYYY hh:mm a")}
                    </Text>
                    <Text style={item.order_status == "3" ? styles.readyStatusText :
                        item.order_status == "4" ? styles.deliveredStatusText :
                        item.order_status == "5" ? styles.canceledStatusText :
                        item.order_status == "1" ? styles.activeStatusText :
                        styles.processingStatusText}>
                        {item.order_status == "3" ? "Ready" :
                            item.order_status == "4" ? "Delivered" :
                            item.order_status == "5" ? "Cancelled" :
                            item.order_status == "1" ? "Active" : "Processing"}</Text>
                </View>
                <View style={styles.DetailBoxRowStyle}>
                <Text style={styles.detailBoxTitle}>{item.product_detail.product_name}</Text>
                <Text style={styles.detailBoxTitle}>{item.store_detail.store_name}</Text>
                </View>

                <View style={styles.DetailBoxRowStyle}>
                    <Text style={styles.storePrice}>{item.product_detail.currency} {item.total}</Text>
                    <Text style={styles.storePrice}>{item.store_detail.payment_option}</Text>
                </View>

            </View>
        )
    }


    return (
        <View style={styles.container}>
            {loading || getPointLoading ?
                <View style={styles.indicator}>
                    < ActivityIndicator size="large" color={Color.skyBlue} />
                </View>
                :
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Header
                        leftText="Nuzo Points"
                        rightNextSecIcon={{ uri: filterIcon }}
                        rightNextIconStyle={styles.filterIcon}
                        onRightNextPress={signOut}
                    />



                    <View style={styles.main}>
                        {token ?
                            <LinearGradient
                                start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                                colors={[Color.linearGradientblue, Color.linearGradientlightblue]}
                                style={styles.linearGradient}>
                                <View style={styles.linearGradientInsideView}>
                                    <Image source={{ uri: gold }} style={styles.goldIcon} />
                                    <View>
                                        <Text style={styles.linearGradienttitle}>You have</Text>
                                        <View style={styles.linearGradienttitleResultView}>
                                            <Text style={styles.linearGradiebtResult}>
                                                {nuzoReward == null ? 0 : (Math.round(parseFloat(nuzoReward.rewards * 100)) / 100).toFixed(2)
                                                }
                                            </Text>
                                            <Text style={styles.linearGradiebtTitleResult}>Nuzo Points</Text>
                                        </View>
                                    </View>

                                </View>
                            </LinearGradient>
                            :
                            <LinearGradient
                                start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                                colors={[Color.linearGradientblue, Color.linearGradientlightblue]}
                                style={styles.linearGradient}>
                                <View style={styles.linearGradientInsideView}>
                                    <Image source={{ uri: gold }} style={styles.goldIcon} />
                                    <Text style={styles.linearGradienttitle}>Login/Sign up to get Nuzo points</Text>
                                    <Image source={nextLogo} style={styles.nextLogo} />

                                </View>
                            </LinearGradient>}


                        <Text style={styles.boxtitle}>Redeem your Nuzo Points</Text>
                        <TouchableOpacity onPress={onLearnMore}>

                            <View
                                style={styles.nuzoPointsPayment}>
                                <View style={styles.voloraImage}>
                                    <Image source={{ uri: valora }} style={styles.icon} />
                                </View>
                                <View style={styles.voloraText}>
                                    <Text style={styles.voloraPayment}>Withdraw to Valora</Text>
                                    <Text style={styles.voloraPoints}>Convert your points to cUSD and earn 50% APY interest</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        {withdrawVilora &&
                            <View style={styles.alertBox}>
                                <Text style={styles.alertText}>To withdraw Nuzo Points to cUSD you must have the Valora wallet app installed</Text>

                                <View style={styles.alertButtonsView}>
                                    <TouchableOpacity onPress={() => setWithdrawVilora(false)} style={styles.buttonAlert} >
                                        <Text style={styles.alertButtonText}>Cancel</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => { setWithdrawVilora(false); Linking.openURL('https://play.google.com/store/apps/details?id=co.clabs.valora&hl=en_IN&gl=US'); }} style={styles.buttonAlert} >
                                        <Text style={styles.alertButtonText}>Download Now</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        }


                        <View style={styles.boxTitleView}>
                            <Text style={styles.boxtitle}>Recent Orders</Text>
                            {/* <View style={{flexDirection: 'row'}}>
                            <Text style={styles.boxtitle}>See all</Text>
                            <Image source={nextLogo} style={styles.nextLogoStyle} />
                        </View> */}
                        </View>

                        {token ?
                            <View>
                                <FlatList
                                    data={allData}
                                    renderItem={renderDetailItem}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            :
                            <>
                                <Text style={styles.loginHeading}>Login or Sign up to see your order history</Text>


                                <TouchableOpacity style={styles.button}
                                    onPress={onLoginConfirm}>
                                    <Text style={styles.ButtonTextStyle}>Login</Text>
                                </TouchableOpacity>

                                {/* <TouchableOpacity onPress={() => navigation.navigate("ResetPassword")}>
                                    <Text style={{ fontSize: 15, color: 'red' , marginTop: 15}}
                                        onPress={() => navigation.navigate("ResetPassword")}>Reset Password</Text>
                                </TouchableOpacity> */}
                            </>
                        }


                    </View>
                </ScrollView>
            }
        </View>
    )
}

