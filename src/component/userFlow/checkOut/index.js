import React, { useState, useEffect } from 'react'
import { Text, View, Image, TextInput, TouchableOpacity, Platform, ActivityIndicator } from 'react-native'
import Header from '../../../utils/Header/Header'
import styles from './styles'
import backIcon from '../../../assets/back_arrow.png'
import percentage from '../../../assets/Percentage.png'
import locationIcon from '../../../assets/locationIcon.png'
import { useMutation } from '@apollo/client'
import { CREATE_ORDER, CREATE_NPORDER } from '../../../graphql/customerMutation/createOrder'
import DeviceInfo from 'react-native-device-info';
import Modal from 'modal-react-native-web';
import Color from '../../../utils/Colors'

import payWith_cash from '../../../assets/pay_cash.png'
import payWith_mpesa from '../../../assets/payWith_mpesa.png'
import payWith_Valora from '../../../assets/payWith_Valora.png'
import dropdown from '../../../assets/dropdown.png'
import AsyncStorage from '@react-native-async-storage/async-storage'


export default function Product_CheckOut({ navigation, route }) {

    // const res= window.localStorage.getItem("allProduct")
    //;.then((res)=>{
    //const itemDetail=JSON.parse(res);
    //    console.log('----------- aa ---', itemDetail)

    //  });
    var offerProduct = true;
    const itemDetail = route.params.allProduct
    const offer_id = itemDetail._id
    const currency = itemDetail.currency
    const product_price = itemDetail.product_price
    const pickup_address = itemDetail.store_detail.shop_address
    const marchant_name = itemDetail.marchant_name
    var discount = itemDetail.discount
    const product_id = itemDetail.product_id == undefined ? itemDetail.product_image[0].product_id : itemDetail.product_id
    const product_name = itemDetail.product_name
    var offer_price = itemDetail.offer_price
    const [name, setName] = useState("")
    const [phone, setPhone] = useState("")
    const [code, setCode] = useState('+254')
    const [countryName, setCountryName] = useState('ke')
    const [errorName, setErrorName] = useState(null)
    const [phoneError, setPhoneError] = useState(null)
    const [normalOrderData, setNormalData] = useState(null)
    const [isModalVisible, setModalVisible] = useState(false);
    const [createOrder, { data, loading, error }] = useMutation(CREATE_ORDER)
    const [createNormalOrder, { data: datanp, loading: normalOrderLoading, error: normalOrderError }] = useMutation(CREATE_NPORDER)



    // const [itemDetail,setItemDetail] = useState();
    // const [offer_id,setoffer_id] =  useState()
    // const [currency,setcurrency] =  useState()
    // const [product_price,setproduct_price] =  useState()
    // const [pickup_address,setpickup_address] =    useState()
    // const [marchant_name,setmarchant_name] =  useState()
    // var [discount,setdiscount] =  useState()
    // const [product_id,setproduct_id] =  useState()
    // const [product_name, setproduct_name]=  useState()
    // var [offer_price,setoffer_price] =  useState()
    // useEffect(()=>{
    //     AsyncStorage.getItem("allProduct").then((res)=>{
    //         const item=JSON.parse(res);
    //         setItemDetail(item)
    //         setoffer_id(itemDetail._id);
    //         setcurrency(itemDetail.currency)
    //         setproduct_price (itemDetail.product_price)
    //         setpickup_address (itemDetail.store_detail.shop_address)
    //         setmarchant_name (itemDetail.marchant_name)
    //         setdiscount (itemDetail.discount)
    //         setproduct_id (itemDetail.product_id == undefined ? itemDetail.product_image[0].product_id : itemDetail.product_id)
    //         setproduct_name (itemDetail.product_name)
    //         setoffer_price (itemDetail.offer_price)

    //       // console.log('----------- aa ---', item)

    //    });

    // },[])


    if (discount == undefined) {
        offerProduct = false;
        discount = "0";
        offer_price = product_price;
    }

    // console.log('route', itemDetail)


    const device_type = Platform.OS == 'android' ? "android" : Platform.OS == 'web' ? "web" : "ios"
    useEffect(async () => {

        if (route.params.item != undefined) {
            await setCode(route.params.item.dial_code)
            await setCountryName(route.params.item.code.toLowerCase())

        }
        else {
            setCode("+254")
            setCountryName('ke')
        }
    })



    //  console.log('----------- countryName ---', countryName, code)
    window.FB.CustomerChat.hide()

    console.log('-----------offer_id----------', offer_id)
    console.log('----------- aa ---', itemDetail)

    const onConfirmOrder = () => {
        if (!phone) {
            setPhoneError("Plaese enter phone number")
        }

        else if (isNaN(phone)) {
            setPhoneError("Please enter valid character")
        }
        else if (!name) {
            setPhoneError(null)
            setErrorName('Please enter your name')
        }
        else {
            var updata
            if (offerProduct) {
                updata = {
                    subtotal: product_price,
                    discount: discount,
                    total: offer_price,
                    pickup_address: pickup_address,
                    country_code: code,
                    country_shortCode: countryName,
                    phone_no: phone,
                    name: name,
                    product_id: product_id,
                    device_id: product_name,
                    device_type: device_type,
                    device_token: "",
                    offer_id: offer_id
                };
                createOrder({
                    variables: updata,
                }).then((res) => {
                    setNormalData(res.data);
                    setModalVisible(!isModalVisible)
    
                }).catch((err) => {
                    console.log('--------err', err)
    
                })
           
            } else {
                updata = {
                    subtotal: product_price,
                    discount: discount,
                    total: offer_price,
                    pickup_address: pickup_address,
                    country_code: code,
                    country_shortCode: countryName,
                    phone_no: phone,
                    name: name,
                    product_id: product_id,
                    device_id: product_name,
                    device_type: device_type,
                    device_token: ""
                };
                createNormalOrder({
                    variables: updata,
                }).then((res) => {
                    setNormalData(res.data);

                    setModalVisible(!isModalVisible)
    
                }).catch((err) => {
                    console.log('--------err', err)
    
                })
                
            }
            console.log('--------updata', updata)
            setPhoneError(null)
            setErrorName(null)
        }
        //     createOrder({
        //         variables: updata,
        //     }).then((res) => {
        //         setModalVisible(!isModalVisible)

        //     }).catch((err) => {
        //         console.log('--------err', err)

        //     })
        //     setPhoneError(null)
        //     setErrorName(null)
        // }
    }




    const on_mPaisa_PaymentRequest = () => {

        navigation.navigate("mPaisaPaymentConfirmation", { itemDetail,
             id: normalOrderData.createOrder._id,
              customer_id: normalOrderData.createOrder.customer_id 
            })
        setModalVisible(false);
    }

    const on_mCash_PaymentRequst = () => {
        navigation.navigate("mCashPaymentConfirmation", { itemDetail,
             id: normalOrderData.createOrder._id,
              customer_id: normalOrderData.createOrder.customer_id
             })
        setModalVisible(false);
    }

    const on_Valora_PaymentRequest = () => {
        navigation.navigate("valoraPaymentConfirmation", { itemDetail, 
            id: normalOrderData.createOrder._id,
             customer_id: normalOrderData.createOrder.customer_id 
             })
        setModalVisible(false);
    }


    console.log('------normalOrderData--',normalOrderData)
    return (
        <>
            {loading || normalOrderLoading ?
                <View style={styles.indicator}>
                    < ActivityIndicator size="large" color={Color.skyBlue} />
                </View>
                :
                <View style={styles.container}>
                    <Header
                        leftIcon={{ uri: backIcon }}
                        onLeftPress={() => navigation.goBack()}// navigation.navigate('webTabNavigation', { screen: 'Nuzo_Shop' })}
                        leftIconStyle={styles.menuLeftIcon}
                        leftNextText="Checkout"
                    />
                    <View style={styles.main}>

                        <Text style={styles.heading}>Order Summary</Text>
                        <View style={styles.title}>
                            <Text style={styles.titleText}>Subtotal</Text>
                            <Text style={styles.titleText}> {currency} {product_price}</Text>
                        </View>

                        <View style={styles.title}>
                            <Text style={styles.titleText}>Discount</Text>
                            <View style={styles.discount}>
                                <Image source={{ uri: percentage }} style={styles.percentIcon} />
                                <Text style={styles.titleText}>- {currency} {discount}</Text>
                            </View>
                        </View>

                        <View style={styles.border} />

                        <View style={styles.title}>
                            <Text style={styles.total}>Total</Text>
                            <Text style={styles.total}> {currency} {offer_price}</Text>
                        </View>

                        <Text style={styles.nextHeading}>Seller Details</Text>
                        <View style={styles.locationView}>
                            <Text style={styles.normalHeading}>Seller Name: </Text>
                            <Text style={styles.locationText}>{marchant_name}</Text>
                        </View>

                        <View style={styles.locationView}>
                            <Text style={styles.normalHeading}>Pickup Address: </Text>
                            <Text style={styles.locationText}>{pickup_address}</Text>
                        </View>

                        <View>

                            <Text style={styles.inputHeading}>Phone Number</Text>

                            <View style={styles.textInputView}>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('Country_ModelBoxs', { checkoutdata: 'checkout' })}
                                    style={styles.inputCodeBoxView}>
                                    <Text
                                        style={styles.inputCodeBox}>{code}</Text>
                                    <Image source={{ uri: dropdown }} style={styles.dropdown} />

                                </TouchableOpacity>

                                <TextInput
                                    style={styles.inputBox}
                                    value={phone}
                                    onChangeText={(text) => setPhone(text)}
                                    keyboardType="phone-pad"
                                />
                            </View>
                            <Text style={styles.errorText}>{phoneError}</Text>

                            <Text style={styles.inputHeading}>Name</Text>
                            <TextInput
                                style={styles.input}
                                onChangeText={(text) => setName(text)}
                                value={name}
                            />
                            <Text style={styles.errorText}>{errorName}</Text>


                            <TouchableOpacity style={styles.button}
                                onPress={onConfirmOrder}>
                                <Text style={styles.buttonText}>Place order</Text>
                            </TouchableOpacity>

                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={isModalVisible}
                                onDismiss={() => {
                                    console.log('model closed')
                                }}>
                                <TouchableOpacity
                                    style={{
                                        flex: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                    activeOpacity={1}
                                    onPressOut={() => setModalVisible(false)}
                                >


                                    <View style={{
                                        width: '100%',
                                        height: 430,
                                        backgroundColor: "#ffffff",
                                        position: 'absolute',
                                        bottom: 0,
                                        padding: 20
                                    }}>
                                        <Text style={styles.paymentHeading}>Choose your preferred payment method</Text>
                                        <TouchableOpacity onPress={on_mCash_PaymentRequst}
                                            style={styles.paymentView}>
                                            <Text style={styles.paymentName}>Pay with Cash</Text>
                                            <View style={styles.paymentMiddleView}>
                                                <Text style={styles.paymentDetail}>Payment will be accepted by the merchant when you go to collect your item or upon delivery.</Text>
                                                <Image source={payWith_cash} style={styles.payWith_Valora} />
                                            </View>
                                        </TouchableOpacity>


                                        <TouchableOpacity onPress={on_mPaisa_PaymentRequest} style={styles.paymentView}>
                                            <Text style={styles.paymentName}>Pay with M-Pesa</Text>
                                            <View style={styles.paymentMiddleView}>
                                                <Text style={styles.paymentDetail}>Payment will be processed from your M-Pesa account using the normal fees</Text>
                                                <Image source={payWith_mpesa} style={styles.payWith_mpesa} />
                                            </View>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={on_Valora_PaymentRequest}
                                            style={styles.paymentView}>
                                            <Text style={styles.paymentName}>Pay with Valora</Text>
                                            <View style={styles.paymentMiddleView}>
                                                <Text style={styles.paymentDetail}>Valora offers cheaper processing fees compared to payment with M-Pesa</Text>
                                                <Image source={payWith_Valora} style={styles.payWith_Valora} />
                                            </View>
                                        </TouchableOpacity>


                                    </View>

                                </TouchableOpacity>
                            </Modal>

                        </View>

                    </View>
                </View>
            }
        </>
    )
}


