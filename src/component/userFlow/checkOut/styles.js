import { StyleSheet } from 'react-native'
import Fonts from '../../../assets/fonts'
import Color from '../../../utils/Colors'


export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.webbackgroud
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 250
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85
    },
    main: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20
    },
    heading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 24,
        marginBottom: 8,
        fontWeight: '600',  
    },
    title: {
        marginRight: 22,
        flexDirection: 'row',
        marginTop: 12,
        justifyContent: 'space-between'
    },
    titleText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
    },
    discount: {
        width: 150,
        flexDirection: 'row',
        alignItems: 'center',
        aligh:"end",
        justifyContent: "flex-end",
    },
    percentIcon: {
        height: 16.02,
        width: 15.27,
        marginEnd:10,
        alignItems: 'center',
        textAlign:"end",

    },
    border: {
        height: 1,
        backgroundColor: Color.borderColor,
        // marginVertical: 12
        marginTop: 12,
        marginBottom: 12
    },
    total: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        fontWeight: 'bold',
        fontStyle: 'normal',
    },
    nextHeading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginTop: 60,
        marginBottom: 14.5,
        fontWeight: '600',
    },
    locationView: {
        marginBottom: 10,

        flexDirection: 'row'
    },
    locationIcon: {
        height: 16,
        width: 10,
        marginRight: 17
    },
    locationText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
    },
    inputHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginTop: 18.6,
        marginBottom: 6.5,
        fontStyle: 'normal',
    },
    normalHeading: {
        width:120,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        fontStyle: 'normal',
    },
    textInputView: {
        height: 52,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        borderRadius: 16,
        marginTop: 6.5,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    inputCodeBoxView: {
        width: '25%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        borderRightWidth: 1,
        borderRightColor: Color.borderColor,
        paddingLeft: 10,
        paddingRight: 10,
    },
    inputCodeBox: {
        width: '59%',
       fontSize: 12,
       color: Color.skyBlue,
       textAlign: 'center',
       alignContent: 'flex-end'
    },
    dropdown: {
        height: 7.41,
        width: 12,
        marginLeft: 10
    },
    inputBox: {
        paddingLeft: 15,
        paddingRight: 15,
        width: '78%',
        fontSize: 12,
        color: Color.skyBlue
    },
    input: {
        height: 52,
        width: '100%',
        backgroundColor: Color.skyBlue_5,
        borderRadius: 16,
        borderColor: Color.borderColor,
        borderWidth: 1,
        paddingLeft: 15,
        paddingRight: 15,
        color: Color.skyBlue
    },
    errorText: {
        fontSize: 9,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        marginTop: 8,
        color: Color.red,
        marginLeft: 10,

    },
    button: {
        height: 52,
        borderRadius: 16,
        backgroundColor: Color.skyBlue,
        // marginVertical: 28,
        marginTop: 28,
        marginBottom: 28,
        alignItems: 'center',
        justifyContent: 'center'
    },
    
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    },
    
    paymentHeading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        fontWeight: "600",
        marginBottom: 5

    },
    paymentView: {
        padding: 15,
        marginVertical: 15,
        backgroundColor: Color.white,
        shadowColor: "#000000",
        paddingBottom: 15,
        paddingTop: 15,
        borderRadius: 13,
        shadowOpacity: 0.2,
        shadowRadius: 12,
        shadowOffset: {
            height: 0,
            width: 0
        },
        elevation: 1,
        paddingLeft: 12,
        paddingRight: 12,
    },
    paymentName: {
        fontSize: 15,
        color: Color.blue_7,
        fontWeight: "500",
        fontFamily: Fonts.Samsung
    },
    paymentDetail: {
        fontSize: 12,
        color: Color.black_60,
        fontWeight: "500",
        paddingTop: 3,

        fontFamily: Fonts.Samsung
    },
    paymentFee: {
        fontSize: 12,
        color: Color.blue_8,
        fontWeight: "500",
        fontFamily: Fonts.Samsung
    },

    paymentMiddleView: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        
    },

    discountPrice: {
        color: Color.green
    },

    payWith_mpesa: {
        height: 29,
        width: 62,
    },

    payWith_Valora: {
        height: 44,
        width: 44,

    }
})