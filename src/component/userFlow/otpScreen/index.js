// import React, { useState, useEffect, useRef } from 'react'
// import { Text, TouchableOpacity, View, Image, TextInput, ActivityIndicator, KeyboardAvoidingView } from 'react-native'
// import Header from '../../../utils/Header/Header'
// import otp_logo from '../../../assets/otp.png'
// import styles from './styles'
// import back_arrow from '../../../assets/back_arrow.png'
// import { useMutation } from '@apollo/client'
// import Color from '../../../utils/Colors'
// import { CUSTOMER_OTP, CUSTOMER_RESEND_OTP } from '../../../graphql/customerMutation/customerOtp'
// import AsyncStorage from '@react-native-async-storage/async-storage'



// export default function Otp_Screen({ navigation, route }) {
//     // console.log("route value is", route.params)
//     const [pins, setPins] = useState('')
//     const pinLength = 6
//     let textInput = useRef(null)
//     const [verify_Otp, { data, loading, error }] = useMutation(CUSTOMER_OTP)
//     const [resend_Otp, { data: resend_otpData, loading: resendLoading, error: resend_error }] = useMutation(CUSTOMER_RESEND_OTP)
//     const phonenumber = route.params.phone
//     const _id = route.params.id
//     const token = route.params.token
//     const lastindex = phonenumber.slice(-3)

//     // console.log('----', pins, token)


//     window.FB.CustomerChat.hide()

//     const onNextScreen = () => {
//         if (!pins) {
//             alert('Please enter otp')
//         }
//         else if (pins.length != 6) {
//             alert('Please enter your 6 digit otp')
//         }
//         else {
//             verify_Otp({
//                 variables: {
//                     customer_id: _id,
//                     otp: pins,
//                 },
//             }).then((res) => {
//                 // console.log('otp res is ---------', res)  
//                 if (res.data.verifyOTPtoCustomer == null) {
//                     alert("Otp is not match")
//                 }
//                 else {
//                     AsyncStorage.setItem("usertoken", token)
//                     AsyncStorage.setItem("customer_id", _id)
//                     // navigation.navigate("Nuzo_Points")
//                     navigation.navigate('webTabNavigation', { screen: 'Nuzo_Points' })
//                 }

//             }).catch((err) => {
//                 alert("invalid otp")
//             });
//         }
//     }


//     // useEffect( async()=> {
//     //     if (data != undefined){
//     //        await navigation.navigate("Nuzo_Points")
//     //         await AsyncStorage.setItem("usertoken", token)
//     //         await AsyncStorage.setItem("customer_id", _id)
//     //     }
//     // })

//     // console.log("-------otp success", resend_otpData)
//     // console.log("-------otp resend_error", resend_error)

//     // console.log("-------otp success------", data)




//     const resendOtp = () => {
//         console.log('pins', _id)

//         resend_Otp({
//             variables: {
//                 customer_id: _id,
//             },
//         })
//     }
//     const onChangeText = (text) => {
//         setPins(text)
//     }

//     useEffect(() => {
//         setTimeout(() => { textInput.current.focus() }, 100)
//     }, [])




//     return (
//         <>
//             {loading || resendLoading ?
//                 <View style={styles.indicator}>
//                     < ActivityIndicator size="large" color={Color.skyBlue} />
//                 </View>
//                 :
//                 <View>
//                     <Header
//                         leftIcon={back_arrow}
//                         leftNextText="OTP Verification"
//                         onLeftPress={() => navigation.goBack()}
//                         leftIconStyle={styles.menuLeftIcon}
//                     />
//                     <View
//                         // keyboardVerticalOffset= {50}
//                         // behavior="padding"
//                         style={styles.main}>
//                         <KeyboardAvoidingView
//                             keyboardVerticalOffset={50}
//                             behavior="padding"
//                             style={styles.main}>

//                         <Image source={otp_logo} style={styles.logo} />
//                         <Text style={styles.heading}>Enter your authentication code to proceed</Text>
//                         <Text style={styles.title}>Type the code sent to **** *** **** {lastindex} in the field below.</Text>


//                         <TextInput
//                         maxLength={pinLength}
//                         style={{ width: 0, height: 0 }}
//                         returnKeyType="done "
//                         keyboardType="number-pad"
//                         value={pins}
//                         onChangeText={onChangeText}
//                         ref={(input) => textInput = input} />

//                         <View style={styles.inputStyles}
//                         // onPress={() => dismissKeyboard()}
//                         >
//                     {
//                         Array(pinLength).fill().map((data, index) => (
//                         <View
//                         key={index}
//                         style={styles.input}>
//                         <Text style={styles.optText}
//                         onPress={() => textInput.current.focus()}
//                         >
//                     {pins && pins.length > 0?pins[index]: ""}
//                         </Text>
//                         </View>
//                     ))
//                     }

//                         </View>


//                         <Text style={styles.instruction}>It may take a minute to receive your code.</Text>
//                         <Text style={styles.instruction}>Haven’t received it?
//                         <Text style={styles.activeInstruction} onPress={resendOtp}>
//                         Resend a new code</Text></Text>

//                         <TouchableOpacity style={styles.button}
//                         onPress={onNextScreen}>
//                         <Text style={styles.buttonText}>Verify</Text>
//                         </TouchableOpacity>
//                         </KeyboardAvoidingView>
//                     </View>

//                 </View>
//             }
//         </>
//     )
// }









import React, { useState, useEffect, useRef } from 'react'
import { Text, TouchableOpacity, View, Image, TextInput, ActivityIndicator, KeyboardAvoidingView, Linking } from 'react-native'
import Header from '../../../utils/Header/Header'
import otp_logo from '../../../assets/otp.png'
import styles from './styles'
import back_arrow from '../../../assets/back_arrow.png'
import { useMutation } from '@apollo/client'
import Color from '../../../utils/Colors'
import { CUSTOMER_OTP, CUSTOMER_RESEND_OTP } from '../../../graphql/customerMutation/customerOtp'
import AsyncStorage from '@react-native-async-storage/async-storage'



export default function Otp_Screen({ navigation, route }) {
    // console.log("route value is", route.params)
    const [pins, setPins] = useState('')
    const pinLength = 6
    const [firsttextInput, setFirsttextInput] = useState('')
    const firsttextInputRef = useRef(null)

    const [sectextInput, setSectextInput] = useState('')
    const sectextInputRef = useRef(null)

    const [thirdtextInput, setThirdtextInput] = useState('')
    const thirdtextInputRef = useRef(null)

    const [forthtextInput, setForthtextInput] = useState('')
    const forthtextInputRef = useRef(null)

    const [fifthtextInput, setFifthtextInput] = useState('')
    const fifthtextInputRef = useRef(null)

    const [sixtextInput, setSixtextInput] = useState('')
    const sixtextInputRef = useRef(null)
    const subRef = useRef(null)

    const [showAlertBox, setShowAlertBox] = useState(true)

    let textInput = useRef(null)
    const [verify_Otp, { data, loading, error }] = useMutation(CUSTOMER_OTP)
    const [resend_Otp, { data: resend_otpData, loading: resendLoading, error: resend_error }] = useMutation(CUSTOMER_RESEND_OTP)
    const phonenumber = route.params.phone
    const _id = route.params.id
    const token = route.params.token
    const lastindex = phonenumber.slice(-3)

    // console.log('----', pins, token)


    window.FB.CustomerChat.hide()

    const onNextScreen = () => {
        console.log('otp res is ---------', pins)

        if (!pins) {
            alert('Please enter otp')
        }
        else if (pins.length != 6) {
            alert('Please enter your 6 digit otp')
        }
        else {
            verify_Otp({
                variables: {
                    customer_id: _id,
                    otp: pins,
                },
            }).then((res) => {
                // console.log('otp res is ---------', res)  
                if (res.data.verifyOTPtoCustomer == null) {
                    alert("Otp is not match")
                }
                else {
                    AsyncStorage.setItem("usertoken", token)
                    AsyncStorage.setItem("customer_id", _id)
                    // navigation.navigate("Nuzo_Points")
                    navigation.navigate('webTabNavigation', { screen: 'Nuzo_Points' })
                }

            }).catch((err) => {
                alert("invalid otp")
            });
        }
    }

    const check_resendOtp = () => {
        setShowAlertBox(true)
    }



    const resendOtp = () => {
        resend_Otp({
            variables: {
                customer_id: _id,
            }
        }).then((res) =>
        //  console.log(res)
         setShowAlertBox(false)
        )
            .catch((err) => alert(err))

        
    }


    useEffect(() => {
        setTimeout(() => { firsttextInputRef.current.focus() }, 100)
    }, [])


    useEffect(() => {
        setPins(firsttextInput + sectextInput + thirdtextInput + forthtextInput + fifthtextInput + sixtextInput);

        if (pins.length < 6) {
            if (firsttextInput.length == 1) {
                if (sectextInput.length == 1) {
                    if (thirdtextInput.length == 1) {
                        if (forthtextInput.length == 1) {
                            if (fifthtextInput.length == 1) {
                                if (sixtextInput.length == 1) {
                                    subRef.current.focus()
                                } else {
                                    sixtextInputRef.current.focus()
                                }
                            } else {
                                fifthtextInputRef.current.focus()
                            }
                        } else {
                            forthtextInputRef.current.focus()
                        }
                    } else {
                        thirdtextInputRef.current.focus()
                    }
                } else {
                    sectextInputRef.current.focus()
                }
            }
        }

    })




    return (
        <>
            {loading || resendLoading ?
                <View style={styles.indicator}>
                    < ActivityIndicator size="large" color={Color.skyBlue} />
                </View>
                :
                <View>
                    <Header
                        leftIcon={back_arrow}
                        leftNextText="OTP Verification"
                        onLeftPress={() => navigation.goBack()}
                        leftIconStyle={styles.menuLeftIcon}
                    />
                    <View
                        // keyboardVerticalOffset= {50}
                        // behavior="padding"
                        style={styles.main}>
                        <KeyboardAvoidingView
                            keyboardVerticalOffset={50}
                            behavior="padding"
                            style={styles.main}>

                            <Image source={otp_logo} style={styles.logo} />
                            <Text style={styles.heading}>Enter your authentication code to proceed</Text>
                            <Text style={styles.title}>Type the code sent to **** *** **** {lastindex} in the field below.</Text>
                            <View style={styles.inputBoxView}>
                                <TextInput
                                    keyboardType="number-pad"
                                    style={styles.inputBox}
                                    value={firsttextInput}
                                    maxLength={1}
                                    onChangeText={(text) => setFirsttextInput(text)}
                                    ref={firsttextInputRef} />


                                <TextInput
                                    keyboardType="number-pad"
                                    style={styles.inputBox}
                                    value={sectextInput}
                                    maxLength={1}

                                    onChangeText={(text) => setSectextInput(text)}
                                    ref={sectextInputRef} />


                                <TextInput
                                    keyboardType="number-pad"
                                    style={styles.inputBox}
                                    value={thirdtextInput}
                                    maxLength={1}

                                    onChangeText={(text) => setThirdtextInput(text)}
                                    ref={thirdtextInputRef} />


                                <TextInput
                                    keyboardType="number-pad"
                                    style={styles.inputBox}
                                    value={forthtextInput}
                                    maxLength={1}

                                    onChangeText={(text) => setForthtextInput(text)}
                                    ref={forthtextInputRef} />


                                <TextInput
                                    keyboardType="number-pad"
                                    style={styles.inputBox}
                                    value={fifthtextInput}
                                    maxLength={1}

                                    onChangeText={(text) => setFifthtextInput(text)}
                                    ref={fifthtextInputRef} />


                                <TextInput
                                    keyboardType="number-pad"
                                    style={styles.inputBox}
                                    value={sixtextInput}
                                    maxLength={1}

                                    onChangeText={(text) => setSixtextInput(text)}
                                    ref={sixtextInputRef} />

                            </View>


                            <Text style={styles.instruction}>It may take a minute to receive your code.</Text>
                            <Text style={styles.instruction}>Haven’t received it?
                                <Text style={styles.activeInstruction} onPress={check_resendOtp}>
                                    Resend a new code</Text></Text>
                            {showAlertBox &&
                                <View style={styles.alertBox}>
                                    <Text style={styles.alertText}>You must enable SMS from our short code. To enable, dial *456*9*5#  Enabling SMS is free and does not attract additional charges.</Text>

                                    <View style={styles.alertButtonsView}>
                                        <TouchableOpacity onPress={() => setShowAlertBox(false)} style={styles.buttonAlert} >
                                            <Text style={styles.alertButtonText}>Dismiss</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={resendOtp} style={styles.buttonAlert} >
                                            <Text style={styles.alertButtonText}>Enable SMS</Text>
                                        </TouchableOpacity>

                                    </View>
                                </View>
                            }

                            <TouchableOpacity style={styles.button}
                                onPress={onNextScreen} ref={subRef}>
                                <Text style={styles.buttonText}>Verify</Text>
                            </TouchableOpacity>
                        </KeyboardAvoidingView>
                    </View>

                </View>
            }
        </>
    )
}