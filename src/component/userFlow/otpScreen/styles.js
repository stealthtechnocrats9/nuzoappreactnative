import { StyleSheet } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white

    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85
    },
    main: {
        marginHorizontal: 20,
    },
  
    logo: {
        alignSelf: 'center',
        height: 46.59,
        width: 46.64,
        marginVertical: 23.4
    },
    heading: {
        fontSize: 15,
        color: Color.black,
        fontWeight: "600",
        fontFamily: Fonts.Samsung,
        alignSelf: 'center',

    },
    title: {
        fontSize: 13,
        color: Color.black_60,
        fontWeight: "500",
        fontFamily: Fonts.Samsung,
        alignSelf: 'center',

    },
    inputStyles: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginHorizontal: 10,
        fontWeight: "500",
        fontFamily: Fonts.Samsung,
        marginTop: 40,
        marginBottom: 20

    },
    input: {
        height: 44,
        width: 44,
        borderColor: Color.borderColor,
        borderWidth: 1,
        borderRadius: 10,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5
    },
    instruction: {
        alignSelf: 'center',
        fontSize: 12,
        color: Color.black_60
    },
    optText: {
        color: Color.skyBlue
    },

    activeInstruction: {
        color: Color.skyBlue
    },
    button: {
        backgroundColor: Color.skyBlue,
        height: 52,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
        },
        inputBoxView: {
            flexDirection: 'row',
            margin: 10,
            justifyContent: 'center'
        },

        inputBox: {
            height: 44,
            width: 44,
            borderColor: Color.borderColor,
            borderWidth: 1,
            borderRadius: 10,
            textAlign: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 5
        },
        alertBox: {
            height: 160,
            width: "90%",
            backgroundColor: Color.white,
            alignSelf: 'center',
            position: 'absolute',
            justifyContent: 'center',
            borderRadius: 16
            
        },
    
        alertText: {
            fontSize: 14,
            fontFamily: Fonts.Samsung,
            color: Color.black,
            margin:15,
    
            fontStyle: 'normal',
            textAlign: 'center'
        },
        alertButtonsView: {
            flexDirection: 'row',
            marginLeft:10,
            marginRight:10,
            justifyContent: 'space-around'
        },
       
        buttonAlert: {
            height: 40,
            width: '45%',
            backgroundColor: Color.skyBlue,
            marginTop: 10,
            borderRadius: 16,
            justifyContent: 'center',
            alignItems: 'center'
        },
        alertButtonText: {
            fontSize: 15,
            fontFamily: Fonts.Samsung,
            color: Color.whiteText,
            fontStyle: 'normal',
            fontWeight: '600',
            textAlign: 'center'
        },
       
})