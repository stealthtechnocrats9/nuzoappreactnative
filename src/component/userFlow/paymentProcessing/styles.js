import { StyleSheet } from "react-native";
import Fonts from "../../../assets/fonts";
import Color from "../../../utils/Colors";



export default StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: Color.webbackgroud
    },
    heading : {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.blue_7,
        alignSelf: 'center'
    },
    topTitle: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7 ,
        marginLeft: 58,
        textAlign: 'center',
        marginRight: 58,
        marginTop: 10,

        marginBottom: 40
    },
    bottomTitle: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7 ,
        textAlign: 'center',
        marginLeft: 38,
        marginRight: 38,
        marginBottom: 28
    },
    button: {
        height: 52,
        borderRadius: 16,
        borderColor: Color.borderColor,
        borderWidth: 1,
        // backgroundColor: Color.white,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20,
        marginVertical: 12
    },
    ButtonTextStyle: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'

    },
    ActiveButtonTextStyle: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        fontWeight: "700",
        color: Color.white
    }

})