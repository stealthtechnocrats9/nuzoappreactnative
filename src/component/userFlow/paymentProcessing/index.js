import React from 'react'
import { Text, View, Pressable, ActivityIndicator } from 'react-native'
import styles from './styles'
import Color from '../../../utils/Colors'


export default function Payment_Processing({ navigation, route }) {

    const merchantDetail = route.params.merchantDetail

    const percentge = route.params.percentge
    const onLoginConfirm = () => {
        navigation.navigate('webTabNavigation', { screen: 'Nuzo_Points' })
    }


    return (
        <View style={styles.container}>
            <View style={{marginTop: 80}}>
            </View> 
            <Text style={styles.heading}>Order Received</Text>
            <Text style={styles.topTitle}>Thank you for your order from {merchantDetail.store_detail.store_name} to pickup your order pease visit {merchantDetail.store_detail.shop_address} or call {merchantDetail.payment_option} </Text>
            <Text style={styles.bottomTitle}>After the store confirm your payment you will receive {percentge} Nuzo Points.</Text>


            {/* <Pressable
                onPress={onLoginConfirm}
                style={({ pressed }) => [
                    {
                        backgroundColor: pressed
                            ? Color.skyBlue
                            : Color.white
                    },
                    styles.button
                ]}>

                {({ pressed }) => (
                    <Text style={
                        pressed ? styles.ActiveButtonTextStyle : styles.ButtonTextStyle}>Check Nuzo Points</Text>
                )}
            </Pressable> */}

            <Pressable
                onPress={onLoginConfirm}
                style={({ pressed }) => [
                    {
                        backgroundColor: pressed
                            ? Color.skyBlue
                            : Color.white
                    },
                    styles.button
                ]}>

                {({ pressed }) => (
                    <Text style={
                        pressed ? styles.ActiveButtonTextStyle : styles.ButtonTextStyle}>Check order status</Text>
                )}
            </Pressable>
        </View>
    )
}

