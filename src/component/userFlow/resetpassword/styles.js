import { StyleSheet } from 'react-native';
import Fonts from '../../../assets/fonts';
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        // alignItems: 'center',

    },
    menuLeftIcon: {
        height: 16,
        width: 15.85
    },

    main: {
        width: '100%',
        paddingHorizontal: 20,

    },
    headingText: {
        fontSize: 18,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        // fontFamily: "Samsung Sharp Sans",
        fontWeight: 'bold',
        marginTop: 12,
        marginBottom: 4
    },

    inputBoxHeading: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        // fontFamily: 'Samsung Sharp Sans',
        fontWeight: 'bold',
        marginTop: 18.5,
        marginBottom: 6.5,
    },

    input: {
        height: 52,
        borderRadius: 10,
        borderWidth: 1,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        paddingHorizontal: 10,
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },
    InputViewStyle: {
        flexDirection: 'row',
        height: 52,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        marginVertical: 6.5,
        width: '100%',
        alignSelf: 'center'

    },
    errorText: {
        fontSize: 11,
        fontFamily: Fonts.Samsung,
        fontWeight: "500",
        marginTop: 8,
        color: Color.red,
        marginLeft: 5,
    },
    passwordInput: {
        height: 50,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        alignContent: 'center',
        borderColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '85%',
        paddingHorizontal: 10,
        backgroundColor: Color.skyBlue_5,
        color: Color.skyBlue,
        fontSize: 15,
        fontFamily: Fonts.Samsung,
    },
    showHidePassword: {
        height: 50,
        borderLeftWidth: 1,
        alignContent: 'center',
        borderLeftColor: Color.borderColor,
        backgroundColor: Color.skyBlue_5,
        width: '15%',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    showHideText: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,

    },

    button: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center',
        height: 52,
        backgroundColor: Color.skyBlue,
        borderRadius: 10,
        marginHorizontal: 20
    },

    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontWeight: '600'
    },

})