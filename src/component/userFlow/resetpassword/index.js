
import React, { useState } from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import backIcon from '../../../assets/back_arrow.png'
import { RESET_PASSWORD } from '../../../graphql/customerMutation/resetPassword'
import { useMutation } from '@apollo/client'


export default function ResetPassword({ navigation }) {
    const [email, setEmail] = useState('')
    const [emailError, setEmailError] = useState('')

    const [password, setPassword] = useState('')
    const [passwordError, setPasswordError] = useState('')

    const [isShow, setIsShow] = useState(true)
    const [newPassword, { data, loading, error }] = useMutation(RESET_PASSWORD)

    const queryParams = window.location.href;
    const lastSegment = queryParams.split("=").pop();


    console.log('------------last seg', lastSegment.toString())
    const onLogin = () => {
        // navigation.navigate('store_create')
        if (!email) {
            setEmailError('Please enter your new password')
        }
        else if (email.length < 6) {
            setEmailError('Please enter minimum 6 digit passwors')
        }
        else if (!password) {
            setPasswordError('Please enter your new password')
        }
        else if (password !== email) {
            setPasswordError('Password does not match')
        }


        else {
            setEmailError(null)
            setPasswordError(null)
            newPassword({
                variables: {
                    token: lastSegment,
                    password: email,
                    confirmPassword: password,
                },
            }).then((res) => {
                alert('Password reset successfully. Please login with the Nuzo app.');
            }).catch((err) => alert(err))
        }
    }

    const onTogglePassword = () => {
        setIsShow(!isShow)
    }

    return (
        <SafeAreaView style={styles.container}>

            <Header
                leftIcon={{ uri: backIcon }}
                onLeftPress={() => navigation.goBack()}
                leftIconStyle={styles.menuLeftIcon}
                leftNextText="Reset Password"
            />

            <View style={styles.main}>

                <Text style={styles.headingText}>Enter your new password</Text>

                <Text style={styles.inputBoxHeading}>New password</Text>
                <TextInput
                    style={styles.input}
                    value={email}
                    onChangeText={(text) => setEmail(text)}

                />
                <Text style={styles.errorText}>{emailError}</Text>


                <Text style={styles.inputBoxHeading}> Confirm password</Text>
                <View style={styles.InputViewStyle}>
                    <TextInput
                        style={styles.passwordInput}
                        value={password}
                        secureTextEntry={isShow}
                        onChangeText={(text) => setPassword(text)}
                    />

                    <TouchableOpacity style={styles.showHidePassword}
                        onPress={onTogglePassword}>
                        {isShow ?
                            <Text style={styles.showHideText}>Show</Text>
                            :
                            <Text style={styles.showHideText}>Hide</Text>
                        }
                    </TouchableOpacity>
                </View>
                <Text style={styles.errorText}>{passwordError}</Text>


                <TouchableOpacity onPress={onLogin} style={styles.button}>
                    <Text style={styles.buttonText}>Reset password</Text>
                </TouchableOpacity>

            </View>
        </SafeAreaView>
    )
}


