import { StyleSheet } from "react-native";
import Fonts from "../../../assets/fonts";
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.webbackgroud
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85
    },
    main: {
        marginLeft: 20,
        marginRight: 20
    },
    paymentTitle: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.black,
        marginBottom: 24,
        fontStyle: 'normal'
    },
    heading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        marginBottom: 16,
        fontStyle: 'normal'

    },
    valoraButton: {
        height: 52,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.borderColor,
        alignSelf: 'center',
        marginTop: 14,
        marginBottom: 24
    },
    valoraButtonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    },
    accountDetailBox: {
        backgroundColor: Color.skyBlue_5,
        paddingTop: 26.5,
        paddingLeft: 7,
        paddingBottom: 32,
        paddingRight: 6
    },
    amountTopHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        marginBottom: 9.5,
        fontStyle: 'normal'


    },
    accountBoxPhoneNumberView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 29.5,
        marginBottom: 6.5,
        alignItems: 'center'
    },
    amountHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,

    },
    amountmobTitle: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
    },
    amountDetail: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        fontWeight: "bold",
        marginTop: 3,
        marginBottom: 3,
        fontStyle: 'normal'

    },



    inputStyles: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: 3,
        flexWrap: 'wrap'
     
    },
    input: {
        height: 40,
        textAlign: 'center',
        justifyContent: 'center',
        backgroundColor: Color.white
    },


    optText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        alignItems: 'center',
        justifyContent: 'space-around',
        color: Color.skyBlue,
        paddingVertical: 8,
        paddingHorizontal: 3
    },
    swipeButton: {
        height: 52,
        width: '100%',
        borderRadius: 16,
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: Color.skyBlue,
        marginTop: 24,
        marginBottom: 32,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        marginLeft: 15,
        marginRight: 15
    },
    swipeConfirmButton: {
        height: 52,
        width: '100%',
        borderRadius: 16,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.white,
        marginTop: 24,
        marginBottom: 32,
        flexDirection: 'row',
        paddingLeft: 14,
        paddingRight: 14,
        flex: 1,
        marginLeft: 5,
        marginRight: 5
    },
    swipeView: {
        height: 52,
        width: 52,
        backgroundColor: Color.white,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.skyBlue
    },


    swipeArrow: {
        height: 16,
        width: 16
    },

    swipeConirmArrow: {
        height: 16,
        width: 16,

    },
    swipeText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.whiteText,
        alignSelf: 'center'

    },
    swipeConfirmText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.skyBlue,
        marginRight: 36,
        alignSelf: 'center'
    }
})