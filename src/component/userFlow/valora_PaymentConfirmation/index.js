import React, { useState, useEffect, useRef } from 'react'
import { Text, View, Image, TextInput, Animated, TouchableOpacity, ActivityIndicator, Linking } from 'react-native'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import backIcon from '../../../assets/back_arrow.png'
import swipeArrow from '../../../assets/swipeArrow.png'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import { CREATE_PAYMENT } from '../../../graphql/customerMutation/createPayment'
import { useMutation } from '@apollo/client'
import Color from '../../../utils/Colors'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function valoraPaymentConfirmation({ navigation, route }) {

    const merchantDetail = route.params.itemDetail
    const Checkout_id = route.params.id
    const customerId = route.params.customer_id
    const [pins, setPins] = useState(null)
    const [pinLength, setPinLength] = useState(10)
    let textInput = useRef(null)
    const [percentge, setPercentge] = useState('')
    const nuzopoints = merchantDetail.offer_price


    useEffect(() => {
        setPins(merchantDetail.payment_option)
        if (pins != null) {
            setPinLength(pins.length)
        }
        let percentVal = merchantDetail.offer_price
        let percentToget = 1
        let res = (percentToget / 100) * percentVal

        setPercentge(Math.floor(res))
    });


    const [createPayment, { data, loading, error }] = useMutation(CREATE_PAYMENT)


    console.log('-----customerId--', customerId)

    // console.log('=Checkout_id===', Checkout_id, merchantDetail)
    const onPaymentConfirm = () => {
        createPayment({
            variables: {
                order_id: Checkout_id,
                customer_id: customerId,
                amount: merchantDetail.currency,
                type: "m-pesa"
            },
        }).then((res) => {
            if (res != undefined) {
                navigation.navigate("Payment_Processing", { percentge ,merchantDetail})
            }
        }).catch((err) => {
            console.log(" error----", err);
        })
    }

    // useEffect(() => {
    //     if (data != undefined) {
    //         navigation.navigate("Payment_Processing", {percentge})
    //     }
    // })

    console.log('data', data, 'error', error)

    const LeftSwipeActions = () => {
        return (
            <TouchableOpacity
                style={styles.swipeConfirmButton} >
                <View style={{ width: 30 }} />
                <Text style={styles.swipeConfirmText}>Confirming payment</Text>
                <Image source={swipeArrow} style={styles.swipeConirmArrow} />

            </TouchableOpacity>
        );
    };


    const swipeFromLeftOpen = () => {
        // alert('Swipe from left');
    };

    return (
        <>
            {loading ?
                <View style={styles.indicator}>
                    < ActivityIndicator size="large" color={Color.skyBlue} />
                </View>
                :
                <View style={styles.container}>
                    <Header
                        leftIcon={{ uri: backIcon }}
                        onLeftPress={() => navigation.goBack()}
                        leftIconStyle={styles.menuLeftIcon}
                        leftNextText="Checkout"
                    />
                    <View style={styles.main}>
                        <Text style={styles.paymentTitle}>Pay with Valora</Text>

                        <Text style={styles.heading}>Valora is a new digital wallet that makes sending, saving, and spending digital money as easy as sending a text.</Text>

                        <Text style={styles.heading}>With Valora, you can send money to any smartphone, anywhere in the world within seconds.</Text>

                        {/* <Text style={styles.heading}> It also allows you to use stablecoins to avoid the price fluctuations of other cryptocurrencies while offering 5% annually on the cUSD you have.</Text> */}

                        <TouchableOpacity onPress={() => {  Linking.openURL('https://play.google.com/store/apps/details?id=co.clabs.valora&hl=en_US&gl=US'); }} style={styles.valoraButton} >
                        <Text style={styles.valoraButtonText}>Download Valora</Text>
                        </TouchableOpacity>

                        {/* <View style={styles.valoraButton}>
                            <Text style={styles.valoraButtonText}>Download Valora</Text>
                        </View> */}

                        <View style={styles.accountDetailBox}>

                            <Text style={styles.amountTopHeading}>Amount to send</Text>
                            <Text style={styles.amountDetail}>{merchantDetail.currency} {merchantDetail.offer_price}</Text>

                            {/* <View style={styles.accountBoxPhoneNumberView}>
                         <Text style={styles.amountHeading}>Merchant Phone Number</Text>
                         <Text style={styles.amountmobTitle}>Call merchant</Text>
                     </View> */}

                            <View style={styles.amountDetailInsideBox}>
                                <Text style={styles.amountDetail}>{merchantDetail.marchant_name}</Text>
                            </View>
                            <Text style={styles.amountHeading}>Merchant Phone Number</Text>


                            <View style={styles.inputStyles}>
                                {
                                    Array(pinLength).fill().map((data, index) => (
                                        <View
                                            key={index}
                                            style={styles.input}>
                                            <Text style={styles.optText}
                                                onPress={() => textInput.current.focus()}
                                            >
                                                {pins && pins.length > 0 ? pins[index] : ""}
                                            </Text>
                                        </View>
                                    ))
                                }

                            </View>

                        </View>



                        <Swipeable
                            renderLeftActions={LeftSwipeActions}
                            onSwipeableLeftOpen={swipeFromLeftOpen}
                            onSwipeableLeftOpen={onPaymentConfirm}

                        >
                            <View style={styles.swipeButton}>

                                <View style={styles.swipeView}>
                                    <Image source={swipeArrow} style={styles.swipeArrow} />
                                </View>
                                <Text style={styles.swipeText}>Swipe to confirm payment</Text>
                                <View style={{ height: 5, width: 20 }} />
                            </View>
                        </Swipeable>

                    </View>
                </View>
            }
        </>
    )
}





// import React, { useState, useEffect, useRef } from 'react'
// import { Text, View, Image, TextInput, Animated , TouchableOpacity} from 'react-native'
// import styles from './styles'
// import Header from '../../../utils/Header/Header'
// import backIcon from '../../../assets/back_arrow.png'
// import swipeArrow from '../../../assets/swipeArrow.png'
// import Swipeable from 'react-native-gesture-handler/Swipeable';

// export default function valoraPaymentConfirmation({ navigation }) {
//     const [pins, setPins] = useState('')
//     const pinLength = 10
//     let textInput = useRef(null)


//     const onChangeText = (text) => {
//         setPins(text)
//     };

//     useEffect(() => {
//         textInput.focus()
//     }, []);


//     const onPaymentConfirm = () => {
//         navigation.navigate("Payment_Processing")
//         alert('valora payment success')
//     }

//     const LeftSwipeActions = () => {
//         return (
//             <TouchableOpacity onPress={onPaymentConfirm}
//                 style={styles.swipeConfirmButton} >
//                 <View style={{ width: 30 }} />
//                 <Text style={styles.swipeConfirmText}>Confirming payment</Text>
//                 <Image source={swipeArrow} style={styles.swipeConirmArrow} />

//             </TouchableOpacity>
//         );
//     };


//     const swipeFromLeftOpen = () => {
//         alert('Swipe from left');
//     };

//     return (
//         <View style={styles.container}>
//             <Header
//                 leftIcon={{ uri: backIcon }}
//                 onLeftPress={() => navigation.goBack()}
//                 leftIconStyle={styles.menuLeftIcon}
//                 leftNextText="Checkout"
//             />
//             <View style={styles.main}>
//                 <Text style={styles.paymentTitle}>Pay with Valora</Text>

//                 <Text style={styles.heading}>Valora is a new digital wallet that makes sending, saving, and spending digital money as easy as sending a text.</Text>

//                 <Text style={styles.heading}>With Valora, you can send money to any smartphone, anywhere in the world within seconds.</Text>

//                 <Text style={styles.heading}> It also allows you to use stablecoins to avoid the price fluctuations of other cryptocurrencies while offering 5% annually on the cUSD you have.</Text>


//                 <View style={styles.valoraButton}>
//                     <Text style={styles.valoraButtonText}>Open Valora app</Text>
//                 </View>


//                 <View style={styles.accountDetailBox}>
//                     <Text style={styles.amountTopHeading}>Amount to send</Text>
//                     <Text style={styles.amountDetail}>KSh 1,995 (18.10 cUSD)</Text>

//                     <View style={styles.accountBoxPhoneNumberView}>
//                         <Text style={styles.amountHeading}>Merchant Phone Number</Text>
//                         <Text style={styles.amountmobTitle}>Call merchant</Text>
//                     </View>

//                     <TextInput
//                         maxLength={pinLength}
//                         style={{ width: 0, height: 0 }}
//                         returnKeyType="done"
//                         keyboardType="number-pad"
//                         value={pins}
//                         onChangeText={onChangeText}
//                         ref={(input) => textInput = input} />

//                     <View style={styles.inputStyles}>
//                         {
//                             Array(pinLength).fill().map((data, index) => (
//                                 <View
//                                     key={index}
//                                     style={styles.input}>
//                                     <Text style={styles.optText}
//                                         onPress={() => textInput.focus()}
//                                     >
//                                         {pins && pins.length > 0 ? pins[index] : ""}
//                                     </Text>
//                                 </View>
//                             ))
//                         }

//                     </View>

//                 </View>



//                 <Swipeable
//                     renderLeftActions={LeftSwipeActions}
//                     onSwipeableLeftOpen={swipeFromLeftOpen}
//                 >
//                     <View style={styles.swipeButton}>

//                         <View style={styles.swipeView}>
//                             <Image source={swipeArrow} style={styles.swipeArrow} />
//                         </View>
//                         <Text style={styles.swipeText}>Swipe to confirm payment</Text>
//                         <View style={{height: 5, width: 20}} />

//                     </View>
//                 </Swipeable>






//             </View>
//         </View>
//     )
// }

