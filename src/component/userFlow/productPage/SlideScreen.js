import React from 'react'
import { View, Image,TouchableOpacity ,Linking} from 'react-native'
import styles from './styles'


export default function SlideScreen({ item, currentIndex }) {

    console.log('-----currentIndex index', currentIndex, item)


    return (
        <TouchableOpacity onPress={() => {
            Linking.openURL('https://nuzo.co/product_images/' + item.product_image);
          }} style={styles.sliderStyle}>

            <Image source= {{ uri: 'https://nuzo.co/product_images/' + item.product_image}} style={styles.imgStyle}  />
        </TouchableOpacity>
    )
}








// import React, { useCallback, useState } from 'react'
// import { View, Text, Image, TouchableOpacity } from 'react-native'
// import styles from './styles'


// export default function SlideScreen({ item, currentIndex, parentCallback }) {
//     const [showImg, setShowImg] = useState(false)


//     return (
//         <>

//             {showImg ?
//                 <TouchableOpacity
//                     style={styles.sliderFullStyle}>
//                     <TouchableOpacity
//                         onPress={() => {
//                             setShowImg(false);
//                         }}
//                         style={styles.crossimg}>
//                         <Text style={styles.cross}>X</Text>
//                     </TouchableOpacity>
//                     <Image source={{ uri: 'https://nuzo.co/product_images/' + item.product_image }} style={styles.fullImgStyle} />
//                 </TouchableOpacity>
//                 :
//                 <TouchableOpacity
//                     onPress={() => {
//                         setShowImg(true);
//                     }}
//                     style={styles.sliderStyle}>
//                     <Image source={{ uri: 'https://nuzo.co/product_images/' + item.product_image }} style={styles.imgStyle} />
//                 </TouchableOpacity>
//             }


//         </>
//     )
// }




