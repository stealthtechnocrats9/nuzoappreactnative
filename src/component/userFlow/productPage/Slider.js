import React from 'react'
import { StyleSheet, Text, View, Animated } from 'react-native'
import Color from '../../../utils/Colors'

export default function Slider({ data, scrollX }) {




    return (
        <View style={{ flexDirection: 'row', alignSelf: 'center', backgroundColor: 'transparent'}}>
            <View style={{ flexDirection: 'row' }}>
                {data.map((_, i) => {
                    const inputRange = [(i - 1) * 252, i * 252, (i + 1) * 252];

                    const dotWidth = scrollX.interpolate({
                        inputRange,
                        outputRange: [8, 16, 8],
                        extrapolate: 'clamp'
                    })

                    const opacity = scrollX.interpolate({
                        inputRange,
                        outputRange: [0.3, 1, 0.3],
                        extrapolate: 'clamp'
                    })
                    return <Animated.View style={[styles.dot, { width: dotWidth, opacity }]}
                        key={i.toString()} />
                })}
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    dot: {
        width: 8,
        height: 4,
        borderRadius: 8,
        backgroundColor: Color.black_60,
        marginLeft: 3,
        marginRight: 3,
    },


})
