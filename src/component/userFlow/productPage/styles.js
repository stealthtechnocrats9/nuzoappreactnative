import { StyleSheet } from 'react-native'
import Fonts from '../../../assets/fonts'
import Color from '../../../utils/Colors'


export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: Color.webbackgroud,
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    backgroubdImage: {
        height: 150,
        width: '100%',
        marginTop: 5,
        marginBottom: 12,
        alignItems: 'center',
        justifyContent: 'center',

    },
    backgroubdImageText: {
        color: Color.white,
        fontSize: 20,
        fontWeight: "600",
        fontFamily: Fonts.Samsung,
        fontStyle: 'normal',
        backgroundColor: '#0000009e',
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        lineHeight: '150%',
        position: 'absolute',
        top: '0',
        left: '0',
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red',
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85
    },
   
    main: {
        marginLeft: 20,
        marginRight: 20
    },
    searchBox: {
        height: 60,
        borderWidth: 1,
        borderRadius: 10,
        paddingLeft: 15,
        borderColor: Color.borderColor,
        alignItems: 'center',
        flexDirection: 'row'
    },
    searchIcon: {
        height: 15.42,
        width: 15.42,
        marginRight: 10

    },
    input: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: 500,
        height: 40,
        width: '100%',
        color: Color.blue_7,
        borderColor: Color.webbackgroud,
        borderWidth: 1,
    },
    activeInput: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: 500,
        height: 40,
        width: '100%',
        color: Color.borderColor,
        borderColor: Color.green,
        borderWidth: 1,
    },

    productHeading: {
        fontSize: 20,
        fontWeight: '600',
        color: Color.black,
        marginTop: 24,
        marginBottom: 28,

    },
    mainBox: {
        width: '100%',
        backgroundColor: Color.white,
        paddingTop: 12,
        paddingBottom: 8,
        paddingLeft: 2,
        paddingRight: 8,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 12


        
    },
    leftView: {
        width: '15%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    productLogo: {
        height: 48,
        width: 48
    },
    right: {
        width: '85%',
        paddingLeft: 3
    },
    nameText: {
        fontSize: 17,
        fontWeight: '600',
        color: Color.blue_7,
        marginBottom: 8
    },
    priceText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,


        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    totalprice: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.red,

    },
    totalPoints: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.blue_7,

    },
    product_price: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.green
    },
   
    price_of: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.red,
        marginLeft: 15,
        textDecorationLine: 'line-through',

    },
    sliderStyle: {
        marginTop: 12,
        marginBottom: 12,
        width: 340,
        alignSelf: 'center',
        // backgroundColor: 'red'
    },
    sliderFullStyle: {
        marginTop: 12,
        marginBottom: 12,
        width: 340,
        height: '100%',
        alignSelf: 'center',
    },
    imgStyle: {
        height: 193,
        width: 320,
        alignSelf: 'center',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 15
    },

    fullImgStyle: {
        height: 500,
        width: 320,
        alignSelf: 'center',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 15
    },
    crossimg: {
        height: 20,
        width: 20,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        display: 'flex',
        position: 'absolute',
        top: 5,
        zIndex: 1,
        right: 10
    },
    cross: {
      fontSize: 20,
      color: Color.skyBlue
    },
    textHeading: {
        marginTop: 25,
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginBottom: 16,
        height: 17,

        fontStyle: 'normal',
        fontWeight: '600',
        // lineHeight: '130%'
    },
    productDetailView: {
        marginTop: 2,
        marginBottom: 2
    },
    detailView: {
        flexDirection: 'row',
        marginLeft: 15
    },
    productDetailItem: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginBottom: 4,
        width: 60,
        alignItems: 'center',
        alignSelf: 'center',
        textAlign: 'end'
        /* identical to box height, or 15px */
    },
    productDetailSaveItem: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        marginBottom: 4,
        alignItems: 'center',
        alignSelf: 'center',
        textAlign: 'end',

        fontStyle: 'normal',


    },
    price: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginLeft: 15,

    },
    priceGreen: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.green,
        marginLeft: 15,


    },
    savePrice: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.green,
        marginLeft: 15,
        fontStyle: 'normal',
    },
    checkTime: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        marginLeft: 15,
        fontWeight: '500',
        fontStyle: 'normal',
    },

    productDetailItemDetail: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.red,
    },
    productDetailItemText: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginBottom: 4,
        // alignItems: 'center',
        // alignSelf: 'center',
        // textAlign: 'end',
        fontWeight: '600',
        fontStyle: 'normal',
    },
    productDetailActualPrice: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.green,
    },
    learnMoreButton: {
        height: 43,
        width: '100%',
        borderRadius: 4,
        backgroundColor: Color.skyBlue_5,
        justifyContent: 'center',
        alignItems: 'center',
        // marginVertical: 20
        marginTop: 20, marginBottom: 20
    },
    learnMoreText: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginLeft: 10,
        marginRight: 10,
    },
    learnMoreTexts: {
        fontSize: 12,
        color: Color.black,
    },
    alertBox: {
        height: 160,
        width: "90%",
        backgroundColor: Color.white,
        alignSelf: 'center',
        position: 'absolute',
        justifyContent: 'center',
        top: 180,
        borderRadius: 16
    },

    alertHeading: {
        fontSize: 15,
        color: Color.black_60
    },
    alertButtonsView: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    clickableText: {
        color: Color.skyBlue
    },
    discription: { marginBottom: 46, },
    discriptionView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    dot: {
        height: 2,
        width: 2,
        backgroundColor: Color.black,
        borderRadius: 1,
        marginLeft: 5,
        marginRight: 5
    },
    discriptionText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        fontStyle: 'normal',
    },
    button: {
        height: 52,
        width: '100%',
        backgroundColor: Color.skyBlue,
        marginBottom: 16,
        marginTop: 28,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonAlert: {
        height: 40,
        width: '60%',
        backgroundColor: Color.skyBlue,
        marginTop: 10,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    alertText: {
        fontSize: 14,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        marginLeft: 15,
        marginRight: 15,

        fontStyle: 'normal',
        textAlign: 'center'
    },

    alertButtonText: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    }

}
)