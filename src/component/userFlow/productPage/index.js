import React, { useEffect, useState, useRef } from 'react'
import { Text, View, ImageBackground, FlatList, Image, TextInput, Animated, TouchableOpacity, Linking, ActivityIndicator, ScrollView } from 'react-native'
import styles from './styles'
import { useQuery } from '@apollo/client'
import logo from '../../../assets/Frame437.png'
import searchIcon from '../../../assets/searchIcon.png'
import LinearGradient from 'react-native-web-linear-gradient';
import Color from '../../../utils/Colors'
import SlideScreen from './SlideScreen'
import Slider from './Slider'
import { Helmet } from "react-helmet";
import Header from '../../../utils/Header/Header'
import backIcon from '../../../assets/back_arrow.png'

import { GET_ALLPRODUCT_BY_ID } from '../../../graphql/customerQueries/getProductDetailByProductId'
import { GET_ALLPRODUCT } from '../../../graphql/customerQueries/getProductDetail'
import AsyncStorage from '@react-native-async-storage/async-storage'





const ProductPage = ({ navigation }) => {
    const [searchText, setSearchText] = useState('')
    const [allData, setAllData] = useState()
    //  const [userText, setUserText] = useState('')
    var userText = "";
    const [productData, setProductData] = useState()
    const [campaignProductData, setCampaignProductData] = useState()
    const [focused, setFocused] = useState(false);
    
    const [productId, setProductId] = useState(null)
    const [showLearnMore, setShowLearnMore] = useState(false)

    const [percentage, setPercentge] = useState(null)
    // const [product_img, setProductImage] = useState(null)
    const queryParams = window.location.href;
    const lastSegment = queryParams.split("/").pop();
    userText = lastSegment;
    var changed=false;
    var isProductId=false;
    // Linking.getInitialURL().then((url) => {
    //     let data = url
    //     const lastSegment = data.split("/").pop().replaceAll("_", " ");
    var myParam=null;
    userText = userText.replaceAll("_", " ");
    //     setUserText(lastSegment)

    // });includes
    if(userText.includes("-id")){
        //setShowDetail(true);
        isProductId=true;
        myParam= userText.substring(userText.indexOf("-id")+3);

        userText = userText.substring(0,userText.indexOf("-id"));
    }
    
    const [showDetail, setShowDetail] = useState(isProductId);

    console.log('----------------url--myParam-----------',myParam,"--userText--",userText);

    const { data, error, loading } = useQuery(GET_ALLPRODUCT, {
        variables: {
            storeName: userText
        }
    });

    //  const { data: productDetail, error: getProducterrorError, loading: getProductLoading, refetch } = useQuery(GET_ALLPRODUCT_BY_ID, {
    //      variables: {
    //          token: productId
    //      }
    //  });

    const [allProduct, setAllProduct] = useState([])

    const [currentIndex, setCurrentIndex] = useState(0);
    const scrollX = useRef(new Animated.Value(0)).current;
    const slidesRef = React.useRef(null);



    useEffect(async () => {
        await setAllData(data.stores[0])
        await setProductData(allData.products)
        await setCampaignProductData(allData.offer_products)
        console.log("---product id=--",allData.offer_products[0].product_image[0].product_id)

        if(myParam!=null&&!changed)
        {
            changed=true;
            for(var i=0;i<allData.offer_products.length;i++){
            console.log("---product id=--",allData.offer_products[i].product_image[0].product_id)
             if(myParam==allData.offer_products[i].product_image[0].product_id){
                 onConfirmCampaignProduct(campaignProductData[i]);
                 break;
             }
         }
        }
    })

    useEffect(async () => {

        // await setAllProduct(productDetail.getProductDetailByProductId[0])
        let percentVal = allProduct.product_price
        let percentToget = 1
        let res = (percentToget / 100) * percentVal

        setPercentge(Math.floor(res))
        await setProductImage(productDetail.getProductDetailByToken[0].product_image[0].product_image)


    })


    console.log('--------------allProduct  -', allProduct)

    const startDate = new Date().getTime();
    const d = allProduct.expire_date;
    const endDate = new Date(d).getTime();
    const timeDiff = (endDate + (1000 * 60 * 60 * 24)) - startDate
    var totalDay = Math.floor(timeDiff / (1000 * 60 * 60 * 24))
    var totalHours = Math.floor((timeDiff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
    // const totalMinute = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60))

    const slidesData = allProduct.product_image

    const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 150 }).current;

    const scrollTo = () => {
        if (currentIndex < slidesData.length - 1) {
            slidesRef.current.scrollToIndex({ index: currentIndex + 1 })
        }

    }


    const onLearnMore = () => {

        setShowLearnMore(true)
    }


    const onConfirmProductDetail = async () => {
        // navigation.navigate("productCheckOut")
       
         const  a= {
            _id:allProduct._id,
            store_id:allData._id,
            product_id:allProduct.product_image[0].product_id,
            product_name:allProduct.product_name,
            status:allProduct.status,
            product_price:allProduct.product_price,
            product_description:allProduct.product_description,
            currency:allProduct.currency,
            discount:allProduct.discount,
            offer_price:allProduct.offer_price,
            message:"hdh",
            total_contacts:1,
            expire_date:allProduct.expire_date,
            store_detail: { store_name:allData.store_name, shop_address:allData.shop_address, _id:allData._id },
            marchant_name:allData.marchant_name,
            payment_option:allData.payment_option,
            product_image:allProduct.product_image
         };
    //  console.log('--------a----', a)
      // window.localStorage.setItem("allProduct",JSON.stringify(a));
       navigation.navigate("productCheckOut", {allProduct:a})
      // navigation.navigate("productCheckOut")

    }
    //          console.log('--------data', data, userText)

    const onConfirmProduct = async (item) => {
        //    await refetch()
        setAllProduct(item)
        setTimeout(() => setShowDetail(true), 100)

    }

    const onConfirmCampaignProduct = async (item) => {
        setAllProduct(item)
        setTimeout(() => setShowDetail(true), 100)

    }




    const renderItem = ({ item }) => {
        return (
            <View>
                <TouchableOpacity onPress={() => onConfirmProduct(item)} style={styles.mainBox}>

                    <View style={styles.leftView}>
                        <Image source={item == undefined ? null : { uri: 'https://nuzo.co/product_images/' + item.product_image[0].product_image }}
                            style={styles.productLogo} />
                    </View>

                    <View style={styles.right}>
                        <Text style={styles.nameText}>{item.product_name}</Text>
                        <View style={styles.priceText}>

                            <Text style={styles.totalprice}><Text style={styles.product_price}>{item.currency + " " + item.product_price}</Text></Text>
                            <Text style={styles.totalPoints}>{"Earn " + Math.floor(item.product_price / 100) + " points"}</Text>

                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )

    }

    const renderCampaignItem = ({ item }) => {
        return (
            <View>
                <TouchableOpacity onPress={() => onConfirmCampaignProduct(item)} style={styles.mainBox}>

                    <View style={styles.leftView}>
                        <Image source={item == undefined ? null : { uri: 'https://nuzo.co/product_images/' + item.product_image[0].product_image }}
                            style={styles.productLogo} />
                    </View>

                    <View style={styles.right}>
                        <Text style={styles.nameText}>{item.product_name}</Text>
                        <View style={styles.priceText}>

                            <Text style={styles.totalprice}>
                                <Text style={{ textDecorationLine: 'line-through' }} >{item.currency + " " + item.product_price}</Text> / <Text style={styles.product_price}>{item.currency + " " + item.offer_price}</Text>
                                </Text>
                            <Text style={styles.totalPoints}>{"Earn " + Math.floor(item.offer_price / 100) + " points"}</Text>

                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )

    }


    return (
        <>

            {
                loading ?
                    <View style={styles.indicator}>
                        < ActivityIndicator size="large" color={Color.skyBlue} />
                    </View>
                    :
                    <>

                        {!showDetail ?
                            <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
                                <ImageBackground
                                    resizeMode='cover'
                                    source={allData == undefined ? null : { uri: 'https://nuzo.co/store_images/' + allData.store_image }}
                                    style={styles.backgroubdImage}>
                                    <Text style={styles.backgroubdImageText}>{allData == undefined ? null : allData.store_name}
                                    </Text>

                                </ImageBackground>

                                <View style={styles.main}>
                                    {/* <View style={styles.searchBox}>
                                <Image source={searchIcon} style={styles.searchIcon} />
                                <TextInput
                                    value={searchText}
                                    onChangeText={(text) => setSearchText(text)}
                                    placeholder="Search for products"
                                    onFocus={() => setFocused(true)}
                                    onBlur={() => setFocused(false)}
                                    style={focused ? styles.input: styles.activeInput}
                                />
                            </View> */}

                                    <Text style={styles.productHeading}>Campaign Products</Text>

                                    <FlatList
                                        data={campaignProductData}
                                        renderItem={renderCampaignItem}
                                        showsVerticalScrollIndicator={false}
                                        keyExtractor={(item, index) => index.toString()}
                                    />

                                    <Text style={styles.productHeading}>Store Products</Text>

                                    <FlatList
                                        data={productData}
                                        renderItem={renderItem}
                                        showsVerticalScrollIndicator={false}
                                        keyExtractor={(item, index) => index.toString()}
                                    />


                                </View>
                            </ScrollView>



                            :





                            <View style={styles.container}>
                                <ScrollView showsVerticalScrollIndicator={false} style={showLearnMore ? { opacity: 0.6 } : null}>
                                    <Header
                                        leftIcon={{ uri: backIcon }}
                                        onLeftPress={() => setShowDetail(false)}
                                        leftIconStyle={styles.menuLeftIcon}
                                        leftNextText="Product details" />



                                    <View style={styles.main}>
                                        <View>
                                            <View style={styles.sliderStyle}>
                                                <FlatList
                                                    data={slidesData}
                                                    renderItem={({ item }) => <SlideScreen item={item} currentIndex={currentIndex} />}
                                                    horizontal
                                                    showsHorizontalScrollIndicator={false}
                                                    pagingEnabled
                                                    bounces={false}
                                                    keyExtractor={(item, index) => index.toString()}
                                                    onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
                                                        useNativeDriver: false
                                                    })}
                                                    scrollEventThrottle={35}
                                                    viewabilityConfig={viewConfig}
                                                    ref={slidesRef} />

                                                {slidesData != undefined ?
                                                    <Slider data={slidesData} scrollX={scrollX} scrollTo={scrollTo} currentIndex={currentIndex} />
                                                    : null}


                                            </View>
                                            <Text style={styles.textHeading}>{allProduct.product_name}</Text>

                                            <View style={styles.productDetailView}>
                                            {allProduct.offer_price ?

                                                <View style={styles.detailView}>
                                                    <Text style={styles.productDetailItem}>{'Price'}</Text>
                                                    <Text style={styles.price_of}>{allProduct.currency} {allProduct.product_price}</Text>
                                                </View>
                                             : 
                                             <View style={styles.detailView}>
                                             <Text style={styles.productDetailItem}>{'Price'}</Text>
                                             <Text style={styles.price}>{allProduct.currency} {allProduct.product_price}</Text>
                                         </View>
                                         }

                                                {allProduct.offer_price ?
                                                    <View style={styles.detailView}>
                                                        <Text style={styles.productDetailItem}>{'Promo'}</Text>
                                                        <Text style={styles.priceGreen}>{allProduct.currency} {allProduct.offer_price}</Text>
                                                    </View>
                                                    : null}

                                                {allProduct.discount ?
                                                    <View style={styles.detailView}>
                                                        <Text style={styles.productDetailSaveItem}>{'You save'}</Text>
                                                        <Text style={styles.savePrice}>{allProduct.currency} {allProduct.discount}</Text>
                                                    </View>
                                                    : null}
                                                {allProduct.expire_date ?
                                                    <View style={styles.detailView}>
                                                        <Text style={styles.productDetailItem}>{'Ends in'}</Text>
                                                        <Text style={styles.checkTime}>
                                                            {totalDay >= 0 ? totalDay + " " + "days" : "0 days"}
                                                            {totalHours >= 0 ? " " + totalHours + " " + "hours" : " 0 hours"}
                                                        </Text>
                                                    </View> : null}
                                            </View>

                                            <TouchableOpacity onPress={onConfirmProductDetail} style={styles.button}>
                                                <Text style={styles.buttonText}>Buy this item (Earn {percentage} Nuzo Points)</Text>
                                            </TouchableOpacity>

                                            <View style={styles.learnMoreButton}>
                                                <Text style={styles.learnMoreText}>This order qualifies for {percentage} Nuzo Points.
                                                    <TouchableOpacity onPress={onLearnMore}>
                                                        <Text style={styles.clickableText}> Learn more.</Text>
                                                    </TouchableOpacity>
                                                </Text>
                                            </View>
                                            {showLearnMore &&
                                                <View style={styles.alertBox}>
                                                    <Text style={styles.alertText}>You earn Nuzo Points for shopping.Nuzo Points can be exchanged for cUSD which earns 50% interest from our partner Valora.Visit <Text
                                                        style={styles.clickableText}
                                                        onPress={() => {
                                                            Linking.openURL('https://www.valoraapp.com');
                                                        }}>
                                                        www.valoraapp.com
                                                    </Text>  for more details</Text>
                                                    <View style={styles.alertButtonsView}>
                                                        <TouchableOpacity onPress={() => setShowLearnMore(false)} style={styles.buttonAlert} >
                                                            <Text style={styles.alertButtonText}>Ok</Text>
                                                        </TouchableOpacity>

                                                    </View>
                                                </View>
                                            }



                                            <View style={styles.discription}>
                                                <Text style={styles.productDetailItemText}>Description</Text>
                                                <Text style={styles.discriptionText}>{allProduct.product_description}</Text>

                                            </View>
                                        </View>

                                    </View>

                                </ScrollView>

                            </View >
                        }
                    </>
            }
        </>
    )
}

export default ProductPage





