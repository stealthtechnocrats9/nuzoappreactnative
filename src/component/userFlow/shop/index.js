import React, { useState } from 'react'
import { Linking, View } from 'react-native'
import styles from './styles'
import User_Product_Detail from '../ProductDetail/index'

import ProductPage from '../productPage'
import NuzoPoints from '../nuzoPoint/index'

export default function Nuzo_Shop({ navigation,route }) {
  //const [show, setShow] = useState(true)
  var show=true;
  var lastSegment//=route.params.productId;

  // if(route.params==undefined){
    const queryParams = window.location.href;
    lastSegment = queryParams.split("/").pop();
 // }else{
 //  lastSegment=route.params;
 // }
  //console.log('----------------url--lastSegment----------', lastSegment)

  // Linking.getInitialURL().then((url) => {
  //   let data = url
  //   lastSegment = data.split("/").pop();
  //   //setUserToken(lastSegment)
  
    
  if(isNaN(lastSegment)){
   // console.log('----------------url--if-----------', 'lastSegment', lastSegment)

   show= true;
  }else{
  //  console.log('----------------url--else-----------', 'lastSegment', lastSegment)

  show= false;
    
  }
  
 // });
    
  return (
    <View style={styles.container}>
      {show ? 
          <ProductPage navigation={navigation} />
          :
        <User_Product_Detail navigation={navigation} />
      }
    </View>
  )
}
// const Stack = createStackNavigator();

// function App() {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator>
//         <Stack.Screen
//           name="Home"
//           component={Nuzo_Shop}
//           options={{ title: 'My home' }}
//         />

//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// }


