import { StyleSheet } from "react-native";
import Fonts from "../../../assets/fonts";
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    filterIcon: {
        height: 20,
        width: 20
    },
    main: {
        marginLeft: 20,
        marginRight: 20
    },
    totalPoints: {
        height: 76,
        width: '100%',
        borderRadius: 15,
        backgroundColor: 'red',
        marginTop: 23
    },
    linearGradient: {
        height: 76,
        borderRadius: 15,
        marginTop: 23,
        justifyContent: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 30
    },
    linearGradientInsideView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    goldIcon: {
        height: 44,
        width: 44,
        marginRight: 8
    },
    pointsValue: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginBottom: 1
    },
    pointsValueText: {
        color: Color.blueText,
        fontSize: 7,
        fontFamily: Fonts.Samsung,
 
    },
    linearGradienttitle: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,

        color: Color.whiteText
    },
    linearGradienttitleResultView: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 2
    },
    linearGradiebtResult: {
        fontSize: 28,
        fontFamily: Fonts.Samsung,

        color: Color.whiteText,
        alignItems: 'center'
    },
    linearGradiebtTitleResult: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,

        color: Color.whiteText,
        marginLeft: 8,
        textAlign: 'center'

    },
    stepper: {
        marginBottom: 40,
    },
    stepperSlider: {
        width: '100%',
        height: 32,
        borderWidth: 1,
        borderRadius: 38,
        borderColor: "#00B2EB"
    },
    sliderTitle: {
        marginTop: 12,
        fontSize: 7,
        fontFamily: Fonts.Samsung,

        color: Color.black_60
    },
    inner: {
        width: "100%",
        height: 32,
        borderRadius: 38,
    },
    label: {
        fontSize: 23,
        fontFamily: Fonts.Samsung,

        color: "black",
        position: "absolute",
        zIndex: 1,
        alignSelf: "center",
    },
    boxtitle: {
        fontSize: 13,
        fontFamily: Fonts.Samsung,

        marginBottom: 24

    },
    boxes: {
        height: 108,
        width: 152,
        borderRadius: 15,
        backgroundColor: Color.white,
        shadowColor: "#000000",
        shadowOpacity: 0.05,
        shadowRadius: 12,
        shadowOffset: {
            height: 0,
            width: 0
        },
        elevation: 1,
        justifyContent: 'center',
        paddingLeft: 12,
        paddingRight: 5

    },

    icon: {
        height: 24,
        width: 24,
        marginLeft: 4,
        marginTop: 7,
        marginBottom: 7
    },
    boxName: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,

        color: Color.black,
        marginBottom: 2
    },
    boxTitle: {
        fontSize: 7,
        fontFamily: Fonts.Samsung,

        color: Color.black_60

    }



})