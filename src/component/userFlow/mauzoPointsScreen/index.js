import React, { useState, useEffect } from 'react'
import { Text, View, Image, FlatList, Animated } from 'react-native'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import filterIcon from '../../../assets/filterIcon.png'
import LinearGradient from 'react-native-web-linear-gradient';
import Color from '../../../utils/Colors'
import gold from '../../../assets/gold.png'

import wait from '../../../assets/wait.png'
import celodollar from '../../../assets/celodollar.png'
import discount from '../../../assets/discount.png'
import mpesa from '../../../assets/mpesa.png'


// Within your render function



export default function mauzoPointsScreen({ navigation }) {
    const [progressStatus, setProgressStatus] = useState(80)
    const anim = new Animated.Value(0);
    const [data, setData] = useState([
        {
            name: "spin",
            img: wait,
            title: "Feeling lucky? Take a spin"
        },
        {
            name: "Withdraw to Celo",
            img: celodollar,
            title: "Convert your points to cUSD"
        },
        {
            name: "Discount",
            img: discount,
            title: "Get discount on your next purchase"
        },
        {
            name: "Withdraw to M-Pesa",
            img: mpesa,
            title: "Convert your points into M-Pesa"
        },

    ])


    const renderItem = (item) => {
        console.log("0o0o0o0o", item, item.item.img)
        return (
            <View style={styles.boxes}>
                <Image source={{ uri: item.item.img }} style={styles.icon} />
                <Text style={styles.boxName}>{item.item.name}</Text>
                <Text style={styles.boxTitle}>{item.item.title}</Text>

            </View>
        )
    }

    const onAnimate = () => {
        let val = progressStatus
        console.log('val', val)
        anim.addListener(({ value }) => {
            setProgressStatus(parseInt(value, 100))
        })
        Animated.timing(anim, {
            toValue: val,
            duration: 50000,
        }).start();
    }

    useEffect(() => {
        onAnimate()
    }, [])
    return (
        <View style={styles.container}>
            <Header
                leftText="Nuzo Points"
                rightIcon={{ uri: filterIcon }}
                rightIconStyle={styles.filterIcon}
                onRightPress={() => navigation.goBack()}
            />
            <View style={styles.main}>

                <LinearGradient
                    start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                    colors={[Color.linearGradientblue, Color.linearGradientlightblue]}
                    style={styles.linearGradient}>
                    <View style={styles.linearGradientInsideView}>
                        <Image source={{ uri: gold }} style={styles.goldIcon} />
                        <View>
                            <Text style={styles.linearGradienttitle}>You have</Text>
                            <View style={styles.linearGradienttitleResultView}>
                                <Text style={styles.linearGradiebtResult}>600</Text>
                                <Text style={styles.linearGradiebtTitleResult}>Nuzo Points</Text>
                            </View>
                        </View>

                    </View>
                </LinearGradient>

                <View style={styles.stepper}>
                          <View style={styles.pointsValue}>
                              <Text style={styles.pointsValueText}>0</Text>
                              <Text style={styles.pointsValueText}>250</Text>
                              <Text style={styles.pointsValueText}>500</Text>
                              <Text style={styles.pointsValueText}>750</Text>
                              <Text style={styles.pointsValueText}>1000</Text>
                              </View>

                    <Animated.View
                    style={styles.stepperSlider}>
                        <LinearGradient
                            start={{ x: 1, y: 1 }} end={{ x: 0, y: 1 }}
                            colors={[ Color.linearGradientlightblue, Color.linearGradientblue]}
                            style={[
                                styles.inner, { width: progressStatus + "%" },
                            ]} />
                    </Animated.View>
                    {/* <Animated.Text style={styles.label}>
                        {progressStatus}%
                    </Animated.Text> */}

                    <Text style={styles.sliderTitle}>Spend KSh 2,500 more and get a free spin where you can win exciting prizes</Text>
                </View>

                <View>
                    <Text style={styles.boxtitle}>Redeem your Nuzo Points</Text>
                    <FlatList
                        data={data}
                        numColumns={2}
                        columnWrapperStyle={{
                            justifyContent: 'space-between',
                            marginTop: 8,
                            marginBottom: 8,
                            marginLeft: 2,
                            marginRight: 2
                        }}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>
        </View>
    )
}

