import {StyleSheet} from 'react-native'
import Fonts from '../../../assets/fonts'
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flea: 1,
        backgroundColor: Color.white
    },
      
    main: {
        marginLeft: 20,
        marginRight: 20
    },
    logo: {
        height: 100,
        width: 100,
        marginTop: 148,
        marginBottom: 24,
        alignSelf: 'center',

    },
    heading: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        color: Color.black,
        fontWeight: '600',
        textAlign: 'center',
        wordBreak:'break-all'
    },
    titleText: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        color: Color.black_60,
        marginTop: 8,
        textAlign: 'center',
        marginLeft: 29,
        marginRight: 39
    },
    address: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 34,
    marginRight: 34,
    marginTop: 36,
    marginBottom: 10
    },
    addressText: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: '500',
        color: Color.black_60
    },
    getDirection: {
        fontSize: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: '500',
        color: Color.skyBlue
    },

    location: {
     flexDirection: 'row',
     marginLeft: 35,
     marginRight: 35,
     alignItems: 'center'
    },

    locationIcon: {
       height: 16,
       width: 10,
       marginRight: 17
    },
    button: {
        height: 52,
        borderRadius: 16,
        width: '100%',
        backgroundColor: Color.skyBlue,
        marginBottom: 28,
        alignItems: 'center', 
        justifyContent: 'center',
        marginTop: 77.5
    },
    buttonText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        color: Color.whiteText,
        fontStyle: 'normal',
        fontWeight: '600',
        textAlign: 'center'
    }
})


 