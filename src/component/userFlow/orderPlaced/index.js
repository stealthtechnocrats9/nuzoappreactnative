import React from 'react'
import { Text, View, Image, TouchableOpacity, Linking } from 'react-native'
import styles from './styles'
import tick from '../../../assets/tick.png'
import location from '../../../assets/locationIcon.png'


export default function Order_Placed_Confirm({ navigation }) {
    const onConfirm = () => {
        // navigation.navigate("spin_screen")
    }

    return (
        <View style={styles.container}>
            <View style={styles.main}>
                <Image source={{ uri: tick }}
                    style={styles.logo} />
                <Text style={styles.heading}>Order placed!</Text>

                <Text style={styles.titleText}>
                    We’ve sent you an SMS with details of your order including your confirmation code.
                </Text>
                <View style={styles.address}>
                    <Text style={styles.addressText}>Pickup address</Text>
                    <Text style={styles.getDirection}>Get direction</Text>
                </View>

                
                <TouchableOpacity style={styles.location} onPress={()=> Linking.openURL("https://www.google.com/maps/dir/?api=1&destination=chandigarh+busstand")}> 
                    <Image source={{ uri: location }} style={styles.locationIcon} />
                    <Text style={styles.addressText}>Kenya Cinema, Moi Avenue, Nairobi, Kenya </Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button}
                    onPress={onConfirm}>
                    <Text style={styles.buttonText}>Go back to product</Text>
                </TouchableOpacity>


            </View>
        </View>
    )
}






// import React from 'react';
// import { StyleSheet, Text, View, FlatList } from 'react-native';
// import Swiper from 'react-native-web-swiper';

// const styles = StyleSheet.create({
//     container: {
//         height: 150,
//         width: 150,
//         backgroundColor: 'lightgreen',
//         alignSelf: 'center'

//     },
//     slideContainer: {
//         height: 100,
//         width: 100,
//         alignSelf: 'center',
//         marginVertical: 25

//     },
//     slideCont: {
//         margin: 30,
//         backgroundColor: 'red'
//     },
//     slide1: {
//         backgroundColor: 'rgba(20,20,200,0.3)',
//     },
//     //   slide2: {
//     //     backgroundColor: 'rgba(20,200,20,0.3)',
//     //   },
//     //   slide3: {
//     //     backgroundColor: 'rgba(200,20,20,0.3)',
//     //   },
// });


// const Data = [
//     "step 1", "step 2", "step 3", "step 4", "step 5"
// ]

// export default class Order_Placed_Confirm extends React.Component {
//     render() {
//         return (
//             <View style={styles.container}>
//                 <Swiper
//                    controlsProps={{
//                     prevTitle: null,
//                     nextTitle: null,
//                     dotsTouchable: true,
//                     dotActiveStyle: {
//                         backgroundColor: 'red',
//                         width: 8
//                     }
//                    }}
//                    >
//                     <View style={[styles.slideContainer, styles.slide1]}>
//                         <Text>Slide 1</Text>
//                     </View>
//                     <View style={[styles.slideContainer, styles.slide2]}>
//                         <Text>Slide 2</Text>
//                     </View>
//                     <View style={[styles.slideContainer, styles.slide3]}>
//                         <Text>Slide 3</Text>
//                     </View>

//                 </Swiper>



//             </View>
//         );
//     }
// }