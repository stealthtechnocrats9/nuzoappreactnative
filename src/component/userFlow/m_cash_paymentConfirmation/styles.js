import { StyleSheet } from "react-native";
import Fonts from "../../../assets/fonts";
import Color from '../../../utils/Colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.webbackgroud
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menuLeftIcon: {
        height: 16,
        width: 15.85
    },
    main: {
        marginLeft: 20,
        marginRight: 20
    },
    paymentTitle: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.black,
        marginBottom: 24
    },
    heading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        marginBottom: 16
    },
    accountDetailBox: {
        backgroundColor: Color.skyBlue_5,
        paddingTop: 26.5,
        paddingLeft: 7,
        paddingBottom: 32,
        paddingRight: 6
    },
    amountTopHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        marginBottom: 9.5

    },
    amountHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        marginBottom: 6.5,
        marginTop: 18.5

    },
    amountDetail: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        fontWeight: "700",
        marginTop: 3,
        marginBottom: 3
    },
    amountTopTitle: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        fontWeight: "500",
        marginTop: 28,
        alignSelf: 'center',
    },
    amountTitle: {
        fontSize: 12,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        fontWeight: "500",
        marginTop: 4,
        marginBottom: 8.1,
        alignSelf: 'center',

    },
    merchantaddress: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      height: 50
    },
    cardName: {
        fontSize: 24,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
        fontWeight: "700",
        alignSelf: 'center',
        textAlign: 'center'
    },
    directionHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.skyBlue,
        fontWeight: 'bold'
       
    },
    addressHeading: {
        fontSize: 15,
        fontFamily: Fonts.Samsung,
        color: Color.blue_7,
    },

    amountDetailInsideBox: {
        height: 40,
        backgroundColor: Color.white,
        justifyContent: 'center',
        alignItems: 'center'
    },


    lastInputStyle: {
        height: 40,
        width: 27,
        textAlign: 'center',
        marginRight: 0,
        alignItems: 'center',
        backgroundColor: Color.white
    },


    swipeButton: {
        height: 52,
        width: '100%',
        borderRadius: 16,
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: Color.skyBlue,
        marginTop: 24,
        marginBottom: 32,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
    },
    swipeConfirmButton: {
        height: 52,
        width: '100%',
        borderRadius: 16,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: Color.borderColor,
        backgroundColor: Color.white,
        marginTop: 24,
        marginBottom: 32,
        flexDirection: 'row',
        paddingLeft: 14,
        paddingRight: 14,
        flex: 1,
        marginLeft: 5,
        marginRight: 5
    },
    swipeView: {
        height: 52,
        width: 52,
        backgroundColor: Color.white,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16,
        borderWidth: 1,
        borderColor: Color.skyBlue
    },


    swipeArrow: {
        height: 16,
        width: 16
    },

    swipeConirmArrow: {
        height: 16,
        width: 16,

    },
    swipeText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.whiteText,
        alignSelf: 'center'

    },
    swipeConfirmText: {
        fontSize: 17,
        fontFamily: Fonts.Samsung,
        fontWeight: "600",
        color: Color.skyBlue,
        marginRight: 36,
        alignSelf: 'center'
    }
})