import React, { useState, useEffect, useRef } from 'react'
import { Text, View, Image, TextInput, Animated, TouchableOpacity, ActivityIndicator } from 'react-native'
import styles from './styles'
import Header from '../../../utils/Header/Header'
import backIcon from '../../../assets/back_arrow.png'
import swipeArrow from '../../../assets/swipeArrow.png'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import { CREATE_PAYMENT } from '../../../graphql/customerMutation/createPayment'
import { useMutation } from '@apollo/client'
import Color from '../../../utils/Colors'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function mCashPaymentConfirmation({ navigation, route }) {

    const merchantDetail = route.params.itemDetail
    const Checkout_id = route.params.id
    const customerId = route.params.customer_id
    const [pins, setPins] = useState(null)
    const [pinLength, setPinLength] = useState(10)
    let textInput = useRef(null)
    const [percentge, setPercentge] = useState('')
    const [storeDetail, setStoreDetail] = useState('')
    const nuzopoints = merchantDetail.offer_price

    const [createPayment, { data, loading, error }] = useMutation(CREATE_PAYMENT)

    useEffect(() => {
        setPins(merchantDetail.payment_option)
        setStoreDetail(merchantDetail.store_detail.shop_address)
        if (pins != null) {
            setPinLength(pins.length)
        }

        let percentVal = merchantDetail.offer_price
        let percentToget = 1
        let res = (percentToget / 100) * percentVal

        setPercentge(Math.floor(res))
    });

    // console.log('percentge------', percentge)
    // console.log('-----customerId--', customerId)

    // console.log('=Checkout_id===', Checkout_id, merchantDetail)
    const onPaymentConfirm = () => {
        createPayment({
            variables: {
                order_id: Checkout_id,
                customer_id: customerId,
                amount: merchantDetail.currency,
                type: "m-cash"
            },
        }).then((res) => {
            navigation.navigate("Payment_Processing", { percentge })

        }).catch((err) => {
            console.log('error', err)

        })
    }

    // useEffect(()=>{
    //   if(data != undefined){
    //   }
    // })

    console.log('data-------', data, 'error', error)

    const LeftSwipeActions = () => {
        return (
            <TouchableOpacity
                style={styles.swipeConfirmButton} >
                <View style={{ width: 30 }} />
                <Text style={styles.swipeConfirmText}>Order confirmed</Text>
                <Image source={swipeArrow} style={styles.swipeConirmArrow} />
            </TouchableOpacity>
        );
    };


    const swipeFromLeftOpen = () => {
        // alert('Swipe from left');
    };

    return (
        <>
            {
                loading ?
                    <View style={styles.indicator}>
                        < ActivityIndicator size="large" color={Color.skyBlue} />
                    </View>
                    :
                    <View style={styles.container}>
                        <Header
                            leftIcon={{ uri: backIcon }}
                            onLeftPress={() => navigation.goBack()}
                            leftIconStyle={styles.menuLeftIcon}
                            leftNextText="Checkout"
                        />
                        <View style={styles.main}>
                            <Text style={styles.paymentTitle}>Pay with Cash</Text>

                            <Text style={styles.heading}>With the cash option, you get to pay when you go to collect your item or upon delivery.</Text>
                            <Text style={styles.paymentTitle}>Payment details</Text>
                            <View style={styles.accountDetailBox}>

                                <Text style={styles.amountTopHeading}>Amount to send</Text>
                                <Text style={styles.amountDetail}>{merchantDetail.currency} {merchantDetail.offer_price}</Text>

                                <View style={styles.merchantaddress}>
                                    <Text style={styles.addressHeading}>Merchant’s Address</Text>
                                    <Text style={styles.directionHeading}>Get Direction</Text>
                                </View>
                                <Text style={styles.amountDetail}>Shop 5, All People’s Road, Mombasa</Text>



                            </View>


                            <Text  style={[styles.heading, {marginTop: 15}]}>While we always give Nuzo Points for every successful transactions, getting your Nuzo points for cash transactions depends on the merchant you buy from.</Text>
                            <Swipeable
                                renderLeftActions={LeftSwipeActions}
                                onSwipeableLeftOpen={swipeFromLeftOpen}
                                onSwipeableLeftOpen={onPaymentConfirm}
                            >
                                <View style={styles.swipeButton} >

                                    <View style={styles.swipeView}>
                                        <Image source={swipeArrow} style={styles.swipeArrow} />
                                    </View>
                                    <Text style={styles.swipeText}>Swipe to confirm Order</Text>
                                    <View style={{ height: 5, width: 20 }} />
                                </View>
                            </Swipeable>






                        </View>
                    </View>
            }
        </>
    )
}

