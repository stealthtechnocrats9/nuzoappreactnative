import { StyleSheet } from "react-native";
import Fonts from "../../assets/fonts";
import Color from '../Colors'; 

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    modelBox: {
        height: 100,
        width: "80%",
        backgroundColor: "#fff",
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        // paddingHorizontal: 15,
        // justifyContent: 'space-around',
        borderRadius: 15
    },
    modelSpinner: {
       width: '35%',
    },
    modelText: {
        width: '65%',
        fontSize: 15,
        paddingRight: 10,
        fontFamily: Fonts.Samsung,
        fontWeight: '700'
        // color: Color.blueText
    }
})

