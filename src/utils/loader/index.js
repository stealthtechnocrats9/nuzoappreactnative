import React, { useState } from 'react'
import { Text, View, ActivityIndicator } from 'react-native'
import styles from './styles'
import Color from '../Colors'; 

import Modal from "react-native-modal";



export default function Loader() {
 
    const [isVisible, setIsVisible] = useState(true)
    return (
        <View style={styles.container}>
            <Modal
                isVisible={true}>
                         < ActivityIndicator size="large" color={Color.skyBlue} />
            </Modal>
        </View>
    )
}



