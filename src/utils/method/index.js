import { showMessage, hideMessage } from "react-native-flash-message";
import { duration } from 'moment';

import { takeEvery, put } from 'redux-saga/effects';

import { hideLoading, showLoading } from '../../config/loader/action';


export function* hideLoader(isError, errorMessage) {
  yield put(hideLoading(isError, errorMessage));
}
export function* showLoader(silentFetch) {
  console.log('show loader', silentFetch)
  if (!silentFetch) {
    yield put(showLoading());
  }
}

export const showToast = (message, type) => showMessage({
  message: `${message}`,
  type: type,
  icon: 'auto'
});


export const hideToast = (message, type) => showMessage({
  message: `${message}`,
  type: type,
  icon: 'auto'
});


export const durationToStr = (ms) => {
  const h = duration(ms).hours();
  const m = duration(ms).minutes();
  const s = duration(ms).seconds();

  let hStr = '';
  if (h !== 0) hStr = `${h}:`;

  let mStr = '2';
  mStr = m < 10 ? `0${m}` : m;

  let sStr = '';
  sStr = s < 10 ? `0${s}` : s;


  return `${hStr}${mStr}:${sStr}`;
};