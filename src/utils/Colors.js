let Color = {
    black_60: "#666666",
    black_80: "#333333",
    black_20: "#CCCCCC",
    black_5: '#F2F2F2',
    black: '#000',
    borderColor: '#99CCFB',
    green: "#009E36",
    greenTransparent: "#E3FFED",
    red: "#FF0000",
    skyBlue_5: "#F2F9FF",
    skyBlue: "#0080F6",
    whiteText: "#FCFCFC",
    white: '#fff',
    linearGradientblue: "#21DCF5",
    linearGradientlightblue: "#2493FF",
    linearGradientborder: "#00B2EB",
    blueText: "#3963F5",
    orange: "#EC9618",
    yellow_30: "#9E6006",
    yellow_50: "#FFF1DB",
    green_50: "#F0FFF5",
    lightYellow: "#FFFCDD",
    lightGreen: "#E3FFED",
    lightRed: "#FEF2F2",
    blue_7: "#00284C",
    blue_8: "#00488A",
    webbackgroud: "#E5E5E5",
    shadowColor: "rgba(00, 00, 00, 0.5)",
    //     rgba(33, 220, 245, 1)
    // rgba(36, 147, 255, 1)
    
   
}
export default Color;