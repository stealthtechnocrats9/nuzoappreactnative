import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import Fonts from '../../assets/fonts'
import Color from '../Colors'
import Images from '../Images'

const Header = props => {
    return (
        <View style={styles.header}>
            <View style={styles.left}>
                {props.leftIcon ?
                    <TouchableOpacity onPress={props.onLeftPress} style={styles.leftclick}>
                        <Image source={props.leftIcon} style={props.leftIconStyle} />
                    </TouchableOpacity>
                    :
                    <Text style={styles.leftTextStyle}>{props.leftText}</Text>
                }
                {!props.leftIcon ?
                    <TouchableOpacity onPress={props.onLeftPress} style={styles.leftclick}>
                        <Image source={props.leftNextIcon} style={props.leftIconNextStyle} />
                    </TouchableOpacity>
                    :
                    <Text style={styles.leftTextNextStyle}>{props.leftNextText}</Text>
                }
            </View>

            <View style={styles.right}>
                {props.rightIcon ?
                    <TouchableOpacity onPress={props.onRightPress} style={styles.onRightPress}>
                        <Image source={props.rightIcon} style={props.rightIconStyle} />
                    </TouchableOpacity>
                    :
                    <Text>{props.rightText}</Text>
                }

                {!props.rightNextText ?
                    <TouchableOpacity onPress={props.onRightNextPress} style={styles.onRightPress}>
                        <Image source={props.rightNextSecIcon} style={props.rightNextIconStyle} />
                    </TouchableOpacity>
                    :
                    <Text>{props.rightNextText}</Text>}

            </View>


            <View style={styles.logoStyle}>
                <Image source={Images.mainlogo} style={styles.logo} />
            </View>
        </View>
    )
}

export default Header;

const styles = StyleSheet.create({
    header: {
        backgroundColor: Color.skyBlue_5,
        height: 60,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        // paddingHorizontal: 21,
        paddingLeft: 6,
        paddingRight: 21,
        marginBottom: 8
    },
    leftclick: {
        paddingVertical: 10,
        paddingLeft: 15,
        paddingRight: 3
    },
    left: {
        width: '66%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    leftTextStyle: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        marginLeft: 7.6,
        color: Color.black,

        fontStyle: 'normal',
        fontWeight: '600',
        // lineHeight: '130%'
    },
    leftTextNextStyle: {
        fontSize: 20,
        fontFamily: Fonts.Samsung,
        marginLeft: 7.6,
        color: Color.black,
        fontWeight: '600'
    },
    right: {
        width: '30%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    logoStyle: {
        position: 'absolute',
        right: 80,
        bottom: 0
    },
    logo: {
        height: 49,
        width: 71
    },
    onRightPress: {
        // backgroundColor: 'red',
        padding: 10
    }
})
