import React, {useEffect} from 'react'
import WebNavigation from './src/navigation/WebFlowNavigation/WebNavigation'
import { ApolloClient, InMemoryCache, ApolloProvider, ApolloLink } from "@apollo/client";
import {setContext} from '@apollo/client/link/context'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createUploadLink } from 'apollo-upload-client';



const App = () => {

  const authLink = setContext(async (_, { headers }) => {
    let token = await AsyncStorage.getItem('usertoken')

    console.log('=======token----------------', token)
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : "",
      }
    }
  });


 const client = new ApolloClient({
    link: authLink.concat(createUploadLink({ uri: 'https://nuzo.co/graphql' })),
    cache: new InMemoryCache()
  });


  return (
    <>
      <ApolloProvider client={client}>
        <WebNavigation />
      </ApolloProvider>
      </>
  )
}

export default App;



