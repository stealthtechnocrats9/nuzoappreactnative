import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Web_App from './Web_App';
//import { firebaseConfig } from './src/firebaseConfig';
//import firebase from '@react-native-firebase/app';
//import analytics from '@react-native-firebase/analytics';

// import crashlytics from "@react-native-firebase/crashlytics";
// import * as firebase from 'react-native-firebase';

 //firebase.initializeApp(firebaseConfig);

if (module.hot) {
  module.hot.accept();
}
AppRegistry.registerComponent(appName, () => Web_App);
AppRegistry.runApplication(appName, {
  initialProps: {},
  rootTag: document.getElementById('app-root'),
});