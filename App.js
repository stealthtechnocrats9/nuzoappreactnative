import React, {useEffect} from 'react'
import StackNavigation from './src/navigation/StackNavigation'
import { LogBox } from 'react-native';
import { ApolloClient, InMemoryCache, ApolloProvider, createHttpLink, ApolloLink } from "@apollo/client";
import {setContext} from '@apollo/client/link/context'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createUploadLink } from 'apollo-upload-client';
import ApolloLinkTimeout from 'apollo-link-timeout';
//import crashlytics from "@react-native-firebase/crashlytics";



//Ignore all log notifications
LogBox.ignoreAllLogs();

const App = () => {

  const httpLink = createUploadLink({ uri: 'https://nuzo.co/graphql' });
 // const httpLink = createUploadLink({ uri: 'http://137.184.65.210/graphql' });
  
  const timeoutLink = new ApolloLinkTimeout(300000);
  const timeoutHttpLink = timeoutLink.concat(httpLink);
  const authLink = setContext(async (_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = await AsyncStorage.getItem('token');
    console.log('=======token', token)

    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : "",
      }
    }
  });




  const client = new ApolloClient({
    link: ApolloLink.from([
      authLink,
      timeoutHttpLink
      // createUploadLink({ uri: 'https://nuzo.co/graphql' }),
    ]),
    cache: new InMemoryCache(),
  });


  // const client = new ApolloClient({
  //   link: authLink.concat(createUploadLink({ uri: 'https://nuzo.co/graphql' })),
  //   cache: new InMemoryCache()
  // });
  

  return (
    <>
      <ApolloProvider client={client}>
        <StackNavigation />

      </ApolloProvider>
      
      </>
  )
}

export default App;

